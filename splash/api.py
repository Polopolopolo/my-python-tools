from pathlib import Path

from . import utils

from myutils.file_io import toml_fload


def lst():
    if utils.config['apps_path'] not in (path.name for path in Path(utils.abspath('.')).iterdir()):
        return {}
    else:
        return toml_fload(utils.abspath(utils.config['apps_path']))

def register(name: str, descr: str, info: dict):
    with open(utils.abspath(utils.config['apps_path']), 'a') as f:
        f.write(f'[{name}]\n')
        f.write(f'descr="{descr}"\n')
        for k, v in info.items():
            f.write(f'{k}="{v}"\n')
        f.write('\n')
