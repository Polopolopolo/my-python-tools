from argparse import ArgumentParser
from sys import argv
import subprocess

from . import utils
from . import api


def lst():
    libs = api.lst()
    if libs:
        width = max(len(name) for name in libs)
        for name, info in libs.items():
            print (name.ljust(width), end=' : ')
            print (info['descr'])
    else:
        print ('No libraries.')

def register(info_ipt, name, descr):
    info = {}
    if info_ipt:
        for inf in info_ipt:
            try:
                k, v = inf.split('=', 1)
            except ValueError:
                exit('The `info` argument must be in the form `key1=value1 key2=value2 ...`')
            else:
                info[k] = v
    api.register(name, descr, info)

def run(app, args):
    if app not in api.lst():
        exit(f'Unknown application: {app}')
    elif api.lst()[app]['mylibs']:
        subprocess.run(['python3', '-m', f'mylibs.{app}'] + args)
    else:
        subprocess.run(['python3', '-m', f'sph-{app}'] + args)

def info(app):
    lst = api.lst()
    if app not in lst:
        exit(f'Unknown application: {app}')
    else:
        app = lst[app]
        print (f'[{app}]')
        width = max(len(k) for k in app)
        for k, v in app.items():
            print (f'{k.ljust(width)} = {v}')


if __name__ == '__main__':
    parser = ArgumentParser(description='Manage splash applications')
    subparsers = parser.add_subparsers(dest='command')

    list_subparser = subparsers.add_parser('list', help='list all registered splash applications')

    register_subparser = subparsers.add_parser('register', help='register an applications to splash')
    register_subparser.add_argument('name', help='name of the applications')
    register_subparser.add_argument('descr', help='description of the application')
    register_subparser.add_argument('--info', nargs='+', help='additional information about the application')

    run_subparser = subparsers.add_parser('run', help='run a splash application')
    run_subparser.add_argument('app', help='name of the application to run')
    run_subparser.add_argument('args', nargs='*', help='arguments to pass to the application')

    info_subparser = subparsers.add_parser('info', help='print information on a given application')
    info_subparser.add_argument('app', help='name of the application')

    if len(argv) == 1:
        parser.print_usage()
    else:
        match argv[1]:
            case '--help':
                parser.print_help()
            case '--list':
                lst()
            case '--register':
                if len(argv) < 5: register_subparser.print_usage()
                else: register(*argv[2:5])
            case '--info':
                if len(argv) < 3: info_subparser.print_usage()
                else: info(argv[2])
            case app:
                if len(argv) < 3: parser.print_usage()
                else: run(argv[2], argv[3:])
