from argparse import ArgumentParser

from . import utils
from . import api


def cmd(args):
    api.cmd(args.path, args.cmd)


if __name__ == '__main__':
    config = utils.get_config()

    parser = ArgumentParser(description='')
    subparsers = parser.add_subparsers(dest='command')

    cmd_subparser = subparsers.add_parser('cmd', help='run a command to a xournal as if it was uncompress')
    cmd_subparser.add_argument('path', help='path of the xournal file')
    cmd_subparser.add_argument('cmd', help=f'command to be applied (use {config["cmd"]["placeholder"]} as a placeholder for the xournal file)')

    args = parser.parse_args()
    match args.command:
        case 'cmd':
            cmd(args)
        case default:
            parser.print_usage()
