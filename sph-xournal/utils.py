from pathlib import Path

from myutils.file_io import toml_fload


def abspath(rel_path):
    dirpath = Path(__file__).parent
    return (dirpath / rel_path).as_posix()

def get_config():
    return toml_fload(abspath('config.toml'))
