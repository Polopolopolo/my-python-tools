from pathlib import Path
import gzip
import shlex
import subprocess

from . import utils


def cmd(path: str, cmd: str):
    config = utils.get_config()
    proc = subprocess.run('mktemp', stdout=subprocess.PIPE)
    tmp_path = proc.stdout.decode().replace('\n', '')
    xopp = gzip.decompress(Path(path).read_bytes())
    Path(tmp_path).write_bytes(xopp)
    cmd_list = shlex.split(cmd.replace(config['cmd']['placeholder'], tmp_path))
    subprocess.run(cmd_list)
