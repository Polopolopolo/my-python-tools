from pathlib import Path
from string import Template


mybeamer_root = Path(__file__).parent

class TexTemplate(Template):
    delimiter = '%$%'
