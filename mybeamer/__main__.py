from argparse import ArgumentParser

from . import api


def new_project(name: str):
    api.new_project(name)

def init_project():
    api.init_project()

def multi_make():
    ...


if __name__ == '__main__':
    parser = ArgumentParser(description='Beamer management')
    sb = parser.add_subparsers(dest='command')

    sb_new_project = sb.add_parser('new-project', help='Create a new project')
    sb_new_project.add_argument('name', help='Name of the directory to be created')

    sb_init_project = sb.add_parser('init-project', help='Init a project according to .mybeamer.yml')

    sb_multi_make = sb.add_parser('multi-make',
                                  help='Create a structure for multi-make (every part can be compiled independently\n'
                                       '(!) init-project'
    )

    args = parser.parse_args()
    match args.command:
        case 'new-project':
            new_project(args.name)
        case 'init-project':
            init_project()
        case 'multi-make':
            ...
        case other:
            parser.print_usage()