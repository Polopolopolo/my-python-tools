from pathlib import Path
import yaml

from .utils import mybeamer_root, TexTemplate


def new_project(name: str):
    '''Create a new directory named `name` containing the following files and directories.
    - commands/ (symlink to lntargets/commands)
    - tikz-template/
    - tex/
    - .mybeamer.yml
    - commands/project_commands.tex
    - main.tex
    - references.bib

    The .mybeamer.yml config file is for the command init-project
    '''
    direc = Path(name)
    direc.mkdir()
    (direc / 'commands').symlink_to(mybeamer_root / 'lntargets/commands')
    (direc / 'tikz-template').mkdir()
    (direc / 'tex').mkdir()
    mybeamerjson = (mybeamer_root / 'cpy/.mybeamer.yml').read_text()
    (direc / '.mybeamer.yml').write_text(mybeamerjson)
    (direc / 'commands/project_commands').touch()
    maintex = (mybeamer_root / 'cpy/main.tex').read_text()
    (direc / 'main.tex').write_text(maintex)
    (direc / 'references.bib').touch()


def init_project():
    '''Initialize a project according to the `.mybeamer.yml` config file.

    - creates a texfile and a folder in `tex/` for each 'parts' of `.mybeamer.yml`
    - then creates in each one of these folders one texfile for each slide in the part
    - finally, fill the main.tex with the part texfiles
    '''
    mybeamer = yaml.load(Path('.mybeamer.yml').read_text(), Loader=yaml.BaseLoader)
    tex = Path('tex')
    if isinstance(mybeamer, dict):
        for part, slides in mybeamer['parts'].items():
            # create the tex/part directory
            (tex / part).mkdir()
            # create the slide texfiles
            for slide, title in slides.items():
                (tex / part / f'{slide}.tex').write_text(
                    '\\begin{frame}\n'
                    f'  \\frametitle{{{title}}}\n'
                    '\\end{frame}\n'
                )
            # create the tex/part.tex
            (tex / f'{part}.tex').write_text(
                '\n'.join(f'\\input{{tex/{part}/{slide}.tex}}' for slide in slides)
            )
        # fill the main.tex
        main = Path('main.tex')
        tmpl = TexTemplate(main.read_text()).safe_substitute(parts='\n'.join(
            f'\\input{{tex/{part}.tex}}' for part in mybeamer['parts']
        ) + '\n%$%new-part')
        main.write_text(tmpl)


def multi_make():
    '''Create file and commands in Makefile for multi-make structure

    - create a main texfile for each part
    - add a makefile compile command for each part
    '''
    mybeamer = yaml.load(Path('.mybeamer.yml').read_text(), Loader=yaml.BaseLoader)
    if isinstance(mybeamer, dict):
        for part in mybeamer['parts']:
            # create a main texfile
            main = Path(f'main-{part}.tex')
            tmpl = TexTemplate(main.read_text()).safe_substitute(
                parts=f'\\input{{tex/{part}.tex}}'
            )
            main.write_text(tmpl)
            # create the makefile command
            ...
