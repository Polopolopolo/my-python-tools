import argparse
from math import ceil, log10
from pathlib import Path
import subprocess
from typing import List
import yaml

from . import api
from . import utils
from . import curses_utils
from .utils import config, pkgpath, pdfpath_from_dir

from mylibs.colors.utils import Colors


def _format_papers(indices, infos: List[dict], expr=''):
    offset1 = ceil(log10(len(api._papers_info(config['papers_path'])))) + 1
    paper_ids_short = []
    for info in infos:
        if 'paper_id' not in info:
            info['paper_id'] = ''
        if res := utils.InfoFile.parse_paper_id(info['paper_id']):
            paper_ids_short.append(res[0]+res[1])
        else:
            paper_ids_short.append(info['paper_id'])
    offset2 = max(len(pap_id) for pap_id in paper_ids_short)
    for i, info, short_id in zip(indices, infos, paper_ids_short):
        yield f'{i + 1: {offset1}} - ' + f"[{short_id}]".ljust(offset2 + len('[] ')) + info['title']

def search(expr: str):
    res = api.search(config['papers_path'], expr)
    if not res:
        print ('No paper found.')
    else:
        indices = [r[0] for r in res]
        infos = [r[1] for r in res]
        papers_line = list(_format_papers(indices, infos, expr))
        idxs = curses_utils.run(papers_line)
        if not idxs:
            print('No paper selected.')
        else:
            for line in papers_line:
                Colors.pcolor_match('red', line, expr)
            for idx in idxs:
                open(indices[idx] + 1)


def search_by_tags(tags: list):
    res = api.search_by_tags(config['papers_path'], tags)
    indices = [r[0] for r in res]
    infos = [r[1] for r in res]
    # _print_papers(indices, infos)

def info(idx: int):
    paper = api._papers_info(config['papers_path'], dirpath=True)[idx - 1]
    print(Path(paper['dirpath'] / '.info').read_text())

def infos(indices: List[int]):
    papers = [api._papers_info(config['papers_path'], dirpath=True)[idx - 1] for idx in indices]
    infos = [yaml.load(Path(paper['dirpath'] / '.info').read_text(), Loader=yaml.BaseLoader) for paper in papers]
    print(yaml.dump(infos))

def go(idx: str, echo: bool):
    papers = api._papers_info(config['papers_path'], dirpath=True)
    if idx.isdecimal():
        idx = int(idx)
        if 1 <= idx < len(papers) + 1:
            paper = papers[idx - 1]
        else:
            exit('No such paper')
    else:
        try:
            paper = next(paper for paper in papers if paper['paper_id'] == idx)
        except StopIteration:
            exit('No such paper')
    if echo:
        print (f'cd {paper["dirpath"]};')
        print (f'''echo "Entering {Path(paper['dirpath']).name} ({paper['title']})";''')
    else:
        subprocess.run(['gnome-terminal', '--tab', f'--working-directory={Path(paper["dirpath"]).as_posix()}'])

def open(idx: int):
    paper = api._papers_info(config['papers_path'], dirpath=True)[idx - 1]
    pdfpath = pdfpath_from_dir(paper['dirpath'], paper['paper_id'])
    if not pdfpath:
        print ('I was not able to guess the paper.')
    else:
        subprocess.Popen([config['pdf_viewer'], pdfpath.as_posix()])

def new():
    utils.InfoFile.new_file(pkgpath / config['info_filename'])
    subprocess.run([config['editor'], pkgpath / config['info_filename']])
    info = yaml.load((pkgpath / config['info_filename']).read_text(), yaml.BaseLoader)
    retcode, path = api.new(info, config['papers_path'])
    if retcode == 0:
        print (f'Papers created in {path}')
        utils.InfoFile.restore_info(pkgpath / config['info_filename'])
    else:
        print (f'Error when trying to create the paper (error {retcode})')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Papers management')
    parser.add_argument('expr', nargs='?', help='expression to search in the papers')
    parser.add_argument('--here', action='store_true', help='run the program with the current directory as root')
    new_group = parser.add_argument_group()
    new_group.add_argument('--tags', '-t', nargs='+', help='search by tags (tags must be separated by a blank space)')
    new_group.add_argument('--info', '-i', type=int, help='print info of the given paper (index expected)')
    new_group.add_argument('--infos', type=int, nargs='+', help='print infos of the given papers (list of indices expected)')
    new_group.add_argument('--go', '-g', help='change directory to the given paper (index or paper id expected)')
    new_group.add_argument('--echo', '-e', action='store_true', default=False, help='display the command to change directory instead of opening a new tab')
    new_group.add_argument('--open', '-o', type=int, nargs='+', help='open the given paper(s)')
    parser.add_argument('--new', '-n', action='store_true', help='add a new paper to the list')

    args = parser.parse_args()
    if args.here:
        config['papers_path'] = Path('.').as_posix()

    if args.expr:
        search(args.expr)
    elif args.tags:
        search_by_tags(args.tags)
    elif args.info:
        info(args.info)
    elif args.infos:
        infos(args.infos)
    elif args.go != None:
        go(args.go, args.echo)
    elif args.open != None:
        for pap in args.open:
            open(pap)
    elif args.new:
        new()
    else:
        search('')
