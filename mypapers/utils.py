from copy import deepcopy
from pathlib import Path
import requests
from string import ascii_letters, digits
import yaml

import bibtexparser
import clipboard



pkgpath = Path(__file__).parent
config = yaml.load((pkgpath / 'config.yml').read_text(), Loader=yaml.BaseLoader)


class InfoFile(dict):
    empty_info = {'title': '', 'authors': [], 'tags': [], 'url': '', 'year': '', 'paper_id': ''}

    @classmethod
    def new_file(cls, path: str, restore=True, cb=True):
        info = deepcopy(cls.empty_info)
        try:
            if restore:
                info.update(yaml.load(Path(path).read_text(), yaml.BaseLoader))
            if cb:
                info.update(InfoFile.parse_bibtex(clipboard.paste()))
        except Exception:
            pass
        info.update({'paper_id': InfoFile.get_paper_id(info['authors'], info['year'], info['title'])})
        Path(path).write_text(yaml.dump(info))

    @classmethod
    def restore_info(cls, path: str):
        Path(path).write_text(yaml.dump(cls.empty_info))

    @staticmethod
    def parse_authors(authors: str) -> list:
        if ' and\n' in authors:
            return authors.split(' and\n')
        else:
            return authors.split(' and ')

    @staticmethod
    def parse_bibtex(bibtex: str) -> dict:
        if hasattr(bibtexparser, 'loads'):
            btp_load = bibtexparser.loads
        else:
            btp_load = bibtexparser.parse_string
        entries = btp_load(bibtex).entries_dict.values()
        if entries:
            entries = list(entries)[0]
            return {
                'title': entries['title'],
                'authors': InfoFile.parse_authors(entries['author']),
                'year': entries['year']
            }
        else:
            return {}

    @staticmethod
    def get_paper_id(authors: list, year: str, title: str) -> str:
        if len(authors) == 1:
            lastnames = authors[0].split()[-1]
        else:
            lastnames = ''.join(a.split()[-1][0].upper() for a in authors)
        firstword = [w for w in title.split() if w not in 'and but for or nor the a an to as'.split()][0]
        return f'{lastnames}{year}{firstword}'

    @staticmethod
    def parse_paper_id(paper_id: str):
        types = ['l' if c in ascii_letters else 'd' if c in digits else '?' for c in paper_id]
        if not types or 'l' not in types or 'd' not in types:
            return False
        idx_date_begin = types.index('d')
        if 'l' not in types[idx_date_begin:]:
            return False
        idx_name_begin = idx_date_begin + types[idx_date_begin:].index('l')
        authors = paper_id[:idx_date_begin]
        date = paper_id[idx_date_begin:idx_name_begin]
        name = paper_id[:idx_name_begin]
        return authors, date, name

def download_paper(url: str) -> bytes:
    if 'arxiv' in url:
        url = url.replace('abs', 'pdf')
    elif 'eprint' in url:
        url += '.pdf'
    else:
        return b''

    try: resp = requests.get(url)
    except Exception: return b''

    if resp.status_code == 200:
        return resp.content
    else:
        return b''


def pdfpath_from_dir(dirpath: str, paper_id: str) -> str:
    ''' Return the most probable path of the pdf file in a given directory.
    '''
    pdf_files = [path.stem for path in Path(dirpath).iterdir() if path.suffix == '.pdf']
    if paper_id in pdf_files:
        file = pdf_files[pdf_files.index(f'{paper_id}')]
    elif len(pdf_files) == 1:
        file = pdf_files[0]
    else:
        return ''
    return Path(dirpath) / (file + '.pdf')
