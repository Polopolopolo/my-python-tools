from pathlib import Path
from typing import List, Dict
import shutil
import yaml

from . import utils
from myutils.iterable import split


def _papers_info(root: str, dirpath=False) -> List[Dict]:
    ''' Return the list of all .info files (as dict) in a `root` folder  
    If `dirpath=True`, add the directory path of each file to its .info dict
    '''
    papers = []
    for f in Path(root).rglob('*.info'):
        paper = yaml.load(f.read_text(), yaml.BaseLoader)
        if dirpath:
            paper['dirpath'] = f.parent
        papers.append(paper)
    return papers

def search(root: str, expr: str) -> List:
    ''' Return all papers (as info dict) matching `expr`
    '''
    matches = []
    for i, info in enumerate(_papers_info(root)):
        if expr.lower() in str(info).lower():
            matches.append((i, info))
    return matches

def search_infos(root: str) -> List:
    ''' Return all papers (as info dict) found in .infos files
    '''
    infos = []
    for f in Path(root).rglob('*.infos'):
        infos += yaml.load(f.read_text(), yaml.BaseLoader)
    return infos

def search_by_tags(root: str, tags: List):
    ''' Return all papers that match at least one tag of `tags`
    '''
    matches = []
    for i, info in enumerate(_papers_info(root)):
        for pap_tag in info['tags']:
            if any(tag.lower() == pap_tag.lower() for tag in tags):
                matches.append((i, info))
    return matches

# def search_by_field(root: str, field: str, value: str):
#     ''' Return all papers that match at least one tag of `tags`
#     '''
#     matches = []
#     for i, info in enumerate(_papers_info(root)):
#         for pap_tag in info['tags']:
#             if any(tag.lower() == pap_tag.lower() for tag in tags):
#                 matches.append((i, info))
#     return matches

# def search_by_expr(root: str, expr: str):
#     words = expr.split()
#     clauses = split(words, 'OR')
#     clauses = [split(clause, 'AND') for clause in clauses]
#     filters = [[split(cl, '=') for cl in clause] for clause in clauses]
#     for and_clause in filters:
#         for filter in and_clause:
#             try:
#                 field, value = filter[0][0], ' '.join(filter[1:][0])
#             except IndexError:
#                 return
#             else:
                
#                 import ipdb; ipdb.set_trace()

def new(info: dict, root: str) -> str:
    ''' Create a new paper given an `info` dict
    - requires valid 'Path' or 'Url' in `info`
    - creates a folder inside `root` if it does not already exist
    - return the name of the new folder or `empty string` if abort
    '''
    if 'path' in info and info['path']:
        bcontent = Path(info['path']).read_bytes()
    elif 'url' in info and info['url']:
        bcontent = utils.download_paper(info['url'])
        if not bcontent:
            return 1, ''
    else:
        return 2, ''

    if 'id' not in info or not info['id']:
        info['id'] = utils.InfoFile.get_paper_id(info['authors'], info['year'], info['title'])
    pap_dir = Path(root) / info['id']
    pap_dir.mkdir()
    try:
        (pap_dir / info['id']).with_suffix('.pdf').write_bytes(bcontent)
        (pap_dir / '.info').write_text(yaml.dump(info))
        return 0, pap_dir
    except Exception:
        shutil.rmtree(pap_dir)
        return 3, ''
