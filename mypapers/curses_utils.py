import curses

def display_menu(stdscr, options):
    curses.curs_set(0)  # Hide the cursor
    current_row = 0
    curses.init_pair(1, 6, 0)

    indices = set()
    while True:
        stdscr.clear()
        height, width = stdscr.getmaxyx()

        for idx, row in enumerate(options):
            x = 0
            y = idx
            if idx in indices:
                row = row.replace('-', '+', 1)
            if idx == current_row:
                stdscr.attron(curses.color_pair(1))
                stdscr.addstr(y, x, row)
                stdscr.attroff(curses.color_pair(1))
            else:
                stdscr.addstr(y, x, row)

        stdscr.refresh()

        key = stdscr.getch()

        if key == curses.KEY_UP and current_row > 0:
            current_row -= 1
        elif key == curses.KEY_DOWN and current_row < len(options) - 1:
            current_row += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:
            return indices.union([current_row])
        elif key == ord('+'):
            indices.add(current_row)
        elif key == ord('-'):
            indices.discard(current_row)
        elif key in [27, 4]:  # Escape, or CTRL-D to exit
            return None

def run(options):
    return curses.wrapper(lambda stdscr: display_menu(stdscr, options))
