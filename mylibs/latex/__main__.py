###########
# IMPORTS #
###########

import argparse
import json
import os
import shutil
import subprocess
import shutil


###########
# GLOBALS #
###########

TEMPLATES_PATH = "templates"

# to be removed in futur version
TEX_TEMPLATE = "templates/cryptopaper/template.tex"
TIKZ_FIG_PATH = "templates/cryptopaper/tikz-figures"
CMD_PATH = "templates/cryptopaper/commands"
MAKEFILE_PATH = "templates/cryptopaper/Makefile"


#########
# UTILS #
#########


def abspath(*path):
    return os.path.join(os.path.split(__file__)[0], *path)


def read_config():
    with open(abspath("config.json")) as f:
        config = json.load(f)
    return config


def write_config(config):
    with open(abspath("config.json"), "w") as f:
        json.dump(config, f, indent=2)


def flatten_diff(s1, s2, window_len=10):
    for (i, (c1, c2)) in enumerate(zip(s1, s2)):
        if c1 != c2:
            print (f"Difference at index {i}:\n"
                    "First:  {s1[i-window_len//2 : i+window_len//2]}"
                    "Second: {s2[i-window_len//2 : i+window_len//2]}")
            return True
    return False


def get_inside_brackets(s: str):
    assert s.startswith("{")
    cpt = 0
    for (i, c) in enumerate(s):
        if c == "{":
            cpt += 1
        elif c == "}":
            cpt -= 1
        if cpt == 0:
            break
    assert cpt == 0
    return s[1:i]


def comments_block_title(s, comment_char = "%"):
    n = len(s)
    return "\n".join([
        comment_char * (n + 4),
        "% " + s + " %",
        comment_char * (n + 4)
    ])


#######
# API #
#######

def prettify(tex: str):
    par_indices = {}
    lines = tex.splitlines()
    for (i, l) in enumerate(lines):
        if l.find("\\paragraph") != -1:
            idx = l.index("\\paragraph")
            par_indices[i] = get_inside_brackets(l[idx + len("\\paragraph"):])
    out = []
    if not par_indices:
        out = lines
    else:
        cur_step = 0
        for k, v in par_indices.items():
            out += lines[cur_step:k]
            cur_step = k
            out += [comments_block_title(v), "\n"]
        out += lines[cur_step:]
    return "\n".join(out)

def list_templates():
    return os.listdir(abspath(TEMPLATES_PATH))

# to be removed in futur version
def new_document():
    with open(abspath(TEX_TEMPLATE)) as f:
        templ = f.read()
    return templ


def detect_format(text: str):
    lines = text.splitlines() + [""]
    line_analysis = [(False, "")]
    for i, l in enumerate(lines[:-1]):

        if  l.strip()\
            and len(l) <= 80\
            and (not l.strip().startswith("\\") or line_analysis[-1][0])\
            and not l.strip().endswith("."):

            line_analysis.append((True, l))

        else:
            line_analysis.append((False, l))

    return line_analysis[1:]


def format_text(text: str):
    formatted_lines = []
    lines = text.splitlines()
    for l in lines:
        # ignore the comment, need to be improved: in the current version: a commented line can be as long as we want
        if l.strip().startswith("%"):
            continue
        l = l.rstrip()
        if len(l) <= 80:
            formatted_lines.append(l)
        else:
            words = l.split(" ")
            current_words = []
            for w in words:
                if sum(len(cw) for cw in current_words) +\
                        len(current_words) + len(w) > 80:
                    formatted_lines.append(" ".join(current_words))
                    current_words = [w]
                else:
                    current_words.append(w)
            formatted_lines.append(" ".join(current_words))
    return "\n".join(formatted_lines)


def unformat_text(text_analysis: list):
    unformatted_lines = []
    new_line = ""
    i = 0
    while i < len(text_analysis) - 1:
        (b, l) = text_analysis[i]
        next_l = text_analysis[i + 1][1]
        while b:
            delimiter = "" if l.endswith(" ") or next_l.startswith(" ") else " "
            new_line += l + delimiter
            # unformatted_lines.append(l + delimiter + next_l)
            i += 1
            (b, l) = text_analysis[i]
        else:
            new_line += l
            unformatted_lines.append(new_line)
            new_line = ""
        i += 1
    return "\n".join(unformatted_lines)


#############
# CALLBACKS #
#############

def prettify_cb(args):
    with open(args.filepath_in) as f_in:
        tex = f_in.read()
    out = prettify(tex)
    with open(args.filepath_out, "w") as f_out:
        f_out.write(out)

def list_templates_cb(args):
    tmpls = list_templates()
    print (*tmpls, sep="\n")

def new_cb(args):
    if args.template not in os.listdir(abspath(TEMPLATES_PATH)):
        exit("This template does not exist. Aborting...")
    else:
        with open(abspath(TEMPLATES_PATH, args.template, ".config")) as f:
            cfg = json.load(f)
        for fp in cfg["copy"]:
            shutil.copy(abspath(TEMPLATES_PATH, args.template, fp), fp)
        for fp in cfg["symlink"]:
            os.symlink(abspath(TEMPLATES_PATH, args.template, fp), fp)


    # # to be removed in futur version
    # if args.template == "cryptopaper":
    #     shutil.copy(abspath(TEX_TEMPLATE), args.filepath)
    #     os.symlink(abspath("templates/cryptopaper/llncs-overlay.cls"), "llncs-overlay.cls")
    #     args.filename = args.filepath
    #     build_makefile_parser_cb(args)
    #     os.symlink(abspath(CMD_PATH), "commands")
    # else:
    #     dirpath = os.path.join(TEMPLATES_PATH, args.template)
    #     for x in os.listdir(abspath(dirpath)):
    #         path = os.path.join(abspath(dirpath), x)
    #         if os.path.isdir(path):
    #             shutil.copytree(path, x)
    #         else:
    #             shutil.copy(path, ".")


def build_makefile_parser_cb(args):
    prefix = os.path.splitext(args.filename)[0]
    with open(abspath(MAKEFILE_PATH)) as f:
        content = f.read()
    with open("Makefile", "w") as f:
        f.write(content.format(prefix=prefix))


def add_lib_cb(args):
    config = read_config()
    if args.name not in config["libraries"]:
        config["libraries"].append(args.name)
        config["libraries"] = sorted(config["libraries"])
    if args.options:
        options = set(config["library_options"][args.name]) if args.name in config["library_options"] else set()
        for opt in args.options:
            options.add(opt)
        config["library_options"][args.name] = sorted(list(options))
    write_config(config)


def format_cb(args):
    with open(args.input, encoding="utf-8") as f:
        content = f.read()
    # before_len = len(content.replace(" ", "").replace("\n", ""))
    before = content.replace(" ", "").replace("\n", "")
    f_text = format_text(content)
    # after_len = len(f_text.replace(" ", "").replace("\n", ""))
    after = f_text.replace(" ", "").replace("\n", "")
    # if before_len == after_len:
    #     print ("Simplified lengths are the same, writing to output file.")
    diff = flatten_diff(before, after)
    if not diff:
        print ("Flattened contents are the same, writing to output file.")
    else:
        while "response is not y or n":
            resp = input(f"Flattened contents are not the same, do you want to continue ? [y/n]")
            # resp = input(f"Simplified lengths are not the same ({before_len} =/= {after_len}), do you want to continue ? [y/n]")
            if resp.lower() == "n":
                exit()
            elif resp.lower() == "y":
                break

    with open(args.output, "w", encoding="utf-8") as f:
        f.write(f_text)


def unformat_cb(args):
    with open(args.input, encoding="utf-8") as f:
        content = f.read()
    line_analysis = detect_format(content)
    uf_text = unformat_text(line_analysis)

    # before_len = len(content.replace(" ", "").replace("\n", ""))
    # after_len = len(uf_text.replace(" ", "").replace("\n", ""))
    # if before_len == after_len:
    #     print ("Simplified lengths are the same, writing to output file.")
    # else:
    #     while "response is not y or n":
    #         resp = input(f"Simplified lengths are not the same ({before_len} =/= {after_len}), do you want to continue ? [y/n]")
    #         if resp.lower() == "n":
    #             exit()
    #         elif resp.lower() == "y":
    #             break

    before = content.replace(" ", "").replace("\n", "")
    f_text = format_text(content)
    after = f_text.replace(" ", "").replace("\n", "")
    diff = flatten_diff(before, after)
    if not diff:
        print ("Flattened contents are the same, writing to output file.")
    else:
        while "response is not y or n":
            resp = input(f"Flattened contents are not the same, do you want to continue ? [y/n]")
            if resp.lower() == "n":
                exit()
            elif resp.lower() == "y":
                break

    with open(args.output, "w", encoding="utf-8") as f:
        f.write(uf_text)


def open_pdf_cb(args):
    if args.filename:
        filename = args.filename
    else:
        pdfs = [f for f in os.listdir(".") if os.path.splitext(f)[1] == ".pdf"]
        if len(pdfs) > 1:
            exit("Error: cannot detect the pdf file to open, please use --filename")
        elif not pdfs:
            exit("Error: there is no pdf in this directory")
        else:
            filename = pdfs[0]
    filepath = abspath("workspace", filename)
    subprocess.Popen([args.pdf_reader, filepath])


def tikz_fig_cb(args):
    names = [os.path.splitext(fn)[0] for fn in os.listdir(abspath(TIKZ_FIG_PATH))]
    if args.list:
        print ("# Available tikz figures:")
        print (*names, sep="\n")
    else:
        if args.name not in names:
            exit("This name does not exist, use --list to show the available names")
        else:
            shutil.copy2(os.path.join(abspath(TIKZ_FIG_PATH), args.name) + ".tex", args.out)


########
# MAIN #
########

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Latex functionalities")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    prettify_parser = subparsers.add_parser("prettify", help="Prettify a tex source document.")
    prettify_parser.add_argument("filepath_in")
    prettify_parser.add_argument("filepath_out")

    list_template_parser = subparsers.add_parser("list-templates", aliases=["lt"], help="List the avaimable templates.")
    list_template_parser.set_defaults(callback=list_templates_cb)

    templates = os.listdir(abspath(TEMPLATES_PATH))
    new_parser = subparsers.add_parser("new", help="Build a new latex document")
    new_parser.add_argument("template", help=f"Template to be used, possible choices are {', '.join('`' + tmpl + '`' for tmpl in templates)}")
    new_parser.set_defaults(callback=new_cb)

    build_makefile_parser = subparsers.add_parser("makefile", help="Create a Makefile for cleaning latex files")
    build_makefile_parser.add_argument("filename", help="Filename of the .tex file")

    build_commands_parser = subparsers.add_parser("commands", help="Create a command files with helpful commands")
    build_commands_parser.add_argument("dirpath", default=".", help="Where to put the command files")
    build_commands_parser.add_argument("--list", action="store_true", help="List availables command files")
    build_commands_parser.add_argument("--commands", default="all", help="Commands files to add. Specified by a sequence of indices (see --list option), default = all")

    add_lib_parser = subparsers.add_parser("add-lib", help="Add a new library with optional options to the default tex file")
    add_lib_parser.add_argument("name", help="Name of the library to add")
    add_lib_parser.add_argument("--options", nargs="+", help="Options of the library to add")

    format_parser = subparsers.add_parser("format", help="Format a LaTex file.")
    format_parser.add_argument("input", help="File to be unformatted")
    format_parser.add_argument("output", help="Output file")

    unformat_parser = subparsers.add_parser("unformat", help="Unformat a LaTex file.")
    unformat_parser.add_argument("input", help="File to be unformatted")
    unformat_parser.add_argument("output", help="Output file")

    open_pdf_parser = subparsers.add_parser("open-pdf", help="Open the generated pdf file (located in the workspace)")
    open_pdf_parser.add_argument("--pdf-reader", default="okular", help="Pdf reader to be used")
    open_pdf_parser.add_argument("--filename", help="Specify a filename to read (by default: detect it by looking in the current folder")

    tikz_fig_parser = subparsers.add_parser("tikz-fig", help=f"Generate one of the tikz template stored in {abspath(TIKZ_FIG_PATH)}")
    tikz_fig_parser.add_argument("name", help="Name of the figure to be added")
    tikz_fig_parser.add_argument("--out", default=".", help="Dirpath to put the figure (default '.')")
    tikz_fig_parser.add_argument("--list", action="store_true", help="List all available tikz figures")

    args = parser.parse_args()
    if args.command in ("list-templates", "lt"):
        list_templates_cb(args)
    elif args.command == "new":
        new_cb(args)
    elif args.command == "makefile":
        build_makefile_parser_cb(args)
    elif args.command == "add":
        add_lib_cb(args)
    elif args.command == "format":
        format_cb(args)
    elif args.command == "unformat":
        unformat_cb(args)
    elif args.command == "open-pdf":
        open_pdf_cb(args)
    elif args.command == "tikz-fig":
        tikz_fig_cb(args)
    elif args.command == "prettify":
        prettify_cb(args)
    else:
        parser.print_usage()

