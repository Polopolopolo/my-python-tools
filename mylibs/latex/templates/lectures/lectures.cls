\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lectures}

\LoadClass{article}

% Packages
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage[utf8]{inputenc}
\RequirePackage{fancyhdr}
\RequirePackage{systeme}

% Page Style
\pagestyle{fancy}

% Counters
\newcounter{exo}
\newcounter{subexo}
\newcounter{subsubexo}

% Exercices Commands
\newcommand{\exo}{\stepcounter{exo} \setcounter{subexo}{0} \setcounter{subsubexo}{0} \subsection*{Exercice \theexo}}
\newcommand{\subexo}{\stepcounter{subexo} \setcounter{subsubexo}{0} \paragraph{\thesubexo)}}
\newcommand{\subexoalph}{\stepcounter{subexo} \setcounter{subsubexo}{0} \paragraph{\alph{subexo})}}
\newcommand{\subsubexo}{\stepcounter{subsubexo} \paragraph{\thesubexo.\thesubsubexo)}}
\newcommand{\subsubexoalph}{\stepcounter{subsubexo} \paragraph{\thesubexo.\alph{subsubexo})}}

\newcommand{\colvec}[3]{
	\begin{pmatrix}
		 #1\\
		 #2\\
		 #3
	\end{pmatrix}
}

\newcommand{\colvectwo}[2]{
	\begin{pmatrix}
		#1\\
		#2
	\end{pmatrix}
}


\newcommand{\intervalleoo}[2]{\mathopen{]}#1\,,#2\mathclose{[}}
\newcommand{\intervallecc}[2]{\mathopen{[}#1\,,#2\mathclose{]}}
\newcommand{\intervalleoc}[2]{\mathopen{]}#1\,,#2\mathclose{]}}
\newcommand{\intervalleco}[2]{\mathopen{[}#1\,,#2\mathclose{[}}
\newcommand{\intervalle}[2]{\mathopen{(}#1\,,#2\mathclose{)}}

\newcommand{\hint}[1]{\paragraph*{}\fbox{\begin{minipage}{\linewidth}
			#1
\end{minipage}}}


\newcommand{\RR}{\mathbb{R}}
