\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{llncs-overlay}

\LoadClass[]{llncs}
\pagestyle{plain}

% Display contents navigation panel properly
\usepackage{etoolbox}
\makeatletter
\let\llncs@addcontentsline\addcontentsline
\patchcmd{\maketitle}{\addcontentsline}{\llncs@addcontentsline}{}{}
\patchcmd{\maketitle}{\addcontentsline}{\llncs@addcontentsline}{}{}
\patchcmd{\maketitle}{\addcontentsline}{\llncs@addcontentsline}{}{}
\setcounter{tocdepth}{2}
\makeatother
\usepackage{hyperref}
\usepackage{bookmark}

% Construction (maybe move it to commands)
\spnewtheorem{construction}{Construction}{\bfseries}{\itshape}

% change BIT and BEN items
\RequirePackage{enumitem}
\setlist[itemize,1]{label=\(\bullet\)}
\setlist[itemize,2]{label=\(-\)}
\setitemize{itemsep=0.5em}
\setenumerate{itemsep=0.5em}

% Change paragraph format
\let\llncssubparagraph\subparagraph
\let\subparagraph\paragraph
\RequirePackage{titlesec}
\let\subparagraph\llncssubparagraph
\titleformat*{\paragraph}{\normalsize\bfseries}

% Microtype package to improve the look of the rendering
\RequirePackage{microtype}
\microtypesetup{
  final,        % always activate, even when in draft mode
  activate={true,nocompatibility},  % activate protrusion and expansion
  kerning=true,
  spacing=true,
  protrusion=true
  % more info on config to be found eg at http://www.khirevich.com/latex/microtype/
}
\microtypecontext{spacing=nonfrench}

% Useful packages
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{braket}
\RequirePackage[noabbrev,capitalize,nameinlink]{cleveref} % name of figure with 'cref' command
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{stmaryrd}
\usepackage{tcolorbox}
\usepackage{tikz}
\usetikzlibrary{matrix,decorations.pathreplacing,calc,fit,backgrounds,positioning}
\usepackage{varwidth}
\usepackage{xcolor}

% Cref
\Crefname{construction}{Construction}{Constructions}

% Customize boxed environment
\NewDocumentEnvironment{progbox}{ O{} +b }{%
  \centering
    \begin{tcolorbox}[arc=0pt,outer arc=0pt,colframe=black,colback=white,
      boxrule=0.4pt,left=0pt,right=0pt,top=0pt,bottom=0pt,hbox]
      \begin{varwidth}[t]{0.8\textwidth}
        #2%
      \end{varwidth}
    \end{tcolorbox}
    \caption{#1}
}{}

\endinput
