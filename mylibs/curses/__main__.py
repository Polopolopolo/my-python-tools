import curses
import curses.textpad
import logging
import os
import traceback

from .app import App
from .widgets.button_widget import ButtonWidget
from .widgets.text_widget import TextWidget
from .widgets.scroll_widget import ScrollWidget
from .screen import Screen
from .engines.simple_engine import SimpleEngine
from .engines.scroll_engine import ScrollEngine


class ColorButtonWidget(ButtonWidget):
    def __init__(self, parent, name, top, left, text=""):
        super().__init__(parent, name, top, left, text)
        self.is_emph = False

    def on_pressed(self, ch):
        self.parent.widgets["txt1"].emph = not self.parent.widgets["txt1"].emph
        return super().on_pressed(ch)


class ChangeScreenButton(ButtonWidget):
    def __init__(self, parent, name: str, top: int, left: int, next_screen: str):
        super().__init__(parent, name, top, left, f"Go to screen `{next_screen}`")
        self.next_screen = next_screen

    def on_pressed(self, ch):
        self.app.set_current_screen("screen2")
        return super().on_pressed(ch)


class PokeScroll(ScrollWidget):
    def on_pressed(self, ch):
        if self.items[self.current_item].lower() == "bulbizarre":
            self.app.set_current_screen("screen")


if __name__ == "__main__":
    logging.basicConfig(filename="log.log", filemode="w", level=0)
    logging.info("Start log session")

    try:
        app = App()
        screen = Screen(app, "screen")

        txt1 = TextWidget(screen, "txt1", 1, 0, "Curses overlay.")
        screen.append_widget(txt1)

        txt2= TextWidget(screen, "txt2", 3, 0, "This library provides a set of classes and methods\ndesigned to ease the development of TUI through `curses`.\n", style={"color": "cyan on black"})
        screen.append_widget(txt2)

        btn1 = ColorButtonWidget(screen, "btn1", txt2.bottom + 1, 10, "Press me !")
        screen.append_widget(btn1)

        btn2 = ChangeScreenButton(screen, "btn2", btn1.bottom + 2, 0, "screen2")
        screen.append_widget(btn2)

        engine = SimpleEngine(screen, "engine")
        screen.add_engine(engine)

        app.append_screen(screen)

        screen2 = Screen(app, "screen2")
        app.append_screen(screen2)

        screen2.append_widget(TextWidget(screen2, "txt3", 0, 0, "Another screen"))

        scroll = PokeScroll(screen2, "scroll", 2, 0, app.height - 3)
        with open(os.path.join(os.path.split(__file__)[0], "pokedex.txt")) as f:
            lines = f.read().splitlines()
        scroll.append_items(lines)
        screen2.append_widget(scroll)
        engine2 = ScrollEngine(screen2, "engine2", scroll)
        screen2.add_engine(engine2)

        app.run()

    except Exception as err:
        tb = "\n".join(traceback.format_tb(err.__traceback__))
        logging.critical(f"""The application ended with the following error: {err.__class__.__name__}, {err.args}
Trace:
{tb}""")

    finally:
        curses.endwin()

    logging.info("End log session")
