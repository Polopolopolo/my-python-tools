import curses
import curses.textpad
import logging
from typing import List

from ..screen import Screen
from .engine import Engine


class SimpleEngine(Engine):
    def __init__(self, screen: Screen, name: str) -> None:
        super().__init__(screen, name)
        self._cursor = 0
        self.compute_selectables()

    @property
    def cursor(self):
        return self._cursor

    @cursor.setter
    def cursor(self, val):
        self.navigables[self.cursor].selected = False
        self._cursor = val
        self.navigables[self.cursor].selected = True

    def compute_selectables(self):
        self.navigables = [w for w in self.screen.widgets.values() if w.navigable]

    def handle_input(self, ch: str):
        if not self.navigables:
            return
        else:
            self.navigables[self.cursor].selected = False
            if ch == curses.KEY_DOWN and self.cursor < len(self.navigables) -1:
                self.cursor += 1
            elif ch == curses.KEY_UP and self.cursor > 0:
                self.cursor -= 1
            self.navigables[self.cursor].selected = True
