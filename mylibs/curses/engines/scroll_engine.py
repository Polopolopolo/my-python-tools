import curses
import curses.textpad
import logging
from typing import List

from ..screen import Screen
from ..widgets.scroll_widget import ScrollWidget
from .engine import Engine


class ScrollEngine(Engine):
    def __init__(self, screen: Screen, name: str, scroll_widget: ScrollWidget) -> None:
        super().__init__(screen, name)
        self.cursor = 0
        self.scroll_widget = scroll_widget

    def handle_input(self, ch: str):
        if ch == curses.KEY_DOWN:
            if self.cursor < self.scroll_widget.height - 1:
                self.cursor += 1
            elif self.scroll_widget.top_idx + self.scroll_widget.height < len(self.scroll_widget.items):
                self.scroll_widget.top_idx += 1
        elif ch == curses.KEY_UP:
            if self.cursor > 0:
                self.cursor -= 1
            elif self.scroll_widget.top_idx > 0:
                self.scroll_widget.top_idx -= 1
        self.scroll_widget.current_item = self.scroll_widget.top_idx + self.cursor
