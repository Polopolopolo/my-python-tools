import curses
import curses.textpad
import logging
from typing import List

from ..screen import Screen
from ..widgets.widget import Widget2


class Engine:
    def __init__(self, screen: Screen, name: str) -> None:
        self.screen = screen
        self.name = name

    def handle_input(self, ch: str):
        pass
