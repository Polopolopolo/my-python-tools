from .widgets.widget import Widget2


class Screen(Widget2):
    def __init__(self, parent, name, engine=None):
        super().__init__(parent, name, 0, 0, navigable=False, pressable=False)
        self.engine = engine

    def add_engine(self, engine):
        self.engine = engine

    def on_entered(self):
        pass

    def handle_input(self, ch):
        if self.engine:
            self.engine.handle_input(ch)
        return super().handle_input(ch)
