from .widget import Widget2


class TextWidget(Widget2):
    def __init__(self, parent, name: str, top: int, left: int, text="", **kwargs):
        super().__init__(parent, name, top, left, navigable=False, pressable=False, **kwargs)
        self._text = text
        self.recompute_shape()

    @property
    def top(self):
        return super().top

    @top.setter
    def top(self, val):
        self._top = val
        self.recompute_shape()

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, val):
        self._text = val
        self.recompute_shape()

    def recompute_shape(self):
        self.bottom = self.top + len(self.text.splitlines()) - 1
        self.right = self.left + max(len(l) for l in self.text.splitlines()) if self.text else self.left

    def display(self):
        for (i, l) in enumerate(self.text.splitlines()):
            self.addstr(i, 0, l)
        return super().display()
