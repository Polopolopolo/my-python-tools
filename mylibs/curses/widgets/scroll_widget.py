import curses
import logging
from typing import Iterable

from .widget import Widget2


class ScrollWidget(Widget2):
    def __init__(self, parent, name, top, left, bottom, trigger_button=(curses.KEY_ENTER, 10, 13)):
        super().__init__(parent, name, top, left, navigable=False, pressable=False)
        self.bottom = bottom
        self.trigger_button = list(trigger_button) if isinstance(trigger_button, Iterable) else [trigger_button]
        self.items = []
        self.top_idx = 0
        self.current_item = 0

    def append_item(self, s: str):
        self.items.append(s)

    def append_items(self, l: Iterable[str]):
        for s in l:
            self.items.append(s)

    def display(self):
        for (i, it) in enumerate(self.items[self.top_idx:self.top_idx + self.height]):
            self.addstr(i, 0, it) if self.top_idx + i != self.current_item else self.addstr(i, 0, it, color=self.app.color("black on white"))
        return super().display()

    def handle_input(self, ch):
        if ch in self.trigger_button:
            self.on_pressed(ch)
