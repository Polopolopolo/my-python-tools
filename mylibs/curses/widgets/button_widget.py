import curses
import logging

from typing import Iterable

from .text_widget import TextWidget


class ButtonWidget(TextWidget):
    def __init__(self, parent, name: str, top: int, left: int, text="",
                trigger_button=(curses.KEY_ENTER, 10, 13)):
        super().__init__(parent, name, top, left, text)
        self.navigable = True
        self.pressable = True
        self.trigger_button = list(trigger_button) if isinstance(trigger_button, Iterable) else [trigger_button]

    def handle_input(self, ch):
        if self.selected and ch in self.trigger_button:
            self.on_pressed(ch)
        return super().handle_input(ch)
