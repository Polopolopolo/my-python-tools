import curses
from string import printable

from .text_widget import TextWidget


class TextEditWidget(TextWidget):
    def __init__(self, parent, name: str, top: int, left: int, placeholder="", **kwargs):
        super().__init__(parent, name, top, left, placeholder, **kwargs)
        self.navigable = True

    def handle_input(self, ch):
        if self.selected:
            if chr(ch) in printable:
                self.text += chr(ch)
            elif ch == curses.KEY_BACKSPACE:
                self.text = self.text[:-1]
