from ..app import App
from ..utils import load_style


class Widget:
    def __init__(self, parent, name):
        self.parent = parent
        self.name = name
        self.widgets = {}

    @property
    def app(self):
        curr = self
        while not isinstance(curr, App):
            curr = curr.parent
        return curr

    @property
    def window(self):
        return self.app.window

    def append_widget(self, widget):
        if widget.name in self.widgets:
            exit(f"Error: widget {widget.name} already in {self.name}'s children, aborting.")
        self.widgets[widget.name] = widget

    def handle_input(self, ch):
        for w in self.widgets.values():
            w.handle_input(ch)

    def display(self):
        for w in self.widgets.values():
            w.display()


class Widget2(Widget):
    def __init__(self, parent, name: str, top: int, left: int,
                navigable: bool, pressable: bool, **kwargs):
        super().__init__(parent, name)
        self._top = top
        self.left = left
        self.bottom = None
        self.right = None

        # flags
        self.navigable = navigable
        self.selected = False
        self.pressable = pressable

        # style
        self.style = {}
        self.emph = False
        load_style(self, kwargs["style"]) if "style" in kwargs else load_style(self, {})

    @property
    def top(self):
        return self._top

    @property
    def x(self):
        if not self.parent:
            return self.left
        else:
            return self.left + self.parent.left

    @property
    def y(self):
        if not self.parent:
            return self.top
        else:
            return self.top + self.parent.top

    @property
    def height(self):
        return self.bottom - self.top + 1

    def addstr(self, _y, _x, txt, **kwargs):
        if "color" in kwargs:
            color = kwargs["color"]
        elif self.emph:
            color = self.style["emph"]
        elif self.selected:
            color = self.style["selected"]
        else:
            color = self.style["color"]
        self.window.addstr(self.y + _y, self.x + _x, txt, color)

    def set_selected(self, value: bool):
        self.selected = value
        if value:
            self.on_selected()

    def on_selected(self):
        pass

    def on_pressed(self, ch):
        pass
