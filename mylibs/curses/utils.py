import curses
import logging


def get_curses_color(s):
    return getattr(curses, f"COLOR_{s.upper()}")


def get_curses_color_pair(s):
    if len(s.split()) == 1:
        text = s.strip()
        highlight = "black"
    else:
        text, _, highlight = s.split()
    return (get_curses_color(text), get_curses_color(highlight))


def load_style(widget, style):
    widget.style["color"] = widget.app.color(style["color"]) if "color" in style else widget.app.color("white on black")
    widget.style["emph"] = widget.app.color(style["emph"]) if "emph" in style else widget.app.color("red on black")
    widget.style["selected"] = widget.app.color(style["selected"]) if "selected" in style else widget.app.color("black on white")
