import curses
import curses.textpad

from .utils import get_curses_color_pair


class App:
    def __init__(self):
        self.window = None

        self.width = 0
        self.height = 0

        self.screens = {}
        self.current_screen = None
        self.previous_screen = None

        self.init_curses()


    def init_curses(self):
        """Setup the curses"""
        self.window = curses.initscr()
        self.window.keypad(True)

        curses.noecho()
        curses.cbreak()

        curses.start_color()
        self.color_pairs = []
        curses.curs_set(0)

        self.height, self.width = self.window.getmaxyx()

    def color(self, s):
        pair = get_curses_color_pair(s)
        if pair in self.color_pairs:
            return curses.color_pair(self.color_pairs.index(pair) + 1)
        else:
            self.color_pairs.append(pair)
            curses.init_pair(len(self.color_pairs), *self.color_pairs[-1])
            return curses.color_pair(len(self.color_pairs))

    def run(self):
        """Continue running the TUI until get interrupted"""
        if not self.screens or not self.current_screen:
            exit("No screen provided, aborting.")
        try:
            self.input_stream()
        except KeyboardInterrupt:
            pass
        finally:
            curses.endwin()


    def input_stream(self):
        """Waiting an input and run a proper method according to type of input"""
        while True:
            self.window.erase()
            self.current_screen.display()
            self.window.refresh()

            ch = self.window.getch()
            if ch == 24:
                break
            else:
                self.current_screen.handle_input(ch)


    def append_screen(self, screen):
        self.screens[screen.name] = screen
        if not self.current_screen:
            self.set_current_screen(screen.name)


    def set_current_screen(self, screen_name):
        if self.current_screen:
            self.previous_screen = self.current_screen.name
        self.current_screen = self.screens[screen_name]
        self.screens[screen_name].on_entered()


    def go_to_previous_screen(self):
        if hasattr(self.current_screen, "up_screen") and self.current_screen.up_screen:
            if self.current_screen.up_screen == -1:
                self.set_current_screen(self.previous_screen)
                self.previous_screen = None
            else:
                self.set_current_screen(self.screens[self.current_screen.up_screen])
