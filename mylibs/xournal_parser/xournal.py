"""
Inspired by https://github.com/catch22/xournal-converters/blob/master/xournal_converters/pdf.py
"""


import argparse
import gzip
import logging
import os
import string
from base64 import b64decode
from io import BytesIO
from xml.etree.ElementTree import ElementTree

from PIL import Image, ImageDraw


class Element:
    def __init__(self, page, item):
        self.page = page
        self.item = item
        self.topleft = None

    def render(self):
        logging.info(f"{self.__class__.__name__} element rendered at page {self.page}: {self.topleft}")

class TextElement(Element):
    def __init__(self, page, item):
        super().__init__(page, item)
        self.topleft = (float(item.attrib["x"]), float(item.attrib["y"]))

    def render(self):
        super().render()
        return self.item.text

class HTextElement(TextElement):
    sections = ["section", "subsection", "paragraph"]

    def __init__(self, page, item, lvl):
        super().__init__(page, item)
        self.lvl = lvl

    def render(self):
        super().render()
        name = self.item.text[self.lvl:].lstrip()
        return f"\\{self.sections[self.lvl-2]}{{{name}}}"

class RessourceElement(Element):
    global_cpt = 0
    ressource_name = "ressource"

    def __init__(self, page, item):
        super().__init__(page, item)
        self.cpt = self.__class__.global_cpt
        self.image_path = f"ressources/{self.ressource_name}_{self.cpt}.png"
        self.__class__.global_cpt += 1
        self.width = 1

    def render(self):
        super().render()
        scale = 1 # TODO set correct scale factor
        # return "\n".join([
        #     "\\begin{figure}[H]",
        #     f"\\includegraphics[scale={scale}]{{{self.image_path}}}",
        #     "\\end{figure}"])
        return f"\\includegraphics[width={self.width}pt]{{{self.image_path}}}"

    def save(self): raise NotImplementedError

class ImageElement(RessourceElement):
    ressource_name = "image"

    def __init__(self, page, item):
        super().__init__(page, item)
        self.topleft = (float(item.attrib["left"]), float(item.attrib["top"]))
        botright = (float(item.attrib["bottom"]), float(item.attrib["right"]))
        width = botright[0] - self.topleft[0]
        self.width = width * 505 / 595

    def save(self):
        bimage = BytesIO(b64decode(self.item.text))
        image = Image.open(bimage)
        image.save(self.image_path)

class StrokeElement(RessourceElement):
    ressource_name = "stroke"

    def __init__(self, page, item):
        super().__init__(page, item)
        self.pts = [float(pt) for pt in self.item.text.split()]
        self.x = [self.pts[i] for i in range(0, len(self.pts), 2)]
        self.y = [self.pts[i] for i in range(1, len(self.pts), 2)]
        self.topleft = (min(self.x), min(self.y))

    def save(self):
        color = self.item.attrib["color"]
        width = round(float(self.item.attrib["width"]))

        self.botright = (max(self.x), max(self.y))
        pts = []
        for i in range(len(pts)):
            if i % 2 == 0:
                pts.append(pts[i] - self.topleft[0])
            if i % 2 == 1:
                pts.append(pts[i] - self.botright[1])

        size = (int(self.topleft[0] - self.topleft[0]) + width, int(self.botright[1] - self.botright[1]) + width)
        image = Image.new("RGBA", size)
        draw = ImageDraw.Draw(image)
        draw.line(pts, fill=color, width=width)

        image.save(self.image_path)

class Engine:
    def __init__(self, input_path, output_path):
        self.input_path = input_path
        self.output_path = output_path

        self._create_files()

        self.elements = []
        self.tex_source = []
        self.title = ""
        self.authors = ""
        self.date = ""
        self.image_cpt = 0

        self.tree = self.get_xml_tree(self.input_path)
        self.pages = self.get_pages_from_tree(self.tree)

    def _create_files(self):
        output_dirpath = os.path.split(self.output_path)[0]
        static_dirpath = os.path.join(output_dirpath, "ressources")
        os.makedirs(static_dirpath)

    @staticmethod
    def get_xml_tree(filepath):
        with gzip.open(filepath, "rb") as f:
            xml = ElementTree(file=f) 
        return xml

    @staticmethod
    def get_pages_from_tree(tree):
        return list(tree.getroot().iter("page"))


    def build_tex(self):
        for (i, page) in enumerate(self.pages):
            for layer in page.iter("layer"):
                for item in layer:
                    if item.tag == "text":
                        self._handle_text(i, item)
                    elif item.tag == "image":
                        self._handle_image(i, item)
                    elif item.tag == "stroke":
                        self._handle_stroke(i, item)
                    else:
                        logging.error(f"Can't handle tag {item.tag}")
        self.elements = sorted(self.elements, key=lambda element: (element.page,) + element.topleft[::-1])

    def _handle_text(self, page, item):
        text = item.text.lstrip()
        if text.startswith("####"):
            self.elements.append(HTextElement(page, item, 4))
        elif text.startswith("###"):
            self.elements.append(HTextElement(page, item, 3))
        elif text.startswith("##"):
            self.elements.append(HTextElement(page, item, 2))
        elif text.startswith("#"):
            self.title = text[1:].lstrip()
        else:
            self.elements.append(TextElement(page, item))

    def _handle_image(self, page, item):
        self.elements.append(ImageElement(page, item))

    def _handle_stroke(self, page, item):
        self.elements.append(StrokeElement(page, item))

    def _save_ressources(self):
        for element in self.elements:
            if isinstance(element, RessourceElement):
                element.save()

    def save(self):
        with open("template.tex") as f:
            template = f.read()
        template = string.Template(template)
        self._save_ressources()
        source = template.substitute({
            "body": "\n".join(element.render() for element in self.elements),
            "title": self.title,
            "authors": self.authors,
            "date": self.date
        })

        with open(self.output_path, "w") as f:
            f.write(source)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    # engine = Engine("example.xopp", "output.tex")
    path = "example.xopp"
    engine = Engine(path, "output.tex")
    engine.build_tex()
    with open("example.xml", 'wb') as f: engine.tree.write(f)
    engine.save()
