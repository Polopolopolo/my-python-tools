###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def new_cb(args):
    pass


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Tool for constructing gif-like animations")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    new_parser = subparsers.add_parser("new", help="Construct a new animation")
    new_parser.add_argument(dest="input", help="input file to construct the animation (the file extension must be specified)")
    new_parser.add_argument(dest="output", help="output file to store the animation in (the file extension must be specified)")
    new_parser.set_defaults(callback=new_cb)


    args = parser.parse_args()
    args.callback(args)
