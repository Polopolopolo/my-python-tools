import os
from xml.etree import ElementTree
import gzip


DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/gif-maker"


def abspath(rel_path):
    return os.path.join(DIRPATH, rel_path)


def parse_xopp(xopp_filename):
    with gzip.open(xopp_filename, "rb") as f:
        tree = ElementTree.fromstring(f.read())
    return tree



if __name__ == "__main__":
    tree = parse_xopp("toremove.xopp")
    import ipdb; ipdb.set_trace()