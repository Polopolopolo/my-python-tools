###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def new_task_cb(args):
    pass


def schedule_cb(args):
    pass


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Schedule tasks")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    new_task_parser = subparsers.add_parser("new_task", help="create a new task as a python function")
    new_task_parser.add_argument(dest="task_name", help="the name of the task to be created")
    new_task_parser.set_defaults(callback=new_task_cb)


    schedule_parser = subparsers.add_parser("schedule", help="schedule a task")
    schedule_parser.add_argument(dest="task_name", help="name of the task to be scheduled")
    schedule_parser.add_argument(dest="schedule_info", help="info for scheduling in cron format : minute hour month-day month week-day (* means any)")
    schedule_parser.set_defaults(callback=schedule_cb)


    args = parser.parse_args()
    args.callback(args)
