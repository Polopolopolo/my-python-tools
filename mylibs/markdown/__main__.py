###########
# IMPORTS #
###########

import argparse
from functools import partial
import os
import subprocess


###########
# GLOBALS #
###########

DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/markdown/"
DEFAULT_BROWSER = "chromium"
PANDOC_CSS_PATH = os.path.join(DIRPATH, "pandoc.css")
OUTPUT_HTML_PATH = os.path.join(DIRPATH, "rendering.html")


#########
# UTILS #
#########

def to_html_id(s):
    """ Returns s formatted as an html id
    """
    return s.strip().lower().replace(" ", "-")


#######
# API #
#######

def render(filepath, browser, math_engine, anchor):
    subprocess.run(["pandoc", filepath, "-o", OUTPUT_HTML_PATH, "--css", PANDOC_CSS_PATH, f"--{math_engine}"])
    print (OUTPUT_HTML_PATH + f"#{anchor}")
    subprocess.run([browser, "file://" + OUTPUT_HTML_PATH + "#" + to_html_id(anchor)])


############
# CALLBACK #
############

def callback(command, args):
    if command == "render":
        render(args.md_file, args.browser, args.math_engine, args.anchor)


########
# MAIN #
########

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Tools for markdown")
    subparsers: argparse._SubParsersAction = parser.add_subparsers(dest="command")

    render_parser: argparse.ArgumentParser = subparsers.add_parser("render", help="Render a markdown file in html")
    render_parser.add_argument("md_file")
    render_parser.add_argument("--browser", choices=["brave-browser", "firefox", "chromium"], default=DEFAULT_BROWSER)
    render_parser.add_argument("--math-engine", choices=["mathjax", "mathml"], default="mathjax")
    render_parser.add_argument("--anchor", default="", help="anchor to navigate to")
    render_parser.set_defaults(callback=partial(callback, "render"))

    args = parser.parse_args()
    if hasattr(args, "callback"):
        args.callback(args)
    else:
        parser.print_usage()
