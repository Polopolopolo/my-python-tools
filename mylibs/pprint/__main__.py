###########
# IMPORTS #
###########
import argparse
import os

from . import utils
from . import api


class ListAction(argparse.Action):
    def __init__(self, option_strings, dest=argparse.SUPPRESS, default=argparse.SUPPRESS, help=None):
        super().__init__(option_strings=option_strings, nargs=0, dest=dest, default=default, help=help)

    def __call__(self, parser, namespace, values, option_string=None):
        lst()
        parser.exit()


#############
# CALLBACKS #
#############
def lst():
    dct = api.lst()
    print ("# DEFAULTS")
    for t in sorted(dct["default"]):
        print (t)
    print()
    print ("# TABLE")
    for t in sorted(dct["table"]):
        print (t)


def pprint(args):
    if args.type:
        ext = args.type
    else:
        try:
            ext = os.path.splitext(args.filepath)[1][1:]
        except:
            exit ("Error: cannot guess file type. Aborting.")
    if ext not in api.lst()["table"]:
        exit (f"Error: cannot read the following file type: `{ext}`. Aborting.")
    try:
        f = open(args.filepath)
    except:
        exit ("Error: cannot read the file. Aborting.")
    else:
        content = f.read()
        if ext == "json":
            print(api.pprint_json(content))
        elif ext in ("yaml", "yml"):
            print(api.pprint_yaml(content))
    finally:
        f.close()


def table(args):
    if args.type:
        ext = args.type
    else:
        try:
            ext = os.path.splitext(args.filepath)[1][1:]
        except:
            exit ("Error: cannot guess file type. Aborting.")
    if ext not in api.lst()["table"]:
        exit (f"Error: cannot read the following file type: `{ext}`. Aborting.")

    if ext == "json":
        tb = api.TableView.read_json(args.filepath)
    elif ext == "tsv":
        tb = api.TableView.read_tsv(args.filepath)
    elif ext in ("yaml", "yml"):
        tb = api.TableView.read_yaml(args.filepath)
    print (*(row["content"] for row in tb.rows()), sep="\n")


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="pretty print files")

    parser.add_argument("--filepath")
    parser.add_argument("--type", help="type of the data of the file (default: guessed from the file's extension)")
    parser.add_argument("--list", help="display the lst of file types handled by this tool", action=ListAction)

    subparsers = parser.add_subparsers(dest="command")

    table_parser = subparsers.add_parser("table", help="pretty print a table")
    table_parser.add_argument("filepath", help="type with the data of the table")
    table_parser.add_argument("--type", help="type of the data of the file (default: guessed from the file's extension)")

    args = parser.parse_args()
    if not args.command:
        if args.lst:
            lst(args)
        else:
            pprint(args)
    elif args.command == "table":
        table(args)
    else:
        parser.print_usage()
