import csv
import json
import pprint
import yaml


def lst():
    return {
        "default":[
            "json",
            "yaml",
            "yml",
        ],
        "table": [
            "json",
            "tsv",
            "yaml"
        ]
    }


def pprint_yaml(content):
    dct = yaml.load(content, Loader=yaml.FullLoader)
    return pprint.pformat(dct)


def pprint_json(content):
    dct = json.loads(content)
    return pprint.pformat(dct)


class TableView:
    def __init__(self, data, headers=None) -> None:
        self.data = list(data)
        if not headers:
            self.headers = [""] * len(data[0])
        else:
            self.headers = headers

    def rows(self):
        if self.headers:
            max_widths = [max(len(self.headers[i]), max(len(str(row[i])) for row in self.data)) for i in range(len(self.data[0]))]
        else:
            max_widths = [max(len(str(row[i])) for row in self.data) for i in range(len(self.headers))]

        topbar = ''.join(['┌─'] + ['─' * mw + '─┬─' for mw in max_widths])[:-2] + '┐'
        botbar = ''.join(['└─'] + ['─' * mw + '─┴─' for mw in max_widths])[:-2] + '┘'
        hbar = ''.join(['├─'] + ['─' * mw + '─┼─' for mw in max_widths])[:-2] + '┤'
        yield {"type": "hbar", "content": topbar}
        if self.headers:
            s = '│ ' + ' │ '.join(self.headers[i].ljust(max_widths[i]) for i in range(len(self.headers))) + ' │'
            yield {"type": "header", "content": s}
            yield {"type": "hbar", "content": hbar}
        for row in self.data:
            s = '│ ' + ' │ '.join(str(x).ljust(max_widths[i]) for i, x in enumerate(row)) + ' │'
            yield {"type": "data", "content": s}
        yield {"type": "hbar", "content": botbar}

    @classmethod
    def read_records(cls, records):
        if isinstance(records[0], dict):
            headers = list(records[0].keys())
            data = [list(rec.values()) for rec in records]
        else:
            headers = None
            data = records
        return cls(data, headers)

    @classmethod
    def read_json(cls, filepath):
        with open(filepath) as f:
            jdct = json.load(f)
        return cls.read_records(jdct)

    @classmethod
    def read_yaml(cls, filepath):
        with open(filepath) as f:
            ydct = yaml.load(f, yaml.FullLoader)
        return cls.read_records(ydct)

    @classmethod
    def read_tsv_content(cls, content, header=False):
        records = list(content)
        if header:
            headers, data = records[0], records[1:]
            records = [dict(zip(headers, row)) for row in data]
        return cls.read_records(records)

    @classmethod
    def read_tsv(cls, filepath, header=False):
        with open(filepath) as f:
            content = csv.reader(f, delimiter="\t")
        return cls.read_tsv_content(content, header)
