import os


def abspath(rel_path):
    return os.path.join(os.path.split(__file__)[0], rel_path)
