###########
# IMPORTS #
###########
import argparse
import yaml

from . import utils
from . import api


###########
# GLOBALS #
###########
PAPERS_PATH = "/home/paul/espaces/phd/Papers"


#############
# CALLBACKS #
#############
def callback(parser, args):
    if args.info:
        create_info(args)
    else:
        parser.print_usage()


def create_info(args):
    api.create_info(".")
    print (f"Empty .info file created at current location.")


def ls_cb(_, args):
    l = api.ls_api(".")
    if any(pap["retcode"] for pap in l):
        exit("Error : a `.info` file is not valid.")
    for (i, pap) in enumerate(sorted(l, key=lambda x: x["data"]["title"])):
        print (i, pap["data"]["title"])


def list_cb(_, args):
    l = api.ls_api(PAPERS_PATH, args.title)
    if any(pap["retcode"] for pap in l):
        exit("Error : a `.info` file is not valid.")
    for (i, pap) in enumerate(sorted(l, key=lambda x: x["data"]["title"])):
        print (i, pap["data"]["title"])


def info_cb(_, args):
    l = api.ls_api(PAPERS_PATH)
    if any(pap["retcode"] for pap in l):
        exit("Error : a `.info` file is not valid.")
    if len(l) <= args.index:
        exit("There is no paper for this index.")
    print (yaml.safe_dump(sorted(l, key=lambda x: x["data"]["title"])[args.index]))


def search_cb(_, args):
    exit("NOT IMPLEMENTED YET")
    rows = api.search_api(args.query_str)
    if rows == 1:
        exit("Error : a `.info` file is not valid.")
    print (*rows, sep="\n")


def tui_cb(parser, args):
    from .tui.__main__ import main as tui_main
    tui_main()



########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="None")
    parser.set_defaults(callback=callback)
    subparsers = parser.add_subparsers(dest="command")

    parser.add_argument("--info", action="store_true", help="Creates a .info empty file at current location")

    ls_parser = subparsers.add_parser("ls", description="Display the list of papers in the current directory")
    ls_parser.set_defaults(callback=ls_cb)

    list_parser = subparsers.add_parser("list", description="Display the list of all papers")
    list_parser.add_argument("--title", help="Filter for title")
    list_parser.set_defaults(callback=list_cb)

    info_parser = subparsers.add_parser("info", description="Display information of one paper")
    info_parser.add_argument("index", type=int, help="Index of the paper (given by list command)")
    info_parser.set_defaults(callback=info_cb)

    search_parser = subparsers.add_parser("search", description="Search for a paper")
    search_parser.add_argument("query_str", help="String to search a paper")
    search_parser.set_defaults(callback=search_cb)

    tui_parser = subparsers.add_parser("tui", help="Run TUI")
    tui_parser.set_defaults(callback=tui_cb)

    args = parser.parse_args()
    args.callback(parser, args)
