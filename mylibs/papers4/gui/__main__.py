from PyQt5 import QtWidgets, QtCore, QtGui

from .new_paper import NewPaperDialog

from .. import api
from .. import utils
import os
from functools import partial


PAPERS_PATH = "/home/paul/espaces/phd/Papers"


class Folder:
    program = "nautilus"
    def __init__(self, dirpath):
        self.dirpath = dirpath
        self.label = QtWidgets.QLabel("Folder")
        self.button = QtWidgets.QPushButton("Open")
        self.button.clicked.connect(self.open)

        self.process = QtCore.QProcess()

    def open(self):
        self.process.startDetached(self.program + " " + f'"{self.dirpath}"')


class QFolderContentMenu(QtWidgets.QMenu):
    programs_filename = {
        "."     : "nautilus",
        ".info" : "code"
    }

    programs_ext = {
        "md"            : "brave-browser",
        "pdf"           : "okular",
        "tex"           : "code",
        "xopp"          : "xournalpp",
    }

    program_default = "code"

    def __init__(self, dirpath):
        super().__init__("Folder context menu")
        self.process = QtCore.QProcess()
        self.actions = []
        self.dirpath = dirpath

        action = QtWidgets.QAction("New File")
        action.triggered.connect(self.new_file)
        self.actions.append(action)
        self.addAction(action)
        self.addSeparator()

        self.addSection("Open file")
        self.files = os.listdir(dirpath)
        self._create_open_action(".")
        for f in self.files:
            self._create_open_action(f)

    def _create_open_action(self, filename):
        filepath = os.path.join(self.dirpath, filename)
        ext = os.path.splitext(filepath)[1][1:]
        action = QtWidgets.QAction(filename)
        self.actions.append(action)
        action.triggered.connect(partial(self.open, filepath, ext))
        self.addAction(action)

    def open(self, filepath, ext):
        if (filename:=os.path.split(filepath)[1]) in self.programs_filename:
            program = self.programs_filename[filename]
        elif ext in self.programs_ext:
            program = self.programs_ext[ext]
        else:
            program = self.program_default
        self.process.startDetached(f'{program} "{filepath}"')

    def new_file(self):
        filename, code = QtWidgets.QInputDialog.getText(self.window(), "New file", "Filename")
        if code:
            if filename in os.listdir(self.dirpath):
                errbox = QtWidgets.QMessageBox()
                errbox.setText(f"Error: there is already a file named {filename}.")
                errbox.exec()
            else:
                with open(os.path.join(self.dirpath, filename), "wb") as f: pass


class QFitTable(QtWidgets.QTableWidget):
    def resizeEvent(self, event):
        super().resizeEvent(event)
        height = self.horizontalHeader().height()
        for row in range(self.model().rowCount()):
            height += self.rowHeight(row)

        if self.horizontalScrollBar().isVisible():
            height += self.horizontalScrollBar().height()
        self.setMaximumHeight(height + 2)


class QFilterLineEdit(QtWidgets.QLineEdit):
    def __init__(self, table, placeholder, column_index):
        super().__init__()
        self.column_index = column_index
        self.table = table
        self.setPlaceholderText(placeholder)
        self.textChanged.connect(self.text_changed_cb)

    def text_changed_cb(self):
        self.table.filter_table()


class QPapersTableItem(QtWidgets.QTableWidgetItem):
    def __init__(self, base_text, repr_text=""):
        self.base_text = base_text
        if not repr_text:
            repr_text = base_text if isinstance(base_text, str) else base_text[0]
        super().__init__(repr_text)


class PapersTable(QtWidgets.QTableWidget):
    def __init__(self, papers, columns, sort_by):
        self.papers = papers
        self.columns = columns
        super().__init__(len(papers) + 1, len(columns))
        self.setAlternatingRowColors(True)

        self.sort_info = {"column": sort_by, "reverse": False}

        # filters
        date_filter = QFilterLineEdit(self, "filter", 0)
        self.setCellWidget(0, 0, date_filter)
        title_filter = QFilterLineEdit(self, "filter", 1)
        self.setCellWidget(0, 1, title_filter)
        authors_filter = QFilterLineEdit(self, "filter", 2)
        self.setCellWidget(0, 2, authors_filter)

        self.fill_table(papers, columns, self.sort_info["column"], reverse=self.sort_info["reverse"])
        self.horizontalHeader().sectionClicked.connect(self.headerClicked_cb)
        self.cellClicked.connect(self.open_left_panel)

        # right click on item
        self.pressed.connect(self.on_item_clicked)


    def keyPressEvent(self, e: QtGui.QKeyEvent) -> None:
        super().keyPressEvent(e)
        if e.key() == QtCore.Qt.Key_Return:
            if self.selectedItems():
                title = self.selectedItems()[0].text()
                if [pap for pap in self.papers if pap["data"]["title"] == title]:
                    pap = [pap for pap in self.papers if pap["data"]["title"] == title][0]
                    print (pap["dir_path"])
                    menu = QFolderContentMenu(pap["dir_path"])
                    id_pap = os.path.split(pap["dir_path"])[1]
                    if f"{id_pap}.pdf" in os.listdir(pap["dir_path"]):
                        menu.open(os.path.join(pap["dir_path"], f"{id_pap}.pdf"), "pdf")


    def _text_filter(self, text, filt):
        return any(filt.lower() in word.lower() for word in text.split())

    def _list_filter(self, lst, filt):
        return any(self._text_filter(text, filt) for text in lst)

    def filter(self, obj, filt):
        assert isinstance(obj, str) or isinstance(obj, list)
        if isinstance(obj, str):
            return self._text_filter(obj, filt)
        else:
            return self._list_filter(obj, filt)


    def sort_table(self, index, reverse):
        if not reverse:
            qt_order = QtCore.Qt.AscendingOrder
        else:
            qt_order = QtCore.Qt.DescendingOrder
        self.sortByColumn(index, qt_order)

    def headerClicked_cb(self, index):
        column = self.horizontalHeaderItem(index).text().lower()
        if column == self.sort_info["column"]:
            self.sort_info["reverse"] = not self.sort_info["reverse"]
        else:
            self.sort_info["column"] = column
            self.sort_info["reverse"] = False

        self.fill_table(self.papers, self.columns, sort_by=column, reverse=self.sort_info["reverse"])


    def open_left_panel(self, row):
        title_index = self.columns.index("Title")
        title = self.item(row, title_index).text()
        pap = [pap for pap in self.papers if pap["data"]["title"] == title][0]

        self.parent().destroy_left_panel()
        self.parent().create_left_panel(pap)


    @staticmethod
    def format_papers(papers):
        for pap in papers:
            auths = pap["data"]["authors"]
            if type(auths) == list:
                if len(auths) > 1:
                    pap["data"]["authors_repr"] = auths[0] + " et al."
                else:
                    pap["data"]["authors_repr"] = auths[0]
            elif type(auths) == str:
                pap["data"]["authors_repr"] = auths
            else:
                pap["data"]["authors_repr"] = ""

            if "date" not in pap["data"]:
                pap["data"]["date_repr"] = ""
                pap["data"]["date"] = ""
            else:
                pap["data"]["date_repr"] = pap["data"]["date"][:4]


    def fill_table(self, papers, columns, sort_by, reverse):
        # unfilter
        for i in range(1, self.rowCount()):
            self.showRow(i)

        self.format_papers(papers)
        self.papers = sorted(papers, key=lambda pap: pap["data"][sort_by], reverse=reverse)

        # add or remove rows and colums to match new number of papers
        for _ in range(len(papers) - len(self.papers)):
            self.removeRow(1)
        for _ in range(len(self.papers) - len(papers)):
            self.insertRow(1)
        for _ in range(len(columns) - len(self.columns)):
            self.removeColumn(1)
        for _ in range(len(self.columns) - len(columns)):
            self.insertColumn(1)

        # fill the table
        for i, pap in enumerate(papers):
            date_item = QPapersTableItem(pap["data"]["date"], pap["data"]["date_repr"])
            title_item = QPapersTableItem(pap["data"]["title"])
            authors_item = QPapersTableItem(pap["data"]["authors"], pap["data"]["authors_repr"])
            self.setItem(i + 1, 0, date_item)
            self.setItem(i + 1, 1, title_item)
            self.setItem(i + 1, 2, authors_item)
        self.resizeColumnsToContents()
        self.setHorizontalHeaderLabels(columns)
        self.setVerticalHeaderLabels([""] + [str(i) for i in range(1, len(papers) + 1)])

        # refilter
        self.filter_table()

    def filter_table(self):
        filters = [self.cellWidget(0, i).text() for i in range(self.columnCount())]
        for i in range(1, self.rowCount()):
            items = [self.item(i, j) for j in range(self.columnCount())]
            if any(filt and not self.filter(it.base_text, filt) for (filt, it) in zip(filters, items)):
                self.hideRow(i)
            else:
                self.showRow(i)

    def on_item_clicked(self, index):
        if QtGui.QGuiApplication.mouseButtons() == QtCore.Qt.RightButton:
            title = self.item(index.row(), index.column()).text()
            pap = [pap for pap in self.papers if pap["data"]["title"] == title][0]
            menu = QFolderContentMenu(pap["dir_path"])
            menu.exec(QtGui.QCursor.pos())


class PaperInfo(QFitTable):
    def __init__(self, data):
        not_repr_items = [it for it in data.items() if not it[0].endswith("_repr")]
        super().__init__(len(not_repr_items), 2)
        self.setWordWrap(True)
        self.horizontalHeader().setStretchLastSection(True)
        self.horizontalHeader().setVisible(False)
        self.verticalHeader().setVisible(False)
        self.setAlternatingRowColors(True)

        for i, (k, v) in enumerate(not_repr_items):
            self.setItem(i, 0, QtWidgets.QTableWidgetItem(k))
            if isinstance(v, str):
                value = QtWidgets.QTableWidgetItem(v)
            elif isinstance(v, list):
                value = QtWidgets.QTableWidgetItem("\n".join(v))
            self.setItem(i, 1, value)
        self.resizeRowsToContents()


class RightPanel(QtWidgets.QWidget):
    def __init__(self, main_widget, pap):
        super().__init__()
        self.setLayout(QtWidgets.QVBoxLayout())
        self.setMaximumWidth(QtWidgets.QDesktopWidget().availableGeometry().width() // 5)

        # header
        header = QtWidgets.QWidget()
        header.setLayout(QtWidgets.QHBoxLayout())
        hide_btn = QtWidgets.QPushButton("x")
        hide_btn.clicked.connect(main_widget.destroy_left_panel)
        header.layout().addWidget(QtWidgets.QWidget(), 5)
        header.layout().addWidget(hide_btn)
        self.layout().addWidget(header)

        # paper info
        self.paper_info = PaperInfo(pap["data"])
        self.layout().addWidget(self.paper_info)

        # widget for spacing (TODO: do it in a better way)
        self.layout().addWidget(QtWidgets.QWidget(), 0)


class MainWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setLayout(QtWidgets.QHBoxLayout())

        papers = api.ls_api(PAPERS_PATH)
        columns = ["Date", "Title", "Authors"]
        self.table = PapersTable(papers, columns, "title")
        self.left_panel = None
        self.layout().addWidget(self.table)

    def create_left_panel(self, pap):
        self.left_panel = RightPanel(self, pap)
        self.layout().addWidget(self.left_panel)

    def destroy_left_panel(self):
        if self.left_panel:
            self.left_panel.deleteLater()
            self.left_panel = None


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.resize(QtWidgets.QDesktopWidget().availableGeometry().size())

        self.menubar = QtWidgets.QMenuBar()
        file_menu = self.menubar.addMenu("File")

        # New paper
        new_paper_action = QtWidgets.QAction("New Paper", self)
        new_paper_action.triggered.connect(self.new_paper_cb)
        file_menu.addAction(new_paper_action)

        # Refresh papers
        refresh_papers_action = QtWidgets.QAction("Refresh", self)
        refresh_papers_action.triggered.connect(self.refresh_paper_cb)
        file_menu.addAction(refresh_papers_action)

        self.setMenuBar(self.menubar)

        self.setCentralWidget(MainWidget())


    def new_paper_cb(self, *args):
        self.new_paper_dialog = NewPaperDialog(self, PAPERS_PATH)
        self.new_paper_dialog.show()

    def refresh_paper_cb(self, *args):
        papers = api.ls_api(PAPERS_PATH)
        columns = ["Date", "Title", "Authors"]
        self.centralWidget().table.fill_table(papers, columns, "title", False)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    app.setWindowIcon(QtGui.QIcon(utils.abspath("gui/Qnewspaper.png")))

    main_window = MainWindow()
    main_window.setWindowTitle("Papers")
    main_window.show()

    app.exec_()
