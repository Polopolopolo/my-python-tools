from .. import utils
import os
import requests
import yaml

from PyQt5 import QtWidgets, QtGui, QtCore, uic

import bibtexparser


def display_error(msg):
    error_msg = QtWidgets.QErrorMessage()
    error_msg.showMessage(msg)
    error_msg.exec()


class NewPaperDialog(QtWidgets.QWidget):
    def __init__(self, main_window, papers_path):
        super().__init__()
        self.main_window = main_window
        self.papers_path = papers_path
        self.setWindowTitle("New Paper")
        uic.loadUi(utils.abspath("gui/new_paper.ui"), self)

        self.bibtex_Button.clicked.connect(self.bibtex_btn_cb)

        self.authors = [self.authors_lineEdit]
        self.tags = [self.tags_lineEdit]

        self.authors_Button.clicked.connect(self.authors_btn_cb)
        self.tags_Button.clicked.connect(self.tags_btn_cb)

        self.filepath = ""
        self.select_radioButton.clicked.connect(self.select_filepath_cb)

        self.validate_Button.clicked.connect(self.validate_btn_cb)

        self.resize(
            QtWidgets.QDesktopWidget().availableGeometry().width() // 2,
            self.height())


    def fill_bibtex_info(self):
        bibtex = self.bibtex_text_edit.toPlainText()
        try:
            data = list(bibtexparser.loads(bibtex).entries_dict.values())[0]
        except Exception as err:
            display_error("Error while parsing the bibtex.")
            print (err, err.__class__.__name__, err.args, sep="\n")
        else:
            self.title_lineEdit.setText(data["title"])
            if " and\n" in data["author"]:
                authors = data["author"].split(" and\n")
            else:
                authors = data["author"].split(" and ")
            for (i, auth) in enumerate(authors):
                self.authors[-1].setText(auth)
                if i < len(authors) - 1:
                    self.authors_btn_cb()
            self.date_lineEdit.setText(data["year"])
            self.paper_id_lineEdit.setText(data["ID"].replace(":", ".").replace("/", "."))

        self.bibtex_w.close()


    def bibtex_btn_cb(self):
        self.bibtex_w = QtWidgets.QWidget()
        self.bibtex_w.resize(
            QtWidgets.QDesktopWidget().availableGeometry().width() // 2,
            QtWidgets.QDesktopWidget().availableGeometry().height() // 2)
        self.bibtex_w.setLayout(QtWidgets.QVBoxLayout())

        self.bibtex_text_edit = QtWidgets.QTextEdit()
        self.bibtex_text_edit.setPlaceholderText("Paste Bibtex here")
        self.bibtex_w.layout().addWidget(self.bibtex_text_edit)

        self.bibtex_validate_btn = QtWidgets.QPushButton("Validate")
        self.bibtex_validate_btn.clicked.connect(self.fill_bibtex_info)
        self.bibtex_w.layout().addWidget(self.bibtex_validate_btn)

        self.bibtex_w.show()


    def authors_btn_cb(self):
        self.authors.append(QtWidgets.QLineEdit())
        self.authors_list.layout().addWidget(self.authors[-1])

    def tags_btn_cb(self):
        self.tags.append(QtWidgets.QLineEdit())
        self.tags_list.layout().addWidget(self.tags[-1])

    def select_filepath_cb(self, event):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        if filepath[0]:
            self.filepath = filepath[0]
        else:
            self.filepath = ""

    def validate_btn_cb(self):
        info = {"title": "", "authors": [], "tags": [], "url": "", "date": "", "paper_id": ""}

        # Title is mandatory
        if not self.title_lineEdit.text() or not self.paper_id_lineEdit.text():
            display_error("""Title and paper_id must be specified !
            If not paper_id for this file, replace it with the title.""")
            return

        # Fill info
        info["title"] = self.title_lineEdit.text().strip()
        info["authors"] = [x.text().strip() for x in self.authors]
        info["tags"] = [x.text().strip() for x in self.tags]
        info["url"] = self.url_lineEdit.text().strip()
        info["date"] = self.date_lineEdit.text().strip()
        info["paper_id"] = self.paper_id_lineEdit.text().strip()

        # Select filepath
        if self.select_radioButton.isChecked():
            # then valid filepath is mandatory
            try:
                with open(self.filepath, "rb") as f:
                    file_content = f.read()
            except Exception as err:
                display_error("You must choose a valid filepath !")
                print (err, err.__class__.__name__, err.args, sep="\n")
                return

        # Download filepath
        elif self.download_radioButton.isChecked():
            # url must be specified
            if not self.url_lineEdit.text():
                display_error("You need to provide url in order to use download option.")
                return

            # only work for arxiv and eprint
            if "arxiv" in info["url"]:
                url = info["url"].replace("abs", "pdf")
            elif "eprint" in info["url"]:
                url = info["url"] + ".pdf"
            else:
                display_error("""Cannot download a paper that is neither in 'arxiv' nor in 'eprint'.
                Please download the paper yourself and use the 'Select paper' option.""")
                return

            # check for any download issue
            try:
                resp = requests.get(url)
            except Exception as err:
                display_error("""There were an error during the download.
                Please try again or download the paper yourself and use the 'Select paper' option.""")
                print (err, err.__class__.__name__, err.args, sep="\n")
                return

            # the response code must be 200
            if resp.status_code != 200:
                display_error("""There were an error during the download.
                Please try again or download the paper yourself and use the 'Select paper' option.""")
                return

            file_content = resp.content

        # If everything is fine, create the new directory, then write pdf file and .info
        new_dir = os.path.join(self.papers_path, info["paper_id"])
        if os.path.exists(new_dir):
            display_error("This paper has already been added.")
            return

        os.makedirs(new_dir)
        filepath = os.path.join(new_dir, info["paper_id"] + ".pdf")
        with open(filepath, "wb") as f:
            f.write(file_content)
        with open(os.path.join(new_dir, ".info"), "w") as f:
            yaml.dump(info, f)

        # Finally, refresh the papers table and close this window
        self.main_window.refresh_paper_cb()
        self.close()


