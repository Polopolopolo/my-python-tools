from mylibs.curses.widgets.text_edit_widget import TextWidget


class HelpFooter(TextWidget):
    SEP_WIDTH = 4

    def __init__(self, parent, name: str, **kwargs):
        self.commands = []
        super().__init__(parent, name, 0, 0, **kwargs)

    def append_command(self, cmd, name):
        self.commands.append({"cmd": cmd, "name": name})

    def compute_shape_and_text(self):
        fcommands = [f"{cmd['cmd']}: {cmd['name']}" for cmd in self.commands]
        mx = max(len(fcmd) for fcmd in fcommands)
        width = mx + mx % self.SEP_WIDTH
        n = self.app.width // width # number of commands per line
        txt = ""
        for i, fcmd in enumerate(fcommands):
            if i and i % n == 0:
                txt += "\n"
            txt += fcmd.ljust(width)
        self.text = "Welcome to papers4.\n".center(self.app.width) + "\n" + txt
