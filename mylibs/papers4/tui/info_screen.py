import curses
import curses.textpad
import logging
import os
import pprint
import subprocess
from typing import Iterable

from .utils import format_info_paper
from mylibs.curses.widgets.text_widget import TextWidget
from mylibs.curses.widgets.button_widget import ButtonWidget
from mylibs.curses.engines.simple_engine import SimpleEngine
from mylibs.curses.screen import Screen



class BackButton(ButtonWidget):
    def __init__(self, parent, name: str, top: int, left: int):
        super().__init__(parent, name, top, left, "Back")

    def on_pressed(self, _):
        self.app.set_current_screen("list_screen")


class OpenFileButton(ButtonWidget):
    def __init__(self, parent, name: str, top: int, left: int, filepath):
        super().__init__(parent, name, top, left, os.path.split(filepath)[1])
        self.filepath = filepath
        execs = self.app.cfg["executables"]
        ext = os.path.splitext(self.filepath)[1]
        self.exec = execs[ext] if ext in execs else None

    def on_pressed(self, ch):
        if self.exec:
            subprocess.Popen([self.exec, self.filepath], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        return super().on_pressed(ch)


class InfoScreen(Screen):
    def __init__(self, parent, name):
        super().__init__(parent, name)
        self.paper = {}
        self.append_widget(BackButton(self, "info_back_button", 0, 0))
        self.add_engine(SimpleEngine(self, "info_engine"))

    def on_entered(self):
        self.engine.cursor = 0

    def _init_info_paper_texts(self, paper, top):
        # remove previous text widgets
        to_remove = []
        for w in self.widgets:
            if w.startswith("info_paper_textw_"):
                to_remove.append(w)
        for w in to_remove:
            del self.widgets[w]
        # add text widget for each information on the paper
        for (k, v) in zip(*format_info_paper(paper)):
            self.append_widget(TextWidget(self, f"info_paper_textw_{k}", top, 0, k, style={"color": "cyan on black"}))
            top += 1
            w = TextWidget(self, f"info_paper_textw_{v}", top, 0, v)
            self.append_widget(w)
            top += w.height + 1
        return top

    def _init_open_file_button(self, top):
        # remove previous buttons
        to_remove = []
        for w in self.widgets:
            if w.startswith("file_"):
                to_remove.append(w)
        for w in to_remove:
            del self.widgets[w]
        # label files TODO: make it work
        self.append_widget(TextWidget(self, "label_files", top, 0, "Files", style={"color": "cyan on black"}))
        # add a button for each file in the paper's directory
        for f in (f for f in os.listdir(self.paper["dir_path"]) if os.path.isfile(f)):
            btn = OpenFileButton(self, f"file_{f}", top + 1, 0, 
                                              os.path.join(self.paper["dir_path"], f))
            self.append_widget(btn)
            top = btn.top
        # label directories TODO: make it work
        self.append_widget(TextWidget(self, "label_dirs", top, 0, "Directories", style={"color": "cyan on black"}))
        top
        # same for each directory
        for f in (f for f in os.listdir(self.paper["dir_path"]) if not os.path.isfile(f)):
            btn = OpenFileButton(self, f"file_{f}", top + 1, 0, 
                                              os.path.join(self.paper["dir_path"], f))
            self.append_widget(btn)
            top = btn.top
        return top

    def set_paper(self, paper):
        self.paper = paper
        # info paper
        top = self._init_info_paper_texts(paper, 0)
        # list of files
        top = self._init_open_file_button(top)
        # back button
        ibb = self.widgets["info_back_button"]
        ibb.top = top + 2
        del self.widgets["info_back_button"]
        self.append_widget(ibb)
        # recompute engine
        self.engine.compute_selectables()

    def handle_input(self, ch):
        if ch == curses.KEY_BACKSPACE:
            self.app.set_current_screen("list_screen")
        return super().handle_input(ch)
