import curses
import curses.textpad
from typing import List

from mylibs.curses.widget import Widget2


class CommandModel:
    def __init__(self, name: str, cmd: str) -> None:
        self.name = name
        self.cmd = cmd


class CommandFooter(Widget2):
    def __init__(self, parent, name, topleft, commands: List[CommandModel]):
        super().__init__(parent, name, topleft)
        self.commands = commands

    def handle_input(self, ch):
        return super().handle_input(ch)

    def display(self):
        for cmd in self.commands:
            self.write(cmd.name)
        return super().display()

