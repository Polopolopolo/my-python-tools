import curses
import logging
import traceback

from mylibs.curses.app import App
from mylibs.papers3.api import ls_api

from ..utils import abspath
from .utils import get_config
from .info_screen import InfoScreen
from .list_screen import ListScreen


class PapersApp(App):
    def __init__(self, config):
        super().__init__()
        self.cfg = config
        self.papers = ls_api(config["papers_path"])

        self.append_screen(ListScreen(self, "list_screen"))
        self.append_screen(InfoScreen(self, "info_screen"))


def main():
    logging.basicConfig(filename="log.log", filemode="w", level=0)
    logging.info("Session starts")
    cfg = get_config(abspath("tui/config.json"))
    try:
        app = PapersApp(cfg)
        app.run()
    except Exception as err:
        tb = "".join(traceback.format_tb(err.__traceback__))
        logging.critical(f"""{err.__class__.__name__} {err.args}
Trace: {tb}""")
        curses.endwin()
        with open("log.log") as f:
            print (f.read())
    logging.info("Session ends")


if __name__ == "__main__":
    main()
