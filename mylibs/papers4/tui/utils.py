import json
import logging
import yaml


def get_config(path):
    with open(path) as f:
        return json.load(f)


def format_info_paper(paper):
    data = {k.title(): paper["data"][k] for k in ["title", "authors", "date", "key"] if k in paper["data"].keys()}
    keys, values = [], []
    for (k, v) in data.items():
        keys.append(k)
        values.append(yaml.dump(v, indent=2))
    if "dir_path" in paper:
        keys.append("Path")
        values.append(yaml.dump(paper["dir_path"], indent=2))
    return (keys , values)
