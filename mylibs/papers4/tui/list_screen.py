from collections import namedtuple
import curses
import curses.textpad
import datetime
import logging
import os
import subprocess
import string
from typing import Iterable

from mylibs.pprint import api as pprt

from mylibs.curses.engines.scroll_engine import ScrollEngine
from mylibs.curses.screen import Screen
from mylibs.curses.widgets.scroll_widget import ScrollWidget
from mylibs.curses.widgets.text_edit_widget import TextEditWidget

from .help_footer import HelpFooter


class FilterWidget(TextEditWidget):
    pass


class ListScrollWidget(ScrollWidget):
    def __init__(self, parent, name, top, left, bottom):
        super().__init__(parent, name, top, left, bottom, trigger_button=[curses.KEY_ENTER, 10, 13, 15])
        self.max_bottom = bottom
        self.papers = self.app.papers
        self.append_items((pap["data"]["title"] for pap in self.papers))

    def recompute_shape(self):
        self.bottom = self.top + min(len(self.items) - 1, self.max_bottom - self.top)

    def filter_items(self, filtr):
        if filtr.strip():
            self.items = []
            self.append_items((pap["data"]["title"] for pap in self.papers\
                               if filtr.lower().strip() in pap["data"]["title"].lower()\
                               or any(filtr.lower().strip() in a.lower() for a in pap["data"]["authors"]) ))

    def on_pressed(self, ch):
        paper = [pap for pap in self.papers if pap["data"]["title"] == self.items[self.current_item]][0]
        if ch in (curses.KEY_ENTER, 10, 13):
            self.app.screens["info_screen"].set_paper(paper)
            self.app.set_current_screen("info_screen")
        elif ch == 15: # CTRL + O
            if (pdf := f"{paper['data']['paper_id']}.pdf") in os.listdir(paper["dir_path"]):
                subprocess.Popen([self.app.cfg["executables"][".pdf"], os.path.join(paper["dir_path"], pdf)])


class ListScrollEngine(ScrollEngine):
    def __init__(self, screen: Screen, name: str, filter_widget: TextEditWidget, scroll_widget: ScrollWidget) -> None:
        super().__init__(screen, name, scroll_widget)
        self.filter_widget = filter_widget

    def handle_input(self, ch: str):
        if chr(ch) in string.digits + string.ascii_letters + string.punctuation\
            or ch == curses.KEY_BACKSPACE:
            self.filter_widget.set_selected(True)
            self.scroll_widget.filter_items(self.filter_widget.text)
            self.cursor = 0
            self.scroll_widget.current_item = 0
            self.scroll_widget.recompute_shape()
        else:
            self.filter_widget.set_selected(False)
            return super().handle_input(ch)


class ListScreen(Screen):
    def __init__(self, parent, name):
        super().__init__(parent, name)
        helper = HelpFooter(self, "list_help_footer")
        helper.append_command("CTRL+X", "Exit")
        helper.append_command("CTRL+O", "Open pdf")
        helper.compute_shape_and_text()
        helper.top = self.app.height - helper.height
        self.append_widget(FilterWidget(self, "list_filter", 0, 0))
        self.append_widget(ListScrollWidget(self, "list_scroll", 2, 0, self.app.height - helper.height - 2))
        self.append_widget(helper)
        self.add_engine(ListScrollEngine(self, "list_engine", self.widgets["list_filter"], self.widgets["list_scroll"]))

    @staticmethod
    def format_authors(authors):
        if not authors or not isinstance(authors, Iterable) or " " not in authors[0]:
            return ""
        else:
            firstname, lastname = authors[0].rsplit(maxsplit=1)
            first_author = f"{firstname[0].upper()}.{lastname}"
            return first_author if len(authors) == 1 else f"{first_author} et al."

    @staticmethod
    def format_date(date):
        if not date or not isinstance(date, str):
            return ""
        elif len(date) > 4:
            return date[:4]
        else:
            return date

    @staticmethod
    def format_paper_item(pap, width):
        pass
        # title = pap["data"]["title"]
        # authors = pap["data"]["authors"]
        # date = pap["data"]["date"]
