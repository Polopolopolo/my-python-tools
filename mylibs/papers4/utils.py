import os
import typeguard
from typing import List
import yaml


INFO_SCHEME = {"title": str, "authors": List[str], "tags": List[str], "url": str}


def abspath(rel_path):
    dirpath = os.path.split(__file__)[0]
    return os.path.join(dirpath, rel_path)

def info_path(dirpath):
    return os.path.join(dirpath, ".info")


def list_papers_folders(root_path):
    return [fp for (fp, _, files) in os.walk(root_path) if ".info" in files]


def check_info_file(dirpath):
    try:
        with open(info_path(dirpath)) as f:
            info = yaml.safe_load(f)
    except:
        return False
    else:
        return True


def get_info_content(dirpath):
    with open(info_path(dirpath)) as f:
        info = yaml.safe_load(f)
    return info


def check_info_content(info):
    for (k, v) in INFO_SCHEME.items():
        if k not in info:
            return False
        else:
            try:
                typeguard.check_type(k, info[k], v)
            except TypeError:
                return False
    return True
