from functools import wraps
from typeguard import check_type


class DictNotConsistent(Exception):
    pass


def check_dict_consistency(scheme):
    def wrapper(f):
        def g(*args, **kwargs):
            dct = f(*args, **kwargs)
            for (k, v) in scheme.items():
                if k not in dct and v["required"]:
                    raise DictNotConsistent(k, "missing")
                elif v["type"]:
                    try:
                        check_type(k, dct[k], v["type"])
                    except TypeError:
                        raise DictNotConsistent(k, "wrong type")
            return dct
        return wraps(g)
    return wrapper
