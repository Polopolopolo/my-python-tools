import os


def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)


class Colors:
    RED   = "\033[1;31m"
    BLUE  = "\033[1;34m"
    CYAN  = "\033[1;36m"
    GREEN = "\033[0;32m"
    ORANGE = "\033[0;33m"
    RESET = "\033[0;0m" 
    BOLD    = "\033[;1m"
    REVERSE = "\033[;7m"

    @classmethod
    def pcolorformat(cls, color, text):
        return cls.__dict__[color.upper()] + text + cls.RESET

    @classmethod
    def pcolor(cls, color, text, end="\n"):
        print(cls.pcolorformat(color, text), end=end)

    @classmethod
    def pcolorformat_match(cls, color, text, pattern):
        l = len(pattern)
        idx = text.lower().find(pattern.lower())
        if not pattern or idx == -1:
            return text
        s = text[:idx]
        s += cls.pcolorformat(color, text[idx:idx + l])
        return s + cls.pcolorformat_match(color, text[idx + l:], pattern)

    @classmethod
    def pcolor_match(cls, color, text, pattern):
        print (cls.pcolorformat_match(color, text, pattern))


if __name__ == "__main__":
    Colors.pcolor_match("red", "Je m'appelle paul", "paul")
    Colors.pcolor_match("red", "Je m'appelle paul...", "paul")
    Colors.pcolor_match("red", "Je m'appelle paul et mon nom est paul", "paul")
    Colors.pcolor_match("red", "Je m'appelle paul et mon nom est paul...", "paul")
    Colors.pcolor_match("red", "Je m'appelle paul et mon nom est Paul...", "paul")
