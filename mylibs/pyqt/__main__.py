###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def main_cb(args):
    api.main_api(args.filename)



########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Automatization of things for PyQt")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    main_parser = subparsers.add_parser("main", help="Create a new main file")
    main_parser.add_argument("filename", help="name of the file to be written")
    main_parser.set_defaults(callback=main_cb)


    args = parser.parse_args()
    args.callback(args)
