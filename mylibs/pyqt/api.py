import shutil

from . import utils


def main_api(dest):
    shutil.copy(utils.abspath("templates/main.py.template"), dest)
