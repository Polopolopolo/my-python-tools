import os
import regex

from myutils.colors import cformat


def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)

def find_on_line(pattern: str, line: str, replace=None):
    match = regex.search(pattern, line)
    if replace is None:
        return {
            'match': match,
            'fstring': cformat(line, 'red reverse', cfilter=pattern)
        }
    else:
        sub = regex.sub(pattern, replace, line)
        begin, offset = 0, 0
        after = ''
        for match in regex.finditer(pattern, line):
            after += sub[begin:match.start() - offset]
            after += cformat(sub[match.start() - offset:match.start() - offset + len(replace)], 'green reverse')
            begin = match.start() - offset + len(replace)
            offset += len(match.span()) - len(replace)
        after += sub[begin:]

        return {
            'match': match,
            'before': cformat(line, 'red reverse', cfilter=pattern),
            'after': after,
            'sub': sub
        }
