from argparse import ArgumentParser
from pathlib import Path
import regex

from myutils.colors import cprint, cformat
from myutils.file_io import write_text

from . import utils
from . import api


def callback(args):
    for file in args.files:
        if Path(file).is_dir():
            if args.recursive:
                args.files += [p.as_posix() for p in Path(file).rglob('*')]
            else:
                exit(f'{file} is a directory; use command `--recursive` to run the program on it')
        elif not args.file_filter or regex.search(args.file_filter, file):
                new_lines = []
                lines = api.find(args.pattern, file, args.replace)
                for i, line in enumerate(lines):
                    if line['match']:
                        prefix = cformat(file, 'dark purple') + ':' + cformat(str(i+1), 'dark green') + ':'
                        if not args.replace:
                            print (prefix + line['fstring'])
                        else:
                            print (prefix + line['before'])
                            print (prefix + line['after'])
                            if not args.non_interactive:
                                input('\n')
                    if args.replace:
                        new_lines.append(line['sub'])
                if args.replace:
                    write_text(file, '\n'.join(new_lines))

if __name__ == "__main__":
    parser = ArgumentParser(description="Find and replace expression in files using regex")

    parser.add_argument('pattern', help='Pattern to search for')
    parser.add_argument('--replace', help='Text to replace the matches')
    parser.add_argument('--non-interactive', action='store_true', default=False, help='Whether the process needs to be interactive or not (default=True)')
    parser.add_argument('files', nargs='+', help='Files in which to do the search')
    parser.add_argument('--file_filter', help='Regex filter for files')
    parser.add_argument('--recursive', '-r', action='store_true', help='Allow for directories')

    args = parser.parse_args()
    callback(args)
