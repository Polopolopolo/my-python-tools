from . import utils
from myutils.file_io import read_lines


def find(pattern: str, filepath: str, replace=None):
    try:
        lines = read_lines(filepath)
    except Exception as err:
        print (f'Error while trying to read {filepath}: ignored')
        return []
    else:
        return [utils.find_on_line(pattern, line, replace) for line in lines]
