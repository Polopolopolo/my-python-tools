from argparse import ArgumentParser
from typing import List


def build_table(headers: List[str], content: List[List[str]], max_width: int):
    # TODO use max width
    widths_headers = [len(h) for h in headers]
    widths_content = [max(len(content[i][j])   for i in range(len(content)))
                            for j in range(len(content[0]))]
    widths = [max(h, c) for (h,c) in zip(widths_headers, widths_content)]

    line_width = sum(widths) + 3 * len(widths) + 1
    line = "-" * line_width
    draw = ""
    draw += line + "\n"
    draw += "".join(f"| {h.center(widths[i])} " for (i, h) in enumerate(headers)) + "|" + "\n"
    draw += line + "\n"
    return draw


if __name__ == "__main__":
    headers = ["Ref", "Tag", "Plaintext", "Ciphertext"]
    content = [
        ["D309", "5A", "", ""]
    ]
    print (build_table(headers, content, -1))
