###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def send_cb(args):
    pass


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="email managing library")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    send_parser = subparsers.add_parser("send", help="Send an email")
    send_parser.add_argument(dest="host", help="Host of the webmail server (e.g. smtp.gmail.com)")
    send_parser.add_argument(dest="port", help="e.g. 587")
    send_parser.add_argument(dest="sender")
    send_parser.add_argument(dest="receiver")
    send_parser.add_argument(dest="subject")
    send_parser.add_argument(dest="message")
    send_parser.set_defaults(callback=send_cb)


    args = parser.parse_args()
    args.callback(args)
