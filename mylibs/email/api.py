from email.message import EmailMessage
import smtplib
import ssl


def send(receiver,
        subject,
        message,
        password,
        host="smtp.gmail.com",
        port=587,
        sender="paul.hermouet.teaching@gmail.com"):

    msg = EmailMessage()
    msg.set_content(message)
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = receiver

    context = ssl.create_default_context()
    with smtplib.SMTP(host, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender, password)
        server.send_message(msg)
