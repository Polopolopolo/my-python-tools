import sqlite3
import os
import argparse


class DataBase:
    def __init__(self, filepath):
        if not os.path.isfile(filepath):
            raise Exception("Wrong path")
        self.conn = sqlite3.connect(filepath)
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.conn.close()

    def query(self, querystring, *args):
        try:
            return self.cursor.execute(querystring, *args)
        except Exception as err:
            raise Exception("Problem with query", err)

    @property
    def tables(self):
        if not hasattr(self, "_tables"):
            self._tables = [r[0] for r in self.query("SELECT name FROM sqlite_master WHERE type='table'")]
        return self._tables

    @property
    def columns(self):
        if not hasattr(self, "_columns"):
            self._columns = {t: [r[1] for r in self.query(f"PRAGMA TABLE_INFO({t})")] for t in self.tables}
        return self._columns

    def show_info(self, display_columns):
        print ("\n== DATABASE INFORMATION ==\n")
        for t in self.tables:
            print ("- " + t)
            if display_columns:
                print (*(f"    - {c}" for c in self.columns[t]), sep="\n", end="\n\n")

    def explore_rows(self, querystring):
        rows = self.query(querystring)
        for r in rows:
            print (*(f"- {c}" for c in r), sep="\n", end="\n\n")
            input("--> Next Row")


def db_info(args):
    db = DataBase(args.db_path)
    db.show_info(args.show_columns)


def explore_callback(args):
    db = DataBase(args.db_path)
    db.explore_rows(f"SELECT * FROM {args.table}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Display the content of a sqlite database")
    parser.add_argument("db_path", help="Path of the sqlite database")
    parser.set_defaults(func=lambda x: parser.print_help())
    subparsers = parser.add_subparsers(dest="command")

    dbinfo = subparsers.add_parser("info", help="Show database information")
    dbinfo.add_argument("-sc", "--show-columns", action="store_true", help="Show the column of tables")
    dbinfo.set_defaults(func=db_info)

    explore = subparsers.add_parser("explore", help="Explore a table")
    explore.add_argument("table", help="Table to be explored")
    explore.set_defaults(func=explore_callback)

    args = parser.parse_args()
    args.func(args)