from argparse import ArgumentParser
import re

from . import api, utils, web
from .utils import config

from mylibs.colors.utils import Colors


def lists():
    print ('Available lists:')
    for l in api.lists():
        print ('-', l)

def user(args):
    print ('// Last download:', utils.last_dl(), end='\n\n')
    matches = api.user(args.name)
    for lst in matches:
        print (f'> List {lst}')
        for m in matches[lst]:
            Colors.pcolor_match('red', m, args.name)
        print()

def dl():
    print ('// Last download:', utils.last_dl(), end='\n\n')
    print ('Downloading the lists')
    session = web.Session()
    ret = session.dl_subscribers(config['lists']['lists'], True)
    if ret == False:
        exit('The lists cannot be downloaded, one possible issue can be that there is already such a list in your working directory.')
    timestamp_path = utils.abspath(config['lists']['storage-dir']) / config['lists']['dl-timestamp']
    utils.write_datetime(timestamp_path)
    print (f"Lists stored in {utils.abspath(config['lists']['storage-dir'])}")

def add(args):
    if not all(lst in config['lists']['lists'] for lst in args.lists):
        exit('One list is not valid. Aborting.')
    if not re.search('.*@.*\\..*', args.email):
        exit(f'The address {args.email} does not seem to be a valid email address. Aborting')
    for lst in args.lists:
        print (f'Adding new member ({args.email} {args.name}) to {lst}')
        session = web.Session()
        session.add_subscriber(lst, args.email, args.name)

def remove(args):
    if not all(lst in config['lists']['lists'] for lst in args.lists):
        exit('One list is not valid. Aborting.')
    if not re.search('.*@.*\\..*', args.email):
        exit(f'The address {args.email} does not seem to be a valid email address. Aborting')
    for lst in args.lists:
        print (f'Removing member {args.email} from {lst}')
        session = web.Session()
        session.remove_subscriber(lst, args.email)

def check_subset():
    if not all(utils.lists_present().values()):
        exit ('Some lists are missing, run `sympa download` to download them.')
    chk1, diff1, chk2, diff2 = api.check_subset()
    if chk1:
        print ('(OK) qi-open is a subset of qi-seminar')
    else:
        print ('(NOK) the following users are in qi-open but not in qi-seminars')
        for user in diff1:
            print (f'- {user}')
    if chk2:
        print ('(OK) qi-members is a subset of qi-open')
    else:
        print ('(NOK) the following users are in qi-members but not in qi-open')
        for user in diff2:
            print (f'- {user}')

def check_names():
    print ('// Last download:', utils.last_dl(), end='\n\n')
    lists = api.check_names()
    if any(lists.values()):
        print ('These users do not have name:', end='\n\n')
        for lst, users in lists.items():
            if users:
                print (f'> List {lst}')
                for user in users:
                    print (user)
                print()
    else:
        print ('Every user have a name')

def check_errors():
    print ('// Last download:', utils.last_dl(), end='\n\n')
    lists = api.check_errors()
    if not lists:
        print ('(OK) No errors in any list.')
    for lst in lists:
        print (f'(NOK) Error(s) in list {lst}')


if __name__ == "__main__":
    parser = ArgumentParser(description="Manage sympa lists")
    subparsers = parser.add_subparsers(dest="commands")

    list_parser = subparsers.add_parser("lists", help="Display the mailing lists you can query.")

    dl_parser = subparsers.add_parser("download", help="Download the lists.")

    user_parser = subparsers.add_parser("user", help="Display information on a user.")
    user_parser.add_argument("name", help="User's name.")

    add_parser = subparsers.add_parser('add', help='Add a new member to a set of lists')
    add_parser.add_argument('lists', nargs='+', help='Lists in which to add the new member')
    add_parser.add_argument('email', help='Email of the new member')
    add_parser.add_argument('name', help='Name of the new member')

    remove_parser = subparsers.add_parser('remove', help='Remove a member from a set of lists')
    remove_parser.add_argument('lists', nargs='+', help='Lists from which to remove the member')
    remove_parser.add_argument('email', help='Email of the member')

    check_subset_parser = subparsers.add_parser('check-subset', help='Check whether qi-open is a subset of qi-seminar')

    check_names_parser = subparsers.add_parser('check-names', help='Check whether all users in the lists have a name')

    check_errors_parser = subparsers.add_parser('check-errors', help='Check if there are some errors in lists')

    args = parser.parse_args()
    match args.commands:
        case "lists":
            lists()
        case "download":
            dl()
        case "user":
            user(args)
        case 'add':
            add(args)
        case 'remove':
            remove(args)
        case 'check-subset':
            check_subset()
        case 'check-names':
            check_names()
        case 'check-errors':
            # exit('WIP')
            check_errors()
        case other:
            parser.print_usage()
