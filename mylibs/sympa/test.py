from .config import config
from .utils import listpath, get_list_emails, diff_list, get_list_lines
from user import ListUser

from itertools import accumulate


def get_users_from_list(lst):
    list_users = [ListUser.parse_line(lst, line) for line in get_list_lines(lst)]
    name_emails = {}
    for user in list_users:
        if user.name in name_emails:
            name_emails[user.name].append(user.email)
        else:
            name_emails[user.name] = [user.email]
    return name_emails


def get_users_from_lists(lists):
    name_emails_maps = {lst: get_users_from_list(lst) for lst in lists}
    names = accumulate(name_emails_maps, func=set.intersection, initial=set())
    name_emails = {names: {}}
    for name in names:
        for lst, nemap in name_emails_maps.items():
            if name in nemap:
                name_emails[name][lst] = nemap[name]


if __name__ == "__main__":
    lst = 'qi-open'
    
