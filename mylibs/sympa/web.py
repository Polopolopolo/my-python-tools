from getpass import getpass
from pathlib import Path
from typing import List, Tuple
import tomllib

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from . import utils
from .utils import abspath, config

from myutils.web.downloader import Downloader, FilePresentException


class Session:
    def __init__(self) -> None:
        username, password = self.get_credentials()
        web = config['web']
        self.server_url = f"{web['protocol']}://{web['domain']}/{web['suffix']}"
        self.driver = self.sign_in(username, password)

    def get_credentials(self) -> Tuple[str, str] :
        ''' Return a username and a password to be used for signing in.  
        If there is a .credentials file, read it.
        Otherwise, ask the user.
        '''
        if abspath('.credentials').exists():
            creds = tomllib.loads(abspath('.credentials').read_text())
            return creds['username'], creds['passwd']
        else:
            username = input('username: ').strip()
            passwd = getpass()
            return username, passwd

    def sign_in(self, username: str, password: str) -> webdriver.Chrome:
        ''' Sign in to sympa.  
        Return a driver object to be used in the other methods.
        '''
        # create the driver
        options = Options()
        options.add_argument('--headless')
        driver = webdriver.Chrome(config['web']['chrome-driver-exec'], options=options)
        # access the login page
        driver.get(self.server_url)
        connection_btn = driver.find_element(By.NAME, 'action_login')
        connection_btn.click()
        # log in
        ipt_field = driver.find_element(By.ID, 'email_login')
        passwd_field = driver.find_element(By.ID, 'passwd')
        ipt_field.send_keys(username)
        passwd_field.send_keys(password)
        passwd_field.send_keys(Keys.ENTER)
        return driver

    def dl_subscribers(self, lsts: List[str], names: bool=True) -> bool:
        '''Download the list of all subscribers of a list `lst`.  
            Once all files are downloaded, they are moved to the directory specified in the config file.
            This method simply prepares the downloader object and returns it.
        '''
        # prepare Download objects for each list
        dls = []
        for lst in lsts:
            url = self.server_url + f'/export_member/{lst}'
            if not names:
                url += '/light'
            dl = Downloader(url, f"{lst}@{config['web']['domain'][4:]}.txt", label=lst)
            try:
                dl.start_dl(self.driver)
            except FilePresentException:
                return False
            else:
                dls.append(dl)
        # wait for the downloader objects to finish downloading
        while not all(dl.finished() for dl in dls):
            continue
        # move the downloaded files to specified location
        for dl in dls:
            dl.move(utils.abspath(Path(config['lists']['storage-dir']) / dl.dl_name))
        return True

    def check_errors(self, lst: str) -> bool:
        ''' Check the potential errors on a list `lst`.
        Return True if there are errors, and False otherwise.
        '''
        # open the sympa home page for this list
        url = self.server_url + f'/info/{lst}'
        self.driver.get(url)
        # get error rate and return True or False accordingly
        link_elements = self.driver.find_elements(By.TAG_NAME, 'a')
        errors_element = next(el for el in link_elements if el.get_attribute('href') and f'reviewbouncing/{lst}' in el.get_attribute('href'))
        rate = errors_element.text.split()[-1]
        return rate != '0%)'

    def add_subscriber(self, lst: str, email: str, name: str, quiet: bool=False):
        '''Add a new member (`email`, `name`) to the list `lst`  
        If quiet == True, the new member is not informed that they just be added to the list
        '''
        # open the sympa page for batch adding and fill it with member's info
        url = self.server_url + f'/import/{lst}'
        self.driver.get(url)
        dump = self.driver.find_element(By.ID, 'dump')
        dump.send_keys(f'{email} {name}')
        # tick quiet box if quiet == True
        quiet_tickbox = self.driver.find_element(By.ID, 'quiet')
        if quiet:
            quiet_tickbox.click()
        # click on register button
        import_btn = self.driver.find_element(By.NAME, 'action_import')
        import_btn.click()

    def remove_subscriber(self, lst: str, email: str, quiet: bool=False):
        ''' Remove a member (`email`) from the list `lst`.
        If quiet == True, the member is not informed that they just be removed from the list
        '''
        # open member's page
        url = f'https://www-soc.lip6.fr/wws/editsubscriber/{lst}?email={email.replace('@', '%40')}'
        self.driver.get(url)
        # tick quiet box if quiet == True
        quiet_tickbox = self.driver.find_element(By.ID, 'quiet')
        if quiet:
            quiet_tickbox.click()
        # click on remove button
        remove_btn = self.driver.find_element(By.NAME, 'action_del')
        remove_btn.click()
        # click on confirm button
        confirm_btn = self.driver.find_element(By.NAME, 'response_action_confirm')
        confirm_btn.click()

