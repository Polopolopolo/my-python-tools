from pathlib import Path, PosixPath
from datetime import datetime
import tomllib
from typing import Tuple


config = tomllib.loads((Path(__file__).parent / 'config.toml').read_text())


# PATHS

def abspath(rel_path: str) -> PosixPath:
    ''' Return the path of a file belonging to this directory
    '''
    return Path(__file__).parent / rel_path

def listpath(lst: str) -> PosixPath:
    ''' Return the path of the file storing the list `lst`
    '''
    return abspath(config['lists']['storage-dir']) / f'{lst}@soc.lip6.fr.txt'

# LISTS FILES

def get_list_lines(lst: str) -> list:
    ''' Read the file storing the list `lst` and return its lines
    '''
    path = listpath(lst)
    return path.read_text().splitlines()

def get_list_emails(lst: str) -> list:
    ''' Return the list of emails stored in the file storing the list `lst`
    '''
    return [line.split()[0] for line in get_list_lines(lst)]

def lists_present():
    return {l: listpath(l).exists() for l in config['lists']['lists']}

def diff_list(emails1: list, emails2: list) -> Tuple[set, set]:
    ''' Given two lists of emails L1, L2, return the differences L1 - L2 and L2 - L1
    '''
    diff1 = set(emails1).difference(emails2)
    diff2 = set(emails2).difference(emails1)
    return diff1, diff2

# DATES

def read_datetime(path: str) -> datetime:
    ''' Return the datetime stored in a file at `path`
    '''
    content = Path(path).read_text()
    return datetime.strptime(content, '%Y-%m-%d %H:%M:%S')

def write_datetime(path: str) -> None:
    ''' Write the datetime "now" in a file at `path`
    '''
    now = datetime.now()
    now = now.strftime('%Y-%m-%d %H:%M:%S')
    Path(path).write_text(now)

def last_dl() -> datetime:
    ''' Return the last time the lists where downloaded if the file exists, or None otherwise
    '''
    timestamp_path = abspath(config['lists']['storage-dir']) / config['lists']['dl-timestamp']
    if timestamp_path.exists():
        return read_datetime(timestamp_path)
    else:
        return
