from typing import List

from .utils import get_list_lines


class ListUser:
    def __init__(self, lst: str, email: str, name: str) -> None:
        self.email = email
        self.name = name
        self.lst = lst

    @classmethod
    def parse_line(cls, lst: str, line: str):
        splt = line.split()
        if len(splt) == 1:
            return cls(lst, splt[0], '')
        else:
            return cls(lst, splt[0], ' '.join(splt[1:]))

    def is_bot(self) -> bool:
        return '[bot]' in self.name.lower()


class User:
    def __init__(self) -> None:
        self.list_users = []

    def append(self, list_user: ListUser):
        self.list_users.append(list_user)

    def merge(self, user):
        for list_user in user.list_users:
            self.append(list_user)

    def check_unique_name(self):
        return len(set(list_user.name for list_user in self.list_users)) == 1

    @classmethod
    def get_users_from_lists(cls, lists: List[str], multi_emails):
        users_dct = {}
        # create User
        for lst in lists:
            list_users = [ListUser.parse_line(lst, line) for line in get_list_lines(lst)]
            for list_user in list_users:
                if list_user.email in users_dct:
                    users_dct[list_user.email].append(list_user)
                else:
                    users_dct[list_user.email] = User()
                    users_dct[list_user.email].append(list_user)
        # handle multi_emails
        final_users = []
        for user_email, user in users_dct.items():
            multi_email = [x for x in multi_emails if user_email in x]
            if not multi_email:
                final_users.append(user)
                continue
            else:
                multi_email = multi_email[0]
            for same_user in [x for x in users_dct if x in multi_email and x != user_email]:
                user.merge(users_dct[same_user])
            final_users.append(user)
            multi_emails.remove(multi_email)
        # raise warning if some names are not unique
        for user in final_users:
            if not user.check_unique_name():
                print (user.list_users[0].name)
        return final_users
