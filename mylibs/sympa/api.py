from typing import Tuple

from .utils import config, diff_list, get_list_emails, listpath
from .web import Session


def lists() -> list:
    ''' Return the list of lists stored in the config file
    '''
    return config['lists']['lists']

def user(name: str) -> dict:
    ''' Return a dictionnary mapping every list to the list's members whose names or emails match `name`
    '''
    matches = {}
    for lst in lists():
        path = listpath(lst)
        for l in path.read_text().splitlines():
            if name in l.lower():
                if lst not in matches:
                    matches[lst] = [l]
                else:
                    matches[lst].append(l)
    return matches

def check_subset() -> Tuple[bool, set]:
    ''' Check that the following rules are respected:
        - qi-open is included in qi-seminar
        - qi-members is included in qi-open

        If they are not, return the errors
    '''
    qimembers = get_list_emails('qi-members')
    qiopen = get_list_emails('qi-open')
    qiseminar = get_list_emails('qi-seminar')
    diff1, _ = diff_list(qiopen, qiseminar)
    diff2, _ = diff_list(qimembers, qiopen)
    return len(diff1) == 0, diff1, len(diff2) == 0, diff2

def check_names() -> list:
    ''' Check that all users have a name.  
    Return users who do not have any.
    '''
    lists = {lst: [] for lst in config['lists']['lists']}
    for lst in lists:
        path = listpath(lst)
        for l in path.read_text().splitlines():
            if len(l.split()) < 2:
                lists[lst].append(l)
    return lists

def check_errors() -> list:
    ''' (WIP) Check if there are errors (as referred to by sympa)
    '''
    s = Session()
    return [lst for lst in config['lists']['lists'] if s.check_errors(lst)]
