from collections import namedtuple
from typing import Any, List

import bibtexparser

from . import utils



class EntryDict(dict):
    UnknownField = namedtuple("UnknownField", ["value"], defaults=["???"])

    def __getitem__(self, __key: Any) -> Any:
        try:
            return super().__getitem__(__key)
        except KeyError:
            return self.UnknownField()


class Library:
    def __init__(self, entries: List[EntryDict], hash: str):
        self.entries = entries
        self.hash = hash

    @staticmethod
    def clean_str(s):
        return " ".join(s.replace("{\\'e}", "é").replace("\n", " ").replace(" and ", " ").split())

    @classmethod
    def read_bibfile(cls, bibfile_path):
        entries = bibtexparser.parse_file(bibfile_path).entries
        parsed_entries = []
        for e in entries:
            fields = EntryDict(e.fields_dict)
            dct = EntryDict(
                key=e.key,
                authors=cls.clean_str(fields["author"].value),
                title=cls.clean_str(fields["title"].value),
            )
            parsed_entries.append(dct)
        return cls(parsed_entries, utils.get_hash_file(bibfile_path))

    @classmethod
    def read_store(cls, store_path="", jdct=None):
        assert store_path or jdct
        if not jdct:
            jdct = utils.read_json(store_path)
        entries = [EntryDict(e) for e in jdct["entries"]]
        return cls(entries, jdct["hash"])

    def store(self, store_path):
        dct = {"hash": self.hash, "entries": self.entries}
        utils.write_json(dct, store_path)


def search(query, bibfile_path, store_path):
    jdct = utils.read_json(store_path)
    if jdct and jdct["hash"] == utils.get_hash_file(bibfile_path):
        yield {"status": "read_store"}
        lib = Library.read_store(jdct=jdct)
    else:
        yield {"status": "read_bibfile"}
        lib = Library.read_bibfile(bibfile_path)
        lib.store(store_path)
    matches = [e for e in lib.entries if query.lower() in e["authors"].lower() or query.lower() in e["title"].lower()]
    yield {"status": "matches", "content": matches}


def add(key, local_bibfile, bibfile):
    entries = bibtexparser.parse_file(bibfile).entries
    for e in entries:
        if e.key == key:
            with open(local_bibfile, "a") as f:
                f.write(e.raw + "\n\n")
            yield {"status": "found"}
    yield {"status": "not found"}
