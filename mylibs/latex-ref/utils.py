import json
import os

from hashlib import sha256



def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)


def get_hash_file(filepath):
    with open(filepath) as f:
        content = f.read()
    return sha256(content.encode()).hexdigest()


def read_json(filepath):
    if not os.path.exists(filepath):
        return None
    with open(filepath) as f:
        jdct = json.load(f)
    return jdct


def write_json(obj, filepath):
    with open(filepath, "w") as f:
        json.dump(obj, f)


class Colors:
    RED     = "\033[1;31m"
    BLUE    = "\033[1;34m"
    CYAN    = "\033[1;36m"
    GREEN   = "\033[0;32m"
    ORANGE  = "\033[0;33m"
    RESET   = "\033[0;0m"
    BOLD    = "\033[;1m"
    REVERSE = "\033[;7m"

    @classmethod
    def color(cls, text, color):
        return getattr(cls, color.upper()) + text + cls.RESET
