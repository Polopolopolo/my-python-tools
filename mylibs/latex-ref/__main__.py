###########
# IMPORTS #
###########
import argparse
import logging
import pprint

from . import utils
from . import api


#############
# CALLBACKS #
#############
def search(args):
    for ret in api.search(args.query, args.bibfile, args.storefile):
        if ret["status"] == "read_store":
            logging.info("Storefile up-to-date, using storefile.")
        elif ret["status"] == "read_bibfile":
            logging.info("Storefile outdated, updating.")
        elif ret["status"] == "matches":
            matches = ret["content"]
    for m in matches:
        s = pprint.pformat(m)
        idx = s.lower().index(args.query.lower())
        s = s[:idx] + utils.Colors.color(s[idx:idx + len(args.query)], "red") + s[idx + len(args.query):]
        print(s, end="\n\n")

def add(args):
    for ret in api.add(args.id, args.local_bibfile, args.bibfile):
        if ret["status"] == "found":
            logging.info(f"Entry added to {args.local_bibfile}.")
            return
        elif ret["status"] == "not found":
            logging.error(f"Entry added to {args.local_bibfile}.")


########
# MAIN #
########
if __name__ == "__main__":
    cfg = utils.read_json(utils.abspath("config.json"))

    parser = argparse.ArgumentParser(description="Manage references of any latex project.")
    subparsers = parser.add_subparsers(dest="command")

    log_parser = subparsers.add_parser("log", help="Display log file's content.")

    search_parser = subparsers.add_parser("search", help="Search for an entry.")
    search_parser.add_argument(dest="query", help="Query string for the search.")
    search_parser.add_argument("--bibfile",
                               help=f"Path of the bibfile to search into (default={cfg['default']['bibfile']}).",default=cfg['default']['bibfile'])
    search_parser.add_argument("--storefile",
                               help=f"Path of the store to search into or store into (default={cfg['default']['store_path']}).",
                               default=cfg['default']['store_path'])

    add_parser = subparsers.add_parser("add", help="Add an entry to the local bibfile.")
    add_parser.add_argument(dest="id", help="Entry's id (entry's key)")
    add_parser.add_argument("--local-bibfile",
                            help=f"Path of the local bibfile (default={cfg['default']['ref_path']}).",
                            default=cfg['default']['ref_path'])
    add_parser.add_argument("--bibfile",
                            help=f"Path of the bibfile to search into (default={cfg['default']['bibfile']}).",default=cfg['default']['bibfile'])

    args = parser.parse_args()
    if args.command == "log":
        with open(utils.abspath(cfg["logpath"])) as f:
            print (f.read())
    else:
        logging.basicConfig(filename=utils.abspath(cfg["logpath"]), filemode="w", level=logging.DEBUG)
        if args.command == "search":
            search(args)
        elif args.command == "add":
            add(args)
        else:
            parser.print_usage()
