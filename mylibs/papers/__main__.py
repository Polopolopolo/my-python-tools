###########
# IMPORTS #
###########

import argparse
from functools import partial
import json
import os
import sqlite3
import subprocess


###########
# GLOBALS #
###########

DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/papers/"

DB_SCHEME = {
    "papers": [
        "title",
        "filepath",
        "url",
        "authors",
        "tags",
        "notes",
    ]
}


#########
# UTILS #
#########

def print_color(text, color_code, **kwargs):
    print (f"\033[{color_code}m{text}\033[0m", **kwargs)


def db_fetchall(conn=None, table="Papers"):
    if not conn:
        conn = sqlite3.connect(os.path.join(DIRPATH, "papers.db"))
        conn.row_factory = sqlite3.Row
    entries = conn.execute(f"SELECT * FROM {table}").fetchall()
    return entries


#############
# CALLBACKS #
#############

def listdir(curdir):
    papers = db_fetchall()
    papers = {pap["Filepath"] : pap["Title"] for pap in papers}
    pdf_files = {os.path.abspath(f): f for f in os.listdir(".") if f.endswith(".pdf")}
    for f in pdf_files:
        print_color(pdf_files[f], 93, end=" ")
        if f in papers:
            print (papers[f])
        else:
            print ("???")


def get_papers():
    papers = db_fetchall()
    for (i, pap) in enumerate(sorted(papers, key=lambda row: row["Title"])):
        print (i, pap["Title"])


def add_paper(dct=None):
    keys = [k[0].upper() + k[1:] for k in DB_SCHEME["papers"]]

    if not dct:
        with open(os.path.join(DIRPATH, "prompt.json"), "w") as f:
            json.dump({k: "" for k in keys}, f, indent=2)
        proc = subprocess.run(["nano", os.path.join(DIRPATH, "prompt.json")])
        if proc.returncode != 0:
            print ("Error during process execution, aborting...")
        else:
            with open(os.path.join(DIRPATH, "prompt.json")) as f:
                dct = json.load(f)

    else:
        # fill every missing fields of dct
        dct = {key: dct[key] if key in dct else "" for key in keys}

    if not dct["Title"] or not (dct["Filepath"] or dct["Url"]):
        exit("Title and (Filepath or Url) are needed : aborting...")

    conn = sqlite3.connect(os.path.join(DIRPATH, "papers.db"))
    query_str = f"""INSERT INTO Papers ({','.join(keys)})
VALUES (?, ?, ?, ?, ?, ?);"""
    dct["Filepath"] = os.path.abspath(dct["Filepath"])
    conn.execute(query_str, [dct[k] for k in keys])
    conn.commit()


def rm(paper_id):
    pap = paper(paper_id)
    if not paper:
        print (f"No paper for id {paper_id}")
    else:
        conn = sqlite3.connect(os.path.join(DIRPATH, "papers.db"))
        if len(conn.execute("SELECT * FROM Papers WHERE Title = ? AND Filepath = ?",
                (pap["Title"], pap["Filepath"])).fetchall()) != 1:
            print("Error : there is not only one paper matching these conditions, aborting...")
        else:
            conn.execute("DELETE FROM Papers WHERE Title = ? AND Filepath = ?",
                    (pap["Title"], pap["Filepath"]))
            conn.commit()
            
            
def mv(paper_id, dirpath):
    pap = paper(paper_id)
    if not paper:
        print (f"No paper for id {paper_id}")
    else:
        conn = sqlite3.connect(os.path.join(DIRPATH, "papers.db"))
        if len(conn.execute("SELECT * FROM Papers WHERE Title = ? AND Filepath = ?",
                (pap["Title"], pap["Filepath"])).fetchall()) != 1:
            print("Error : there is not only one paper matching these conditions, aborting...")
        else:
            conn = sqlite3.connect(os.path.join(DIRPATH, "papers.db"))
            dirpath = os.path.abspath(dirpath)
            new_path = os.path.join(dirpath, os.path.basename(pap["Filepath"]))

            proc = subprocess.run(["mv", pap["Filepath"], dirpath])
            if proc.returncode != 0:
                print ("Error when trying to move the file, aborting...")
            else:
                conn.execute("UPDATE Papers SET Filepath = ? WHERE Title = ? AND Filepath = ?",
                        (new_path, pap["Title"], pap["Filepath"]))
                conn.commit()


def paper(paper_id):
    paper = None
    papers = db_fetchall()
    papers = sorted(papers, key=lambda row: row["Title"])
    if paper_id.isnumeric() and int(paper_id) < len(papers):
        paper = papers[int(paper_id)]
    else:
        matching_papers = [ (i, pap) for (i, pap) in enumerate(papers)
                            if paper_id.lower() in pap["Title"].lower() ]
        if len(matching_papers) == 1:
            paper = matching_papers[0][1]
        elif len(matching_papers) > 1:
            print (f"Multiple papers match {paper_id}:")
            for (i, pap) in matching_papers:
                print (i, pap["Title"])
    if paper:
        return paper
    else:
        print (f"No paper for id {paper_id}")


def check():
    papers = sorted(db_fetchall(), key=lambda row: row["Title"])
    errors = [ (i, paper) for (i, paper) in enumerate(papers) if not os.path.isfile(paper["Filepath"]) ]
    return errors


def callback(command, args):
    if command == "ls":
        listdir(os.path.abspath("."))
    elif command == "list":
        get_papers()
    elif command == "add":
        add_paper(args.dict)
    elif command == "rm":
        rm(args.id)
    elif command == "mv":
        mv(args.id, args.dirpath)
    elif command == "paper":
        if args.field.lower() not in DB_SCHEME["papers"]:
            print (f"Invalid field name : {args.field}")
        else:
            pap = paper(args.id)
            if pap:
                print (pap[args.field])
    elif command == "check":
        errors = check()
        if not errors:
            print ("All good")
        else:
            print ("Wrong path for these papers:")
            print ("\n\n".join(f"{i}: {paper['Title']}\n({paper['Filepath']})" for (i, paper) in errors))


########
# MAIN #
########

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="command")

    ls_subparser = subparsers.add_parser("ls", help="List the papers in the current directory")
    ls_subparser.set_defaults(callback=partial(callback, "ls"))

    list_subparser = subparsers.add_parser("list", help="List the papers in the database")
    list_subparser.set_defaults(callback=partial(callback, "list"))

    add_subparser = subparsers.add_parser("add", help="Add a paper in the database")
    add_subparser.add_argument("--dict", help="key-value map with paper information")
    add_subparser.set_defaults(callback=partial(callback, "add"))

    rm_subparser = subparsers.add_parser("rm", help="Remove a paper from the database")
    rm_subparser.add_argument("id", help="Id of the paper (can be obtained from 'list' command")
    rm_subparser.set_defaults(callback=partial(callback, "rm"))
    
    mv_subparser = subparsers.add_parser("mv", help="Move a to a new location and apply the changes to the database")
    mv_subparser.add_argument("id", help="Id of the paper (can be obtained from 'list' command")
    mv_subparser.add_argument("dirpath", help="Path of the directory where to move the file")
    mv_subparser.set_defaults(callback=partial(callback, "mv"))

    paper_subparser = subparsers.add_parser("paper", help="Get a paper's field from its index or title")
    paper_subparser.add_argument("id")
    paper_subparser.add_argument("field")
    paper_subparser.set_defaults(callback=partial(callback, "paper"))

    check_subparser = subparsers.add_parser("check", help="Check wheither all the papers' path are correct or not")
    check_subparser.set_defaults(callback=partial(callback, "check"))

    args = parser.parse_args()
    if hasattr(args, "callback"):
        args.callback(args)
    else:
        parser.print_usage()
