###########
# IMPORTS #
###########
import argparse
import csv
import os
import subprocess


###########
# GLOBALS #
###########
DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/tabletex/"
TEX_DIR = os.path.join(DIRPATH, "tex")
TEX_FILE = os.path.join(TEX_DIR, "main.tex")
PDF_FILE = os.path.join(TEX_DIR, "main.pdf")


#########
# UTILS #
#########
def get_csv_rows(filepath):
    with open(filepath, newline="") as f:
        reader = csv.reader(f, delimiter=";")
        rows = list(reader)
    return rows


def to_tex_row(row, hline=False):
    row = [col.split("\n") for col in row]
    height = max(len(col) for col in row)
    for (i, col) in enumerate(row):
        row[i] += ["~"] * (height - len(col))
    
    tex_row = ""
    for i in range(height):
        tex_row += " & ".join(col[i] for col in row) + "\\\\"
        if i != height - 1:
            tex_row += "\n        "
    if hline:
        tex_row += " \\hline"

    return tex_row


#######
# API #
#######
def gen_table_code(csv_path, hline=True):
    rows = get_csv_rows(csv_path)
    return """\
\\begin{{table}}
    \\begin{{tabular}}{tabular_opts}
        \hline
        {tex_rows}
    \\end{{tabular}}
\\end{{table}}""".format(
    tabular_opts="{|" + "l|" * len(rows) + "}",
    tex_rows="\n        ".join(to_tex_row(row, hline) for row in rows))


def render(tex_code, doc_class, pdf_viewer):
    with open(TEX_FILE, "w") as f:
        f.write("""\
\\documentclass{{{doc_class}}}
\\begin{{document}}
{tex_code}
\\end{{document}}""".format(doc_class=doc_class, tex_code=tex_code))
    subprocess.run(["pdflatex", "-synctex=1", "-interaction=nonstopmode", f"-output-directory={TEX_DIR}", TEX_FILE])
    subprocess.run([pdf_viewer, PDF_FILE])


def callback(parser, args):
    if args.csv:
        tex_code = gen_table_code(args.csv, not args.no_hline)
        if args.render:
            render(tex_code, args.render_docclass, args.render_viewer)
        else:
            print (tex_code)
    else:
        parser.print_usage()


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a table in latex code from a csv file")
    parser.add_argument("csv", help="path to the csv file")
    parser.add_argument("--render", action="store_true", help="Show the table in pdf")
    parser.add_argument("--render-docclass", default="article", help="Document class for latex rendering (default is article)")
    parser.add_argument("--render-viewer", default="evince", help="Software for pdf view (default is evince)")
    parser.add_argument("--no_hline", default="False", action="store_true", help="No horizontal line between rows")


    args = parser.parse_args()
    callback(parser, args)
