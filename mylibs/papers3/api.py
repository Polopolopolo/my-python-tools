from . import utils
import shutil
import sqlite3


def ls_api(root_path, title_filter=""):
    ret1 = [{"dir_path": dp} for dp in utils.list_papers_folders(root_path)]
    ret2 = []
    for r in ret1:
        dp = r["dir_path"]
        if not utils.check_info_file(dp):
            r["retcode"] = 1
        else:
            info = utils.get_info_content(dp)
            if not utils.check_info_content(info):
                r["retcode"] = 2
            else:
                r["retcode"] = 0
                r["data"] = info
                if not title_filter\
                    or title_filter.lower() in info["title"].lower().replace(" ", ""):
                    ret2.append(r)
    return ret2


def create_info(path):
    shutil.copy(utils.abspath("template/.info"), path)


def search_api(query_str):
    db = sqlite3.connect(":memory:")
    db.execute("CREATE TABLE Papers (Authors, Tags, Title, Url)")
    papers = ls_api(".")
    for pap in sorted(papers, key=lambda x: x["data"]["title"]):
        if pap["retcode"]:
            return 1
        else:
            for auth in pap["data"]["authors"]:
                for tag in pap["data"]["tags"]:
                    db.execute(f"""INSERT INTO Papers VALUES ("{auth}", "{tag}", "{pap['data']['title']}", "{pap['data']['url']}")""")
            rows = db.execute(f"SELECT * FROM Papers WHERE {query_str}")
            return rows
