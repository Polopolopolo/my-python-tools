import PyQt5
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets, QtCore, QtGui

from .checkable_combobox import CheckableComboBox
from .new_paper import NewPaperDialog

from . import api, utils
import os
import shutil


class Ressource:
    programs = {
        "md"    : "brave-browser",
        "pdf"   : "okular",
        "tex"   : "code",
        "xopp"  : "xournalpp",
    }

    template_paths = {
        "md": utils.abspath("template/template.md"),
        "tex": utils.abspath("template/template.tex"),
        "xopp": utils.abspath("template/template.xopp"),
    }

    def __init__(self, filepath):
        self.filepath = filepath
        self.filename = os.path.basename(filepath)
        self.ext = os.path.splitext(filepath)[1][1:]

        self.label = QtWidgets.QLabel(self.filename)
        self.label.setWordWrap(True)
        self.button = QtWidgets.QPushButton("Open")
        self.button.clicked.connect(self.open)

        self.process = QtCore.QProcess()

    def open(self):
        print ("New process :", self.programs[self.ext] + " " + f'"{self.filepath}"')
        self.process.startDetached(self.programs[self.ext] + " " + f'"{self.filepath}"')


class Folder:
    program = "nautilus"
    def __init__(self, dirpath):
        self.dirpath = dirpath
        self.label = QtWidgets.QLabel("Folder")
        self.button = QtWidgets.QPushButton("Open")
        self.button.clicked.connect(self.open)

        self.process = QtCore.QProcess()

    def open(self):
        self.process.startDetached(self.program + " " + f'"{self.dirpath}"')


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi(utils.abspath("main_window.ui"), self)
        self.resize(QtWidgets.QDesktopWidget().availableGeometry().size())

        self.root_path = "/home/paul/espaces/phd/Papers"

        self.menubar = QtWidgets.QMenuBar()
        file_menu = self.menubar.addMenu("File")
        new_paper_action = QtWidgets.QAction("New Paper", self)
        new_paper_action.triggered.connect(self.new_paper_cb)
        file_menu.addAction(new_paper_action)

        self.setMenuBar(self.menubar)

        self.sort = QtWidgets.QComboBox()
        self.filters_tags = CheckableComboBox()
        self.filters_authors = CheckableComboBox()

        self.filter_sort.layout().addWidget(self.sort, 1, 3)
        self.sort.addItem("A-Z")
        self.sort.addItem("Z-A")

        self.all_papers = api.ls_api(self.root_path)
        if any(pap["retcode"] for pap in self.all_papers):
            exit("Error : a `.info` file is not valid.")
        self.papers = []
        self.papers_list.currentItemChanged.connect(self.fill_info_cb)
        self.papers_list.currentItemChanged.connect(self.fill_ressources_cb)

        self.filters_authors.itemCheckedSignal.connect(self.author_item_checked_cb)
        self.filters_authors.itemUncheckedSignal.connect(self.author_item_unchecked_cb)
        self.filters_tags.itemCheckedSignal.connect(self.tag_item_checked_cb)
        self.filters_tags.itemUncheckedSignal.connect(self.tag_item_unchecked_cb)

        self.filter_sort.layout().addWidget(self.filters_authors, 1, 1)
        self.filters_authors.addItem("All")
        self.filters_authors.setCheckState(0, QtCore.Qt.Checked)
        self.authors_set = sorted({auth for pap in self.all_papers for auth in pap["data"]["authors"] if auth})
        for (i, auth) in enumerate(self.authors_set):
            self.filters_authors.addItem(auth)
            self.filters_authors.setCheckState(i + 1, QtCore.Qt.Checked)

        self.filter_sort.layout().addWidget(self.filters_tags, 2, 1)
        self.filters_tags.addItem("All")
        self.filters_tags.setCheckState(0, QtCore.Qt.Checked)
        self.tags_set = sorted({tag.lower().title() for pap in self.all_papers for tag in pap["data"]["tags"] if tag})
        for (i, tag) in enumerate(self.tags_set):
            self.filters_tags.addItem(tag)
            self.filters_tags.setCheckState(i + 1, QtCore.Qt.Checked)

        self.refresh_btn.clicked.connect(self.refill_papers_ls_cb)

        self.new_file_btn.clicked.connect(self.create_new_file_cb)

        self.refill_papers_ls_cb()

    def new_paper_cb(self):
        self.new_paper_dialog = NewPaperDialog(self.root_path)
        self.new_paper_dialog.show()

    def sort_papers(self):
        if self.sort.currentText() == "A-Z":
            self.papers = sorted(self.papers, key=lambda pap: pap["data"]["title"])
        elif self.sort.currentText() == "Z-A":
            self.papers = sorted(self.papers, key=lambda pap: pap["data"]["title"], reverse=True)

    def author_item_checked_cb(self, index):
        if self.filters_authors.itemText(index) == "All":
            for i in range(self.filters_authors.count()):
                self.filters_authors.setCheckState(i, QtCore.Qt.Checked)

    def author_item_unchecked_cb(self, index):
        if self.filters_authors.itemText(index) == "All":
            for i in range(self.filters_authors.count()):
                self.filters_authors.setCheckState(i, QtCore.Qt.Unchecked)

    def tag_item_checked_cb(self, index):
        if self.filters_tags.itemText(index) == "All":
            for i in range(self.filters_tags.count()):
                self.filters_tags.setCheckState(i, QtCore.Qt.Checked)

    def tag_item_unchecked_cb(self, index):
        if self.filters_tags.itemText(index) == "All":
            for i in range(self.filters_tags.count()):
                self.filters_tags.setCheckState(i, QtCore.Qt.Unchecked)



    def filter_papers(self):
        # search bar filter
        # works only for a single word atm
        search_bar_word = self.search_bar.text().strip().lower()
        if search_bar_word:
            search_bar_filter_func = lambda pap: search_bar_word.lower() in pap["data"]["title"].lower()
        else:
            search_bar_filter_func = lambda pap: True

        # tags filter
        filter_tags = {self.filters_tags.itemText(i)    for i in range(self.filters_tags.count()) 
                                                        if self.filters_tags.itemChecked(i)}
        filter_tags_func = lambda pap: filter_tags.intersection(
                                        (tag.lower().title() for tag in pap["data"]["tags"]))

        # authors filter
        filter_authors = {self.filters_authors.itemText(i)  for i in range(self.filters_authors.count()) 
                                                            if self.filters_authors.itemChecked(i)}
        filter_authors_func = lambda pap: filter_authors.intersection(pap["data"]["authors"])

        self.papers = [pap  for pap in self.papers
                            if search_bar_filter_func(pap) and filter_authors_func(pap) and filter_tags_func(pap)]

    def current_paper(self):
        if self.papers_list.currentItem():
            pap_title = self.papers_list.currentItem().text()
            pap_search = [pap for pap in self.papers if pap["retcode"] == 0 and pap["data"]["title"] == pap_title]
            if pap_search:
                pap = pap_search[0]
                return pap

    def refill_papers_ls_cb(self):
        self.clear_ressources()
        self.papers_list.clear()

        self.papers = api.ls_api(self.root_path)
        self.sort_papers()
        self.filter_papers()
        for pap in self.papers:
            if pap["retcode"] == 0:
                self.papers_list.addItem(pap["data"]["title"])
            else:
                item = QtWidgets.QListWidgetItem(pap["dir_path"])
                item.setForeground(QtCore.Qt.red)
                self.papers_list.addItem(item)

    def fill_info_cb(self):
        pap = self.current_paper()
        if pap:
            self.title.setText(pap["data"]["title"])
            self.authors.setText(", ".join(pap["data"]["authors"]))
            self.tags.setText(", ".join(pap["data"]["tags"]))
            self.url.setText(pap["data"]["url"])
            if "date" in pap["data"].keys():
                self.date.setText(pap["data"]["date"])

    def clear_ressources(self):
        for i in range(self.ressources.layout().rowCount()):
            for j in range(self.ressources.layout().columnCount()):
                item = self.ressources.layout().itemAtPosition(i, j)
                if item:
                    widget = item.widget()
                    self.ressources.layout().removeWidget(widget)
                    widget.setParent(None)

    def fill_ressources_cb(self):
        self.clear_ressources()

        if self.papers_list.currentItem():
            pap_title = self.papers_list.currentItem().text()
            pap_search = [pap for pap in self.papers if pap["retcode"] == 0 and pap["data"]["title"] == pap_title]
            self.ressource_objs = []
            if pap_search:
                pap = pap_search[0]
                dir_ressource = Folder(pap["dir_path"])
                self.ressource_objs.append(dir_ressource)
                self.ressources.layout().addWidget(dir_ressource.label, 0, 0)
                self.ressources.layout().addWidget(dir_ressource.button, 0, 1)
                for i, fp in enumerate( [ fp for fp in os.listdir(pap["dir_path"]) if os.path.splitext(fp)[1] ] ):
                    ressource = Ressource( os.path.join(pap["dir_path"], fp) )
                    self.ressource_objs.append(ressource)
                    self.ressources.layout().addWidget(ressource.label, i + 1, 0)
                    self.ressources.layout().setColumnStretch(0, 1)
                    self.ressources.layout().addWidget(ressource.button, i + 1, 1)

                height = self.ressources.layout().rowCount()
                self.ressources.layout().addWidget(QtWidgets.QWidget(), height, 0)
                self.ressources.layout().setRowStretch(height, 1)

    def create_new_file_cb(self):
        text = self.new_file_line_edit.text()
        pap = self.current_paper()
        if text and pap:
            curdir = pap["dir_path"]
            if text not in os.listdir(curdir):
                ext = os.path.splitext(text)[1][1:]
                if ext in Ressource.template_paths:
                    shutil.copy(Ressource.template_paths[ext], os.path.join(curdir, text))
                else:
                    with open(os.path.join(curdir, text), "w") as f: pass # just create an empty file
                self.fill_ressources_cb()
                return
        msg = QtWidgets.QMessageBox()
        msg.setText("Something went wrong went creating a new file.")
        msg.exec()


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    app.setWindowIcon(QtGui.QIcon("/home/paul/Images/icons/Qnewspaper.png"))

    main_window = MainWindow()
    main_window.setWindowTitle("Papers")
    main_window.show()

    app.exec_()
