from datetime import datetime
import os
import typeguard
from typing import List
import yaml


DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/papers3"

INFO_SCHEME = {"title": str, "authors": List[str], "tags": List[str], "url": str, "date": str}


def abspath(rel_path):
    return os.path.join(DIRPATH, rel_path)

def info_path(dirpath):
    return os.path.join(dirpath, ".info")


def list_papers_folders(root_path):
    return [fp for (fp, _, files) in os.walk(root_path) if ".info" in files]


def check_info_file(dirpath):
    try:
        with open(info_path(dirpath)) as f:
            info = yaml.safe_load(f)
    except:
        return False
    else:
        return True


def get_info_content(dirpath):
    with open(info_path(dirpath)) as f:
        info = yaml.safe_load(f)
    return info


def check_info_content(info):
    if not info:
        return False
    for (k, v) in INFO_SCHEME.items():
        if k not in info:
            return False
        else:
            try:
                typeguard.check_type(k, info[k], v)
            except TypeError:
                return False
    return True


def get_year(s_date: str):
    try:
        year = int(s_date)
        return year
    except:
        try:
            date = datetime.strptime(s_date, "%Y-%m-%d")
            return date.year
        except:
            print (f"Error: Date {s_date} has wrong format.")


def papers_id(info):
    if not check_info_content(info):
        return
    auths = info["authors"]
    year = get_year(info["date"])
    if year and auths and all(a for a in auths):
        if len(auths) == 1:
            auth_part = auths[0].split()[-1]
        else:
            auth_part = "".join(a.split()[-1][0].upper() for a in auths)
        title_part = info["title"].split()
        while title_part[0].lower() in "a, the, on":
            title_part = title_part[1:]
        return f"{auth_part}{year}{title_part[0]}"
