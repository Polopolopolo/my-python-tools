from xml.etree.ElementTree import dump
from . import utils
import os
import shutil
import yaml

from PyQt5 import QtWidgets, QtGui, QtCore, uic


class NewPaperDialog(QtWidgets.QWidget):
    def __init__(self, root_path):
        super().__init__()
        self.root_path = root_path
        uic.loadUi(utils.abspath("new_paper.ui"), self)

        self.authors = [self.authors_lineEdit]
        self.tags = [self.tags_lineEdit]

        self.authors_Button.clicked.connect(self.authors_btn_cb)
        self.tags_Button.clicked.connect(self.tags_btn_cb)
        self.validate_Button.clicked.connect(self.validate_btn_cb)
        self.filepath_btn.mousePressEvent = self.filepath_btn_moussePressEvent

    def authors_btn_cb(self):
        self.authors.append(QtWidgets.QLineEdit())
        self.authors_list.layout().addWidget(self.authors[-1])

    def tags_btn_cb(self):
        self.tags.append(QtWidgets.QLineEdit())
        self.tags_list.layout().addWidget(self.tags[-1])

    def validate_btn_cb(self):
        info = {"title": "", "authors": [], "tags": [], "url": ""}
        if not self.title_lineEdit.text():
            error_msg = QtWidgets.QErrorMessage()
            error_msg.showMessage("Title must be specified !")
            error_msg.exec()
            return

        info["title"] = self.title_lineEdit.text().strip()
        info["authors"] = [x.text().strip() for x in self.authors]
        info["tags"] = [x.text().strip() for x in self.tags]
        info["url"] = self.url_lineEdit.text().strip()

        filepath = self.filepath_lineEdit.text()
        filename = os.path.split(filepath)[1]
        new_dir = os.path.splitext(os.path.join(self.root_path, filename))[0]
        if os.path.exists(new_dir):
            error_msg = QtWidgets.QErrorMessage()
            error_msg.showMessage("There already exists such a file")
            error_msg.exec()
            return

        os.makedirs(new_dir)
        shutil.move(filepath, new_dir)
        with open(os.path.join(new_dir, ".info"), "w") as f:
            yaml.dump(info, f)

        self.close()

    def filepath_btn_moussePressEvent(self, event):
        filepath = QtWidgets.QFileDialog.getOpenFileName()
        if filepath[0]:
            self.filepath_lineEdit.setText(filepath[0])