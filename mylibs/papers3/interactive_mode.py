class WrongCommand(Exception):
    def __init__(self, cmd: str, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.cmd = cmd

class NoCommand(Exception):
    pass


class Command:
    name = ""
    aliases = []

    def parse_args(self):
        raise NotImplementedError

    def callback(self):
        raise NotImplementedError


class ExitCommand(Command):
    name = "exit"
    aliases = ["quit"]

    def callback(self):
        exit(0)


class HelpCommand(Command):
    name = "help"

    def callback(self):
        return


class InteractiveMode:
    def __init__(self, commands) -> None:
        self.basic_commands = ["exit", "quit", ""]
        self.commands = commands

    def parse_command(self, cmd_ipt: str):
        corresp_commands = [cmd for cmd in self.commands if cmd.startswith(cmd_ipt)]
        if len(corresp_commands) == 1:
            return corresp_commands[0]
        else:
            raise WrongCommand

    def parse_input(self, ipt: str) -> dict:
        splt =  ipt.split()
        if len(splt) == 0:
            raise NoCommand
        else:
            cmd = self.parse_command(splt[0])
            if len(splt) == 1:
                return {"cmd": cmd, "args": ""}
            else:
                return {"cmd": cmd, "args": splt[1]}

    def run(self):
        while "exit not given":
            try:
                cmd = input("> ").strip()
            except NoCommand:
                continue
            except WrongCommand as err:
                print (f"Command {err.cmd} unknown, try help for available commands.")
                continue
