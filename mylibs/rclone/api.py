import subprocess
from time import time

from . import utils


REFRESH_RATE = 1


def sync(source, destination, dry_run=True):
    if dry_run:
        dry_run_arg = "--dry-run"
    else:
        dry_run_arg = ""

    with open(utils.STDOUT_FILEPATH, "w") as f_out, open(utils.STDOUT_FILEPATH, "w") as f_err:
        print ("Starting to sync...")
        process = subprocess.Popen([f"rclone", "sync", {source}, {destination}, {dry_run_arg}, "--progress"],
            stdout=f_out, stderr=f_err)

        t0 = 0
        while process.poll() is None:
            if time() - t0 > 1 / REFRESH_RATE:
                parsing = utils.parse_rclone_out("stdout")
                print (parsing["progress"])
                t0 = time()

        if process.poll() != 0:
            exit(f"There was an error during rclone execution (err {process.poll()}."\
                 f"Please check {utils.abspath('stderr')} for more information")
        else:
            if dry_run:
                log = "The following files will be modified:"
            else:
                log = "The following files has been modified"
            print (log)

            parsing = utils.parse_rclone_out("stdout")
            print (*parsing["files"], sep="\n")


def download(remote, dest):
    (added, deleted, modified) = check(remote, dest)
    diffs = ["+ " + path for path in added]
    diffs += ["- " + path for path in deleted]
    diffs += ["* " + path for path in modified]
    yield {"retcode": 1, "value": diffs}
    # print ("Starting downloading...")
    # sync(remote, dest)


def upload(source, remote):
    (added, deleted, modified) = check(source, remote)
    yield (added, deleted, modified)

    # print ("Starting uploading...")
    # sync(source, remote)


def check(source, destination):
    with open(utils.STDOUT_FILEPATH, "w") as f_out, open(utils.STDERR_FILEPATH, "w") as f_err:
        process = subprocess.Popen([f"rclone", "check", source, destination],
                stdout=f_out, stderr=f_err)
        process.wait()
    parsing = utils.parse_rclone_check_err("stderr")
    return parsing["lists"]


def list():
    subprocess.run(["rclone", "listremotes"])


def refresh_token():
    pass
