###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def download_cb(args):
    for ret in api.download(args.remote, args.destination):
        if ret["retcode"] == 1:
            for path in ret["value"]:
                if path.startswith("+"):
                    color = "green"
                elif path.startswith("-"):
                    color = "red"
                if path.startswith("*"):
                    color = "orange"
                utils.Colors.color_print(path, color)



def upload_cb(args):
    raise NotImplemented
    up = api.upload(args.source, args.remote)
    (added, deleted, modified) = next(up)
    diffs = ["+ " + path for path in added]
    diffs += ["- " + path for path in deleted]
    diffs += ["* " + path for path in modified]
    print (*diffs, sep="\n")


def list_cb(args):
    return api.list()


def refresh_token_cb(args):
    raise NotImplementedError


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Python helper for rclone sync tool")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    download_parser = subparsers.add_parser("download", help="Download remote and sync with local (dry-run by default)")
    download_parser.add_argument(dest="remote", help="Name of the remote to be synchronized")
    download_parser.add_argument(dest="destination", help="Filepath of the destination")
    download_parser.set_defaults(callback=download_cb)


    upload_parser = subparsers.add_parser("upload", help="Upload local to remote (dry-run by default)")
    upload_parser.add_argument(dest="source", help="Filepath of the source")
    upload_parser.add_argument(dest="remote", help="Name of the remote to be synchronized")
    upload_parser.set_defaults(callback=upload_cb)


    list_parser = subparsers.add_parser("list", help="List of the available remotes")
    list_parser.set_defaults(callback=list_cb)


    refresh_token_parser = subparsers.add_parser("refresh-token", help="Refresh the auth token used by a remote")
    refresh_token_parser.add_argument(dest="remote", help="Name of the remote whose token needs to be refreshed")
    refresh_token_parser.set_defaults(callback=refresh_token_cb)


    args = parser.parse_args()
    args.callback(args)
