import os
import sys


STDOUT_FILEPATH = "stdout"
STDERR_FILEPATH = "stderr"


class Colors:
    RED   = "\033[1;31m"
    BLUE  = "\033[1;34m"
    CYAN  = "\033[1;36m"
    GREEN = "\033[0;32m"
    ORANGE = "\033[0;33m"
    RESET = "\033[0;0m"
    BOLD    = "\033[;1m"
    REVERSE = "\033[;7m"

    @classmethod
    def color_print(cls, text, color, f_out=sys.stdout, end="\n"):
        f_out.write(cls.__dict__[color.upper()])
        f_out.write(text)
        f_out.write(cls.RESET)
        f_out.write(end)


def between(s, a, b):
    return s.split(a, 1)[-1].split(b, 1)[0]


def get_err_type(err_type_str):
    if err_type_str.startswith("File not in Local file system at"):
        return 0 # in drive but not in local
    elif err_type_str.startswith("File not in Encrypted drive"):
        return 1 # in local but not in drive
    elif err_type_str.startswith("Sizes differ"):
        return 2 # in both but not the same
    else:
        return -1


def parse_rclone_check_err(err_filepath, first_line=0):
    added = []
    deleted = []
    modified = []

    with open(err_filepath) as f:
        content = f.read()
    lines = content.splitlines()[first_line:]
    i = -1
    for (i, line) in enumerate(lines):
        words = line.split()
        if words[2] == "ERROR":
            path = between(line, "ERROR : ", ": ")
            err_type = get_err_type(line[line.index(path) + len(path) + 2:])
            if err_type == -1:
                import ipdb; ipdb.set_trace()
            (added, deleted, modified)[err_type].append(path)

    return {
        "lists": [added, deleted, modified],
        "last_line": i
    }


def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)
