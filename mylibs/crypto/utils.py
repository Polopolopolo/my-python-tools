from pathlib import Path
from hashlib import sha256
from Crypto.Cipher import AES
from secrets import token_bytes

def abspath(rel_path):
    dirpath = Path(__file__).parent
    return (dirpath / rel_path).as_posix()

def aes_key_from_str(key: str) -> bytes:
    return sha256(key.encode()).digest()

def gen_iv():
    return token_bytes(AES.block_size)
