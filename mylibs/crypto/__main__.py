import argparse

from . import utils
from . import api


def encrypt(args):
    b_key = utils.aes_key_from_str(args.key)
    iv = bytes.fromhex(args.iv) if args.iv else utils.gen_iv()
    print ("IV:", iv.hex())
    print("Ciphertext:", api.encrypt_api(args.message, b_key, iv).hex())

def decrypt(args):
    b_key = utils.aes_key_from_str(args.key)
    iv = bytes.fromhex(args.iv)
    print("Plaintext:", api.decrypt_api(args.ciphertext, b_key, iv).decode())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generic lib for crypto stuff")
    subparsers = parser.add_subparsers(dest="command")

    encrypt_parser = subparsers.add_parser("encrypt", help="encrypt a bitstring")
    encrypt_parser.add_argument(dest="message", help="message to be encrypted")
    encrypt_parser.add_argument(dest="key", help="key used to encrypt")
    encrypt_parser.add_argument("--iv", default="", help="initialization value")

    decrypt_parser = subparsers.add_parser("decrypt", help="decrypt a ciphertext")
    decrypt_parser.add_argument(dest="ciphertext", help="ciphertext to be decrypted (must be in hexadecimal format)")
    decrypt_parser.add_argument(dest="key", help="key used to decrypt")
    decrypt_parser.add_argument("iv", help="initialization value")

    gui_parser = subparsers.add_parser('gui', help='graphic interface (WIP)')

    args = parser.parse_args()
    match args.command:
        case 'encrypt':
            encrypt(args)
        case 'decrypt':
            decrypt(args)
        case 'gui':
            exit('This feature is not available yet')
        case default:
            parser.print_usage()
