import os

import PyQt5
from PyQt5 import QtWidgets, QtCore, QtGui


def decryption_encoding(f):
    """ Wrapper  
    Checks if the plaintext is utf-8 encoded. If not, display an error message.
    """
    def g(*args, **kwargs):
        try:
            args[0].plaintext.decode()
        except UnicodeDecodeError:
            QtWidgets.QMessageBox.critical(args[0], "Erreur", f"Mauvais format de vecteur d'initialisation")
            return
        else:
            return f(*args, **kwargs)
    return g


def iv_format(f):
    """ Wrapper  
    Checks if the iv has a valid format. If not, display an error message.
    """
    def g(*args, **kwargs):
        flag = False
        try:
            iv = bytes.fromhex(args[0].iv_in.text())
        except ValueError:
            flag = True
        else:
            if len(iv) != 16:
                flag = True
        finally:
            if flag:
                QtWidgets.QMessageBox.critical(args[0], "Erreur", f"Mauvais format de vecteur d'initialisation")
                return
            else:
                return f(*args, **kwargs)
    return g


def key_format(f):
    """ Wrapper  
    Checks if the key has a valid format. If not, display an error message.
    """
    def g(*args, **kwargs):
        if len(args[0].key) != 16:
            QtWidgets.QMessageBox.critical(args[0], "Erreur", f"Mauvais format de clef")
            return
        else:
            return f(*args, **kwargs)
    return g



def empty_field(field, fieldname):
    """ Wrapper  
    Checks if the field 'field' is empty. If yes, display an error message.
    """
    def empty_field_wrapper(f):
        def g(*args, **kwargs):
            if isinstance(getattr(args[0], field), QtWidgets.QLineEdit):
                text = getattr(args[0], field).text()
            else:
                text = getattr(args[0], field).toPlainText()
            if not text:
                QtWidgets.QMessageBox.critical(args[0], "Erreur", f"Le champ '{fieldname}' doit être rempli")
                return
            else:
                return f(*args, **kwargs)
        return g
    return empty_field_wrapper


def not_a_file(field):
    """ Wrapper  
    Checks if the file whose path is contained in 'field' is indeed a file. If it is not a file: display an error message.
    """
    def not_a_file_wrapper(f):
        def g(*args, **kwargs):
            if isinstance(getattr(args[0], field), QtWidgets.QLineEdit):
                text = getattr(args[0], field).text()
            else:
                text = getattr(args[0], field).toPlainText()
            if not os.path.isfile(text):
                QtWidgets.QMessageBox.critical(args[0], "Erreur", "Le fichier de clef que vous avez choisi n'est pas un fichier")
                return
            else:
                return f(*args, **kwargs)
        return g
    return not_a_file_wrapper


def file_access(field, mode):
    """ Wrapper  
    Checks if the file whose path is contained in 'field' is accessible (depends on the 'mode'). If not: display an error message.
    """
    def file_access_wrapper(f):
        def g(*args, **kwargs):
            if isinstance(getattr(args[0], field), QtWidgets.QLineEdit):
                text = getattr(args[0], field).text()
            else:
                text = getattr(args[0], field).toPlainText()
            if not os.access(text, mode):
                QtWidgets.QMessageBox.critical(args[0], "Erreur", "Le fichier de clef que vous avez choisi ne peut pas être lu")
                return
            else:
                return f(*args, **kwargs)
        return g
    return file_access_wrapper