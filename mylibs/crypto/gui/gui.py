import os

import PyQt5
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.uic import loadUi

from .wrappers import *

from ..api import encrypt_api, decrypt_api
from ..utils import gen_iv, abspath


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi(abspath("gui/main.ui"), self)
        self.setWindowTitle("Cryptool")

        self.encrypt_btn.clicked.connect(self.encrypt_callback)
        self.decrypt_btn.clicked.connect(self.decrypt_callback)


    @empty_field("key_ipt", "fichier de clef")
    @not_a_file("key_ipt")
    @file_access("key_ipt", os.R_OK)
    def _get_key_encrypt(self):
        with open(self.key_ipt.text(), "rb") as f:
            self.key = f.read()
            return True


    @key_format
    @empty_field("plaintext_in", "texte à chiffrer")
    def _encrypt(self):
        iv = gen_iv()
        ciphertext = encrypt_api(self.plaintext_in.toPlainText(), self.key, iv)
        self.iv_out.setText(iv.hex())
        self.ciphertext_out.setText(ciphertext.hex())


    def encrypt_callback(self):
        if self._get_key_encrypt():
            self._encrypt()


    @empty_field("key_ipt_3", "fichier de clef")
    @not_a_file("key_ipt_3")
    @file_access("key_ipt_3", os.R_OK)
    def _get_key_decrypt(self):
        with open(self.key_ipt_3.text(), "rb") as f:
            self.key = f.read()
            return True


    @empty_field("iv_in", "vecteur d'initialisation")
    @empty_field("ciphertext_in", "texte à déchiffrer")
    @key_format
    @iv_format
    def _decrypt(self):
        iv = bytes.fromhex(self.iv_in.text())
        self.plaintext = decrypt_api(self.ciphertext_in.toPlainText(), self.key, iv)

    @decryption_encoding
    def _display_plaintext(self):
        self.plaintext_out.setText(self.plaintext.decode())


    def decrypt_callback(self):
        if self._get_key_decrypt():
            self._decrypt()
            self._display_plaintext()


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    main_window = MainWindow()
    main_window.showMaximized()

    app.exec_()
