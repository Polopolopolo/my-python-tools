from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad


def encrypt_api(msg: str, key: bytes, iv: bytes) -> bytes:
    aes = AES.new(key, mode=AES.MODE_CBC, IV=iv)
    b_msg = msg.encode()
    padded_msg = pad(b_msg, AES.block_size)
    return aes.encrypt(padded_msg)


def decrypt_api(ciphertext: str, key: bytes, iv: bytes) -> bytes:
    aes = AES.new(key, mode=AES.MODE_CBC, IV=iv)
    b_ciphertext = bytes.fromhex(ciphertext)
    msg = aes.decrypt(b_ciphertext)
    return unpad(msg, AES.block_size)
