import netsquid as ns


class Qubit:
    @classmethod
    def zero(cls):
        return ns.qubits.create_qubits(1)[0]

    @classmethod
    def one(cls):
        qb = cls.zero()
        ns.qubits.operate(qb, ns.X)
        return qb

    @classmethod
    def plus(cls):
        qb = cls.zero()
        ns.qubits.operate(qb, ns.H)
        return qb

    @classmethod
    def minus(cls):
        qb = cls.one()
        ns.qubits.operate(qb, ns.H)
        return qb
