###########
# IMPORTS #
###########
import argparse

import netsquid as ns

from . import utils
from . import api


#############
# CALLBACKS #
#############



########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="None")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")



    args = parser.parse_args()
    args.callback(args)
