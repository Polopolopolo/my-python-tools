###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def list_cb(args):
    pass


def get_cb(args):
    pass


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Password manager")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    list_parser = subparsers.add_parser("list", help="Display the list of all the websites/app for which we have a password")
    list_parser.set_defaults(callback=list_cb)


    get_parser = subparsers.add_parser("get", help="Display a password given the name of the website/app and the root password")
    get_parser.add_argument(dest="name", help="name of the website/app")
    get_parser.add_argument(dest="root", help="Root password")
    get_parser.set_defaults(callback=get_cb)


    args = parser.parse_args()
    args.callback(args)
