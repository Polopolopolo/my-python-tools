###########
# IMPORTS #
###########

import argparse
from base64 import b64encode
import configparser
from getpass import getpass
from hashlib import sha256
import os


###########
# GLOBALS #
###########

DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/password/"
NAMES_FILEPATH = os.path.join(DIRPATH, "passwd_names.cfg")
DEFAULT_LENGTH = 8


#######
# API #
#######

def list_api():
    config = configparser.ConfigParser()
    config.read(NAMES_FILEPATH)
    passwds = [config[section] for section in config.sections()
                if section.startswith("PASSWORD_") and section[9:].isdecimal()]
    return passwds


def get_api(passwd_name, root):
    passwds = list_api()
    psw = [psw for psw in passwds if psw["name"] == passwd_name]
    if not psw:
        return
    else:
        if not root:
            root = getpass("Root Password:")
        length = psw[0]["length"] if "length" in psw else DEFAULT_LENGTH
        return sha256((root + psw[0]["name"]).encode()).hexdigest()[:length]


#############
# CALLBACKS #
#############

def list_cb(args):
    passwds = list_api()
    for passwd in passwds:
        print (passwd["name"], passwd["description"], end="\n\n")


def get_cb(args):
    psw = get_api(args.name, args.root)
    if not psw:
        print (f"No password named {passwd_name} in the list, aborting...")
    else:
        print (psw)


########
# MAIN #
########

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Password manager")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    list_parser = subparsers.add_parser("list", help="Lists all the password's names")
    list_parser.set_defaults(callback=list_cb)

    get_parser = subparsers.add_parser("get", help="Get a password from its name and the root password")
    get_parser.add_argument("name", help="Name of the password")
    get_parser.add_argument("--root", help="Root password")
    get_parser.set_defaults(callback=get_cb)

    args = parser.parse_args()
    args.callback(args)
