from hashlib import sha256


class Password:
    def __init__(self, name, length, options):
        """ Password description  
        @name:str: name of the password's service  
        @length:int: length of the password  
        @options:List[str]: options of the password; available options are 'numbers', 'lowercase', 'uppercase', 'specials'; default is ['numbers', 'lowercase']
        """
        self.name = name
        self.length = length
        if not options:
            self.options = options
        else:
            self.options = ["numbers", "lowercase"]

    def password(self, root_passwd):
        """ Output the password  
        @root_passwd:str: root password that generates all children passwords
        """
        h = sha256((root_passwd + self.name).encode()).digest()
