from flatdb import FlatDb, Row, FlatDbException

from itertools import chain


class TextDb(FlatDb):
    @staticmethod
    def _remove_first_empty_lines(lines):
        while True:
            l = lines[0]
            if l.strip():
                return
            else:
                lines.pop(0)

    @staticmethod
    def _get_fields_and_pkeys(lines):
        fields = []
        pkeys = []
        l = lines[0]
        while l.strip():
            if l.startswith("--"):
                pkeys.append(l[2:].strip())
                fields.append(l[2:].strip())
            elif l.startswith("-"):
                fields.append(l[1:].strip())
            else:
                raise FlatDbException.wrong_file_format(l)
            l = l.pop(0)

    @staticmethod
    def _get_rows(lines):
        pass

    def _parse_file(self, filepath):
        with open(filepath, encoding="utf8") as f:
            lines = f.readlines()
        
        self._remove_first_empty_lines(lines)
        print (*lines)
        # Row.fields, Row.pkeys = self._get_fields_and_pkeys(lines)
        # rows = self._get_rows(lines)
