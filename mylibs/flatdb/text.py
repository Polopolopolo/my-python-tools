from .flatdb import FlatDb


class TextDb(FlatDb):
    def _parse_file(self, filepath):
        with open(filepath, encoding="utf8") as f:
            lines = f.readlines()

        # dont take the first empty lines into account
        for l in lines:
            if l.strip():
                break

        # get the pkeys
        
    def pkeys(self):
        with open(self.filepath, encoding="utf8") as f:
            lines = f.readlines()

        pkeys = []
        while True:
            l = lines[0].strip()
            if l.startswith("-"):
                pkeys.append(l[1:])
            
