from flatdb import FlatDb


class MarkdownDb(FlatDb):
    def _get_metadata(self, lines):
        metadata = {}
        i = 0
        while i < len(lines):
            if lines[i].startswith("---"):
                i += 1
                while not lines[i].startswith("---"):
                    k, v = lines[i].split(":", 1)
                    metadata[k] = v
                    i += 1
            i += 1
        return metadata


    def _parse_file(self, filepath):
        with open(filepath, encoding="utf8") as f:
            content = f.read()
        lines = content.split("\n")
        metadata = self._get_metadata(lines)
        print (metadata)
