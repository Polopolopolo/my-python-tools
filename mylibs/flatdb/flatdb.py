class FlatDbException(Exception):
    @classmethod
    def pkey_missing(cls, *missing_pkeys):
        return cls("Missing primary key(s):", ", ".join(missing_pkeys))

    @classmethod
    def wrong_row_format(cls):
        return cls("Wrong row format")

    @classmethod
    def duplicate(cls, *pkeys):
        return cls("Duplicate primary keys:", ",".join(pkeys))

    @classmethod
    def wrong_fields(cls, *fields):
        return cls("Wrong fields:", ",".join(fields))


class Row(dict):
    """ Class for a database row

    :param fields: List of the fields of the database  
    :type fields: list  
    :param pkeys: List of the primary keys of the database  
    :type pkeys: list
    """
    fields = []
    pkeys = []

    def __init__(self, **keyvalues):
        """ Constructor method

        :param keyvalues: Dict of the fields/values of the row  
        :type keyvalues: dict
        """
        super().__init__(**keyvalues)

    def __getitem__(self, key):
        """ Overwritting of __getitem__: returns '' if the key is in the database fields but not in the Row

        :param key: name of the fields  
        :key type: str  
        """
        if key not in self.keys() and key in self.fields:
            return ""
        else:
            return super().__getitem__(key)

    def pkey(self):
        """ Returns the key/values of the primary key(s) of this row
        """
        return {k: v for (k, v) self.keyvalues.items() if k in self.pkeys}


class RowList(dict):
    """ Class for a list of Rows, typically the entire database
    """
    def __init__(self, rows):
        """ Constructor method

        :param rows: List of the rows to add in the list  
        :type rows: list  
        """
        super().__init__({row.pkey().values(): row for row in rows})

    def __iter__(self):
        """ Overwritting of __iter__: returns an iteration over the rows
        """
        return iter(self.values())


class FlatDb:
    """ Abstract class for flat db

    :param filepath: Location of the flatdb file  
    :type filepath: str  
    :param rows: List of the rows of the database  
    :type rows: RowList  
    :param length: Number of rows in the database  
    :type length: int  
    """

    def __init__(self, filepath):
        """ Constructor method

        :param filepath: Location of the flatdb file  
        :type filepath: str  
        """
        self.filepath = filepath
        self.rows = self._parse_file(filepath)
        self.length = len(self.rows)

    def _parse_file(filepath):
        """ Abstract method, returns all the database's rows from the db file

        :param filepath: The path of the database file
        :type filepath: str
        :return: The RowList of all the rows in the database
        :rtype: RowList
        """
        pass

    def fields(self):
        """ Returns all the fields of the database
        
        :return: List of the fieldnames  
        :rtype: list  
        """
        return set().union(row.keys() for row in self.select())

    def pkeys(self):
        """ Returns the list of all primary keys of the database

        :return: List of the primary keys  
        :rtype: list
        """
        pass


    def __len__(self):
        """ Returns the number of rows in the database

        :return: Number of rows in the database  
        :rtype: int 
        """
        return self.length

    def __getitem__(self, *pkeys):
        """ Returns the row matching pkeys

        :return: The row matching pkeys if it exists, else None  
        :rtype: Row
        """

    def select(self, *fields):
        """ Returns a generator yielding all the rows in the database
        (with the fields restricted by the fields param)

        :param fields: List of the fields' names to include in the output
        (if fields is empty, then all the fields will be included)  
        :type fields: list
        """
        pass

    def insert(self,**keyvalues):
        row = Row(**keyvalues)
        # check that all the primary keys are filled
        if row.pkeys != self.pkeys():
            raise FlatDbException.wrong_row_format()
        missing_pkeys = [pk for pk in self.pkeys() if not row[pk]]
        if missing_pkeys:
            raise FlatDbException.pkey_missing(missing_pkeys)
        # check that it is not a duplicate
        row_pkeys = [row[pk] for pk in self.pkeys()]
        if self[*row_pkeys]:
            raise FlatDbException.duplicate(row_pkeys)
