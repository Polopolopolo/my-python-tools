import json

from flatdb import FlatDb


class JsonDb(FlatDb):
    def _parse_file(self, filepath):
        with open(filepath, encoding="utf8") as f:
            dct = json.load(f.read())

        if "__pkeys__" not in dct or "__fields__" not in dct:
            pass
        else:
            pkeys = 
