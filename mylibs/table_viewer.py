import argparse
import os
import sqlite3


def get_connection(dbpath):
    if not os.path.isfile(dbpath):
        exit(f"Error : {dbpath} is not a valid path")
    return sqlite3.connect(dbpath)


def get_headers(conn, table):
    return [r[0] for r in conn.cursor().execute("SELECT name FROM "
                                f"pragma_table_info('{table}')")]


def get_rows(conn, table, headers, cols, filters, verbose=True):
    # check if table is in the database
    tables = [r[0] for r in 
            conn.cursor().execute("SELECT name FROM sqlite_master")]
    if table not in tables:
        conn.close()
        exit(f"Error : the table {table} does not exist, "
            f"available tables are {', '.join(tables)}")

    # check if the columns are in the table
    if cols != "*":
        if not all(c in headers for c in cols):
            conn.close()
            exit(f"Error : one of the columns {', '.join(cols)} does not exist"
                f", available columns are {' '.join(headers)}")
        cols = ", ".join(cols)


    filters = "WHERE " + " ".join(filters) if filters else ""

    try:
        if verbose:
            print (f"SELECT {cols} FROM {table} {filters}")
        yield from conn.cursor().execute(f"SELECT {cols} FROM {table} {filters}")
        conn.close()
    except Exception as err:
        conn.close()
        print("Error : an issue happened during the SQL query, it probably "
            "comes from the filters.")
        print (err.__class__.__name__)
        exit (err)


def pretty_string_rows(headers, rows):
    # convert in string
    headers = [str(h) for h in headers]
    rows = [[str(x) for x in r] for r in rows]

    # get max widths
    widths = [max(len(h), max(len(r[i]) for r in rows))
             for (i, h) in enumerate(headers)]
    lines = ["| " + " | ".join(h.center(widths[i]) 
            for (i, h) in enumerate(headers)) + " |"]
    lines += ["| " + " | ".join("-" * widths[i] 
            for (i, h) in enumerate(headers)) + " |"]
    lines += ["| " + " | ".join(x.center(widths[i]) for (i, x) in enumerate(r)) + " |" 
            for r in rows]
    return "\n" + "\n".join(lines)


def callback(args):
    conn = get_connection(args.dbpath)
    headers = get_headers(conn, args.table)
    rows = get_rows(conn, args.table, headers, args.columns, args.filters)
    args.columns = headers if args.columns == "*" else args.columns
    print (pretty_string_rows(args.columns, rows))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Display a nice representation"
                                                " of data from a database.")
    parser.add_argument("dbpath", type=str, help="Path to the database")
    parser.add_argument("table", type=str, help="Name of the table to display")
    parser.add_argument("-c", "--columns", type=str, nargs="+", default="*", 
                        help="Columns to display")
    parser.add_argument("-f", "--filters", type=str, nargs="+",
                        help="Filters for the data to display")

    args = parser.parse_args()
    callback(args)