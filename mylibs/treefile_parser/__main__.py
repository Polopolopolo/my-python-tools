###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############



########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parse a tree-structured file")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")



    args = parser.parse_args()
    args.callback(args)
