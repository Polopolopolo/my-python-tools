class BlockTree:
    def __init__(self, content):
        self.content = content
        self.children = []

    @staticmethod
    def parse(content, open_char, close_char):
        if open_char not in content:
            return
        lvl = -1
        block = ""
        for c in content:
            if lvl >= 0:
                block += c
            if c == open_char:
                lvl += 1
            if c == close_char:
                lvl -= 1
                assert lvl >= -1
                if lvl == -1:
                    return block[1:-1]


# class Separator:
#     def __init__(self, _open, _close):
#         self.open = _open
#         self.close = _close


# class Separators(list):
#     def __init__(self, *seps):
#         super().__init__(seps)

#     def open_seps(self):
#         return [x.open for x in self]

#     def close_seps(self):
#         return [x.close for x in self]

#     def open(self, sep):
#         for s in self:
#             if sep == s.close:
#                 return sep.open

#     def close(self, sep):
#         for s in self:
#             if sep == s.open:
#                 return sep.close


# class Tree:
#     def __init__(self, open_char="", close_char=""):
#         self.open_char = open_char
#         self.close_char = close_char
#         self.children = []



# def build_tree(s: str, sep: Separators):
#     root = Tree()
#     for (i, c) in enumerate(s):
#         if c in sep.open_seps():

