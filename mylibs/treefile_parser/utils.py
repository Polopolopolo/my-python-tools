import os


# DIRPATH = "$dirpath"


def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)
