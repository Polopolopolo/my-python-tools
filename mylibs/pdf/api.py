import os
from math import log10
from PIL import Image
from PyPDF2 import PdfReader, PdfWriter, PdfFileMerger
import pdf2image


def convert_api(pdf_filepath, output_path, ext):
    img_pages = pdf2image.convert_from_path(pdf_filepath)

    # define all output paths
    prefix = os.path.splitext(pdf_filepath)[0]
    len_idx = int(log10(len(img_pages)))
    output_paths = [f"{prefix}_{i:0{len_idx}}.{ext}" for i in range(len(img_pages))]

    # check if all output paths are available
    for path in output_paths:
        if os.path.exists(path):
            raise Exception(f"There already exists a file {path}")

    # if they are: save pages
    for i in range(len(img_pages)):
        img_pages[i].save(output_paths[i])


def rotate_api(input_filepath, output_path, rotation_level, pages="all"):
    """ Rotate the given pdf  
    `rotation_level` can be 1, 2 or 3 and it is a clockwise rotation  
    `pages` is a list of pages that to be rotated or all for all the pages
    """
    reader = PdfReader(input_filepath)
    writer = PdfWriter()

    # rotate the pages
    for (i, page) in enumerate(reader.pages):
        if pages == "all" or i in pages:
            page = page.rotate(90 * rotation_level)
        writer.add_page(page)

    # check if the path is available
    if os.path.exists(output_path):
            raise Exception(f"There already exists a file {output_path}")

    # if yes: save the pdf
    with open(output_path, "wb") as f:
        writer.write(f)

