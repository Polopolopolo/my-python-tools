###########
# IMPORTS #
###########
import argparse

from . import utils
from . import api


#############
# CALLBACKS #
#############
def convert_cb(args):
    api.convert_api(args.file, args.o)


def rotate_cb(args):
    api.rotate_api(args.input, args.output, args.rot_lvl, args.pages)


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Toolbox for pdf manipulation")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    convert_parser = subparsers.add_parser("convert", help="Convert the given file into a pdf file")
    convert_parser.add_argument(dest="file", help="File to be converted")
    convert_parser.add_argument("-o", default="", help="Output file")
    convert_parser.set_defaults(callback=convert_cb)

    rotate_parser = subparsers.add_parser("rotate", help="Rotate pages of a given pdf file")
    rotate_parser.add_argument(dest="input", help="Path of the pdf file")
    rotate_parser.add_argument(dest="output", help="Output file")
    rotate_parser.add_argument(dest="rot_lvl", type=int, help="Level of rotation: 1 for 90°, 2 for 180°, 3 for 270° (clockwize)")
    rotate_parser.add_argument(dest="pages", type=int, nargs="*", default="all", help="Files to be rotated")
    rotate_parser.set_defaults(callback=rotate_cb)


    args = parser.parse_args()
    args.callback(args)
