import copy
from itertools import product
from functools import reduce
from math import cos, pi, sin, sqrt
import numpy as np
from numpy import eye, kron, matrix, trace, transpose
from sage import all as sage


def QState(values):
    return sage.Matrix(values, ring=sage.CC)

zero    = QState([1, 0]).T
one     = QState([0, 1]).T
plus    = QState([1, 1]).T / sqrt(2)
minus   = QState([1, -1]).T / sqrt(2)

EPR     = QState([1, 0, 0, 1]).T / sqrt(2)

H       = QState([[1, 1],
                  [1, -1]]) / sqrt(2)

Bell = [
        QState([1, 0, 0, 1]).T / sqrt(2),
        QState([1, 0, 0, -1]).T / sqrt(2),
        QState([0, 1, 1, 0]).T / sqrt(2),
        QState([0, 1, -1, 0]).T / sqrt(2)
    ]


def density(state):
    if state.dimensions()[1] != 1:
        return state
    else:
        return state * state.H


def tensor(*states):
    ret = states[0].tensor_product(states[1])
    for i in range(len(states) - 2):
        ret = ret.tensor_product(states[i + 2])
    return ret


# def probas_measurement(state, *bases):
#     # turn all states into density matrices
#     rho = density(state)
#     bases = copy.deepcopy(bases)
#     for (i, b) in enumerate(bases):
#         for (j, x) in enumerate(b):
#             bases[i][j] = density(x)

#     # compute the dict keys
#     ranges = [range(len(b)) for b in bases]
#     keys = product(*ranges)

#     # compute the probas
#     measures = (reduce(kron, prod) for prod in product(*bases))
#     probas = [trace(M * rho) for M in measures]

#     return dict(zip(keys, probas))


# def post_measurement_states(state, *bases):
#     # turn all states into density matrices
#     rho = density(state)
#     bases = copy.copy(bases)
#     for (i, b) in enumerate(bases):
#         for (j, x) in enumerate(b):
#             bases[i][j] = density(x)

#     # compute the dict keys
#     ranges = [range(len(b)) for b in bases]
#     keys = product(*ranges)

#     # compute the probas
#     measures = (reduce(kron, prod) for prod in product(*bases))
#     post_measurement = [(M * rho * M.H) / trace(M * rho) for M in measures if round(trace(M * rho), 2) != 0]

#     return dict(zip(keys, post_measurement))


# def fidelity(a, b):
#     a, b = density(a), density(b)
#     return trace(np.sqrt(np.sqrt(a) * b * np.sqrt(a))) ** 2



