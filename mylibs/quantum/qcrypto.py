import copy
from itertools import product
from functools import reduce
from math import cos, pi, sin, sqrt
import numpy as np
from numpy import array, eye, kron, ndarray, trace, transpose


class DensityState:
    def __init__(self, matrix, repr=''):
        self.a = '|0 X 0|'
        self.repr = repr
        arr_matrix = array(matrix)
        if len(arr_matrix.shape) == 1:
            arr_matrix = array([arr_matrix]).T
        if arr_matrix.shape[1] == 1:
            arr_matrix = arr_matrix @ arr_matrix.conj().T
        self.array = array(arr_matrix)

    def __repr__(self) -> str:
        if self.repr:
            return self.repr
        else:
            return getattr(self, 'array').__repr__()

    def __getattr__(self, name):
        return getattr(self.array, name)

    def __add__(self, other):
        return DensityState(self.array.__add__(other.array), self.repr + other.repr)

    def __mul__(self, other):
        return DensityState(self.array.__mul__(other.array), self.repr + other.repr)

    def __matmul__(self, other):
        return DensityState(self.array.__matmul__(other.array), self.repr + other.repr)

    def __sub__(self, other):
        return DensityState(self.array.__sub__(other.array), self.repr + other.repr)


def II(n): return DensityState(eye(2**n), f'II{n}')

ZERO    = DensityState(array([1, 0]).T, '|0 X 0|')
ONE     = DensityState(array([0, 1]).T, '|1 X 1|')
PLUS    = DensityState(array([1, 1]).T / sqrt(2), '|+ X +|')
MINUS   = DensityState(array([1, -1]).T / sqrt(2), '|- X -|')

EPR     = DensityState(transpose(array([1, 0, 0, 1])) / sqrt(2), '|EPR X EPR|')

H = DensityState(array([[1, 1],
            [1, -1]]) / sqrt(2), 'H')

BELL = [
        DensityState(transpose(array([1, 0, 0, 1])) / sqrt(2), '|EPR X EPR|'),
        DensityState(transpose(array([1, 0, 0, -1])) / sqrt(2), 'BELL 2'),
        DensityState(transpose(array([0, 1, 1, 0])) / sqrt(2), 'BELL 3'),
        DensityState(transpose(array([0, 1, -1, 0])) / sqrt(2), 'BELL 4')
    ]

def R(angle):
    return DensityState([
        array([cos(angle), sin(angle)]).T,
        array([-sin(angle), cos(angle)]).T
    ], f'R({angle})')

def tensor(*states, repr=''):
    matrix = reduce(kron, states)
    return DensityState(matrix, repr)

def BB84(value, basis=None):
    if ('+' in value or '-' in value):
        if basis:
            raise Exception('Set a basis or use +/-, but not both')
        qubits = [ZERO if xi == '0'
                  else ONE if xi == '1'
                  else PLUS if xi == '+'
                  else MINUS
                  for xi in value]
        repr = value
    else:
        qubits = [ZERO if (xi, ti) == ('0', '0')
                  else ONE if (xi, ti) == ('1', '0')
                  else PLUS if (xi, ti) == ('0', '1')
                  else MINUS
                  for xi, ti in zip(value, basis)]
        repr = ''.join(xi if ti == '0'
                       else '+' if (xi, ti) == ('0', '1')
                       else '-'
                       for (xi, ti) in zip(value, basis))
    return tensor(*qubits, repr=repr)

def proj_measurement(state, povm):
    dct = {}
    for M in povm:
        proba = trace(M * state)
        dct[M.repr] = proba
    return dct

def fidelity(a, b):
    return trace(np.sqrt(np.sqrt(a) * b * np.sqrt(a))) ** 2


if __name__ == "__main__":
    rho = BB84('-+-0')
    print (rho)
    M0 = BB84('0000', '0100')
    mz = proj_measurement(rho, [M0, II(4) - M0])
