"""
This module provides some libraries that I made.  
Below is the list of these libraries:

Type `mylibs <libname>` to use this library.
"""

import subprocess
import sys


LIBS = [
    (False, "readhex",         "display the hexadecimal content of a file"),
    (False, "search",          "search files"),
    (False, "secret_transfer", "encrypt a file"),
    (False, "sql",             "[not finished yet]"),
    (False, "table_viewer",    "display the content of a database's table"),
    (False, "pc1_to_pc2",      "transfer a file from PC1 to PC2"),
    (False, "papers",          "phd papers info [deprecated, use papers3 instead]"),
    (False, "markdown",        "pretty rendering of markdown local files"),
    (False, "quantum",		   "quantum computation [use qcrypto only]"),
    (False, "tabletex",        "generate latex code from a csv file"),
    (False, "password",        "password manager"),
    (True, "libs",            "manage libraries"),
    (False, "papers2",         "phd papers info [deprecated, use papers3 instead]"),
    (False, "latex",           "build a tex file"),
    (False, "papers3",         "phd papers info, provides a GUI"),
    (False, "pdf",             "toolbox for pdf manipulation"),
    (False, "xournal",         "toolbox for xournal files manipulation"),
    (False, "pprint",          "pretty print files"),
    (True,  "crypto",          "library for crypto tools"),
    (False, "pyqt",            "library for pyqt tools"),
    (False, "rclone",          "library for simpler rclone management"),
    (False, "sympa",           "library for managing lip6's sympa list"),
    (False, "papers4",         "phd papers info, provides a TUI"),
    (False, "latex-ref",       "manage references for latex projects"),
    (False, 'regex',           'Find and replace expression in files using regex'),
]


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] in (libname for (_, libname, _) in LIBS):
        subprocess.run(["python3", "-m", f"mylibs.{sys.argv[1]}", *sys.argv[2:]])
    else:
        print (__doc__)
        max_libname = max(len(libname) for (_, libname, _) in LIBS)
        for (ready, libname, descr) in sorted(LIBS, key=lambda x: x[1]):
            suffix = f' {libname.ljust(max_libname)}: {descr}'
            print ('(x)' + suffix if ready else '( )' + suffix)
        print()
