from copy import deepcopy
from math import ceil
import os
import shutil
from typing import List, Union

from .utils import abspath, config
from .xournal_doc import XournalDocument, Page, Layer, Stroke, Image, Text


def frame(page: Page, margins: List[float]) -> Page:
    ''' Return a page with the same elements as `page`, whose dimensioned to fit perfectly these elements
    Arguments:
        page:           The page to shrink
        margins:        Margin to add to the top, bottom, left, right.
    '''
    # create a blank new page with the new dimensions
    new_page = page.blank_copy()
    new_page.xml.attrib['width'] = str(page.right - page.left + sum(margins[2:]))
    new_page.xml.attrib['height'] = str(page.bot - page.top + sum(margins[:2]))
    # insert each element on this page, at its new position
    for el in page.elements:
        el = deepcopy(el)
        if isinstance(el, Stroke):
            points = [(x - page.left + margins[2], y - page.top + margins[0]) for x, y in el.points]
            el.xml.text = ' '.join(f'{x} {y}' for x, y in points)
        elif isinstance(el, Image):
            el.xml.attrib['left'] = str(float(el.xml.attrib['left']) - page.left + margins[2])
            el.xml.attrib['right'] = str(float(el.xml.attrib['right']) - page.left + margins[2])
            el.xml.attrib['top'] = str(float(el.xml.attrib['top']) - page.top + margins[1])
            el.xml.attrib['bottom'] = str(float(el.xml.attrib['bottom']) - page.top + margins[1])
        elif isinstance(el, Text):
            el.xml.attrib['x'] = str(float(el.xml.attrib['x']) - page.left + margins[2])
            el.xml.attrib['top'] = str(float(el.xml.attrib['y']) - page.top + margins[1])
        new_page.layers[el.layer.index].append(el)
    return new_page

def highlighted_stroke(stroke: Stroke, color='#ffff007f', width='8.5', capStyle='round'):
    hg = deepcopy(stroke)
    hg.set('tool', 'highlighter')
    hg.set('color', color)
    hg.set('width', width)
    hg.set('capStyle', capStyle)
    return hg

def inside_shape(doc: XournalDocument, shape: Stroke):
    return [s for s in doc.strokes
            if all(shape.left < pt[0] < shape.right
                   and shape.top < pt[1] < shape.bot
                   for pt in s.points)
            ]

def is_rectangle(stroke: Stroke):
    if len(stroke.points) != 5:
        return False
    else:
        A, B, C, D, E = stroke.points
        return  A == E and\
                A[0] == D[0] and\
                A[1] == B[1] and\
                B[0] == C[0] and\
                C[1] == D[1]

