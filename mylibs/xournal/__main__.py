import argparse
import os

from . import utils
from .utils import config
from . import api
from .xournal_doc import XournalDocument


def vsplit(args):
    ...

def _pink_split(filepath, out):
    api.pink_split(filepath, out)

def pink_split_cb(args):
    if not args.out:
        name, ext = os.path.splitext(args.filepath)
        out = f"{name}.split{ext}"
    else:
        out = args.out
    _pink_split(args.filepath, out)

def frame(args):
    doc = XournalDocument(args.filepath)
    idx_pages = [int(i) for i in args.pages] if args.pages != '*' else list(range(len(doc.pages)))
    margins = [float(arg) if arg.replace('.', '').isdecimal()
               else float(config['frame-margins'][i]) if arg == 'c'
               else args
               for i, arg in enumerate(args.margins)]
    for i in idx_pages:
        page = doc.pages[i]
        page_margins = [page.top, page.bot - page.top, page.left, page.right - page.left]
        new_page = api.frame(page, [page_margins[i] if m == '_' else m for i, m in enumerate(margins)])
        doc.xmlroot.remove(doc.pages[i].xml)
        doc.insert(new_page, i)
    out = args.filepath if not args.out else args.out
    doc.write(out)

def gui(args):
    from .gui.utils import main
    main()

def test(args):
    doc = XournalDocument(utils.abspath('tests/test-font.xopp'))
    for text in doc.texts:
        text.xml.attrib['font'] = 'DejaVu Math TeX Gyre'
        print (f'text: {text.text}')
        l, t, b, r = text.left, text.top, text.bot, text.right
        print (text.textbbox(text.text, text.size, text.font))
        text.layer.new_stroke([(l, t), (r, t), (r, b), (l, b), (l, t)])
    doc.write(utils.abspath('tests/test-font-square.xopp'))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Xournal companion')
    subparsers = parser.add_subparsers(dest='command')

    vsplit_parser = subparsers.add_parser('vsplit', help='split a page at some (colored) lines')
    vsplit_parser.add_argument(dest='filepath', help='path of the file')
    vsplit_parser.add_argument('--out', help='path of the split file (default: `{input file}.split`)')

    frame_parser = subparsers.add_parser('frame', help='frame a document so that there is no longer blank margins round the figures')
    frame_parser.add_argument('filepath', help='path of the xournal document')
    frame_parser.add_argument('--out', default='', help='path of the output file (default: overwrite file')
    frame_parser.add_argument('--pages', default='*', help='pages to include, starting by 0 (default: all)')
    frame_parser.add_argument('--margins', nargs=4, default=['c', 'c', 'c', 'c'], help='(top, bottom, left, right) margins to use (four arguments expected). Write "_" to use the original page\'s margin; "c" to use the margin written in the configuration file; or an integer to use it as the margin\'s value')

    gui_parser = subparsers.add_parser('gui')

    test_parser = subparsers.add_parser('test')

    args = parser.parse_args()
    match args.command:
        case 'vsplit':
            ...
        case 'frame':
            frame(args)
        case 'gui':
            gui(args)
        case 'test':
            test(args)
        case default:
            parser.print_usage()
