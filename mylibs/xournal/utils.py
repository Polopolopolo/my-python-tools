from functools import wraps
import gzip
from inspect import signature
from pathlib import Path, PosixPath
import os
from xml.etree import ElementTree
try:
    import tomllib
except ImportError:
    import toml as tomllib


ELEMENTS = [
    "stroke",
]


def abspath(rel_path: str) -> PosixPath:
    return Path(__file__).parent / rel_path

config = tomllib.loads(abspath('config.toml').read_text())


def find_pink_lines(tree):
    lines = []
    pink = "#ff00ffff"
    pages = tree.findall("page")
    for (i, page) in enumerate(pages):
        width = float(page.attrib["width"])
        layers = page.findall("layer")
        for layer in layers:
            assert all(el.tag in ELEMENTS for el in layer.findall("*"))
            strokes = layer.findall("stroke")
            for stroke in strokes:
                if stroke.attrib["color"] == pink:
                    pts = stroke_to_xy(stroke.text)
                    if len(pts) == 2\
                            and pts[0][1] == pts[1][1]\
                            and abs(pts[1][0] - pts[0][0]) >= .9 * width:
                        lines.append((i, pts[0][1]))
    return lines


class Color:
    Black           = "000000"
    Green           = "008000"
    LightBlue       = "00c0ff"
    LightGreen      = "00ff00"
    Blue            = "3333cc"
    Gray            = "808080"
    Red             = "ff0000"
    Magenta         = "ff00ff"
    Orange          = "ff8000"
    Yellow          = "ffff00"
    White           = "ffffff"
    LightMagenta    = "ffc8ff"
    LightGreen      = "64c864"
    LightLightBlue  = "96e1fa"

    @classmethod
    def get_name(cls, code):
        if code in cls.__dict__.values():
            return [k for k, v in cls.__dict__.keys() if v == code]
        else:
            return "Unknown"

    @classmethod
    def get_code(cls, name):
        if name in cls.__dict__:
            return getattr(cls, name)
        else:
            return "Unknown"



if __name__ == "__main__":
    tree = xournal_to_tree("test.xopp")
    lines = find_pink_lines(tree)
    print(lines)
