import gzip
from itertools import pairwise
from typing import List, Tuple, Union
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from matplotlib import font_manager
from PIL import Image as PILImage, ImageDraw, ImageFont

from .utils import config


class Text:
    ''' Object representing a text of a layer
    Attributes and properties:
        xml         Xml tree of this text
        layer       Layer to which this text belongs
        index       Stroke's index in the layer
        fontname    Name of this text's font
        font        This text's font
        color       Color of this text
        size        Font size of this text
        text        Content of the text
        left        Left coordinate of this text
        right       Right coordinate of this text
        top         Top coordinate of this text
        bot         Bottom coordinate of this text
        width       Width of this text
        height      Height of this text
    '''
    def __init__(self, layer, im_xml):
        self.xml   = im_xml
        self.layer = layer
        self._font = None

    def __repr__(self) -> str:
        return f'<Text {self.layer.page.index}-{self.layer.index}-{self.index} {self.text}>'

    def __eq__(self, o):
        if isinstance(o, Text) and o.xml == self.xml and o.layer == self.layer:
            return True
        else:
            return False

    @property
    def font(self):
        if self._font: return self._font
        for font in font_manager.findSystemFonts():
            try:
                name = font_manager.FontProperties(fname=font).get_name()
            except RuntimeError:
                continue
            else:
                if name == self.fontname:
                    return font
        else:
            exit(f'The font {self.fontname} cannot be resolved. Aborting.')

    @staticmethod
    def textbbox(text: str, fontsize: float, font: str):
        ''' Return the bounding box [left, top, right, bottom] of a text in a font
        '''
        im_font = ImageFont.truetype(font, size=fontsize)
        pixel_bbox = ImageDraw.ImageDraw(PILImage.new('RGB', (1, 1))).textbbox((0, 0), text, font=im_font)
        return [round(4/3 * x, 2) for x in pixel_bbox]

    @property
    def index(self): return self.layer.texts.index(self)
    @property
    def fontname(self): return self.xml.get('font')
    @property
    def color(self): return self.xml.get('color')
    @property
    def size(self): return float(self.xml.get('size'))
    @property
    def text(self): return self.xml.text
    @property
    def left(self): return float(self.xml.get('x'))
    @property
    def right(self): return self.left + self.textbbox(self.text, self.size, self.font)[2]
    @property
    def top(self): return float(self.xml.get('y'))
    @property
    def bot(self): return self.top + self.textbbox(self.text, self.size, self.font)[3]
    @property
    def width(self): return self.right - self.left
    @property
    def height(self): return self.bot - self.top


class Image:
    ''' Object representing an image of a layer
    Attributes and properties:
        xml         Xml tree of this image
        layer       Layer to which this image belongs
        index       Stroke's index in the layer
        left        Left coordinate of this image
        right       Right coordinate of this image
        top         Top coordinate of this image
        bot         Bottom coordinate of this image
        width       Width of this image
        height      Height of this image
    '''
    def __init__(self, layer, im_xml):
        self.xml   = im_xml
        self.layer = layer

    def __repr__(self) -> str:
        return f'<Image {self.layer.page.index}-{self.layer.index}-{self.index}>'

    def __eq__(self, o):
        if isinstance(o, Image) and o.xml == self.xml and o.layer == self.layer:
            return True
        else:
            return False

    @property
    def index(self): return self.layer.images.index(self)
    @property
    def left(self): return float(self.xml.get("left"))
    @property
    def right(self): return float(self.xml.get("right"))
    @property
    def top(self): return float(self.xml.get("top"))
    @property
    def bot(self): return float(self.xml.get("bottom"))
    @property
    def width(self): return self.right - self.left
    @property
    def height(self): return self.bot - self.top


class Stroke:
    ''' Object representing a stroke of a layer
    Attributes and properties:
        xml         Xml tree of this stroke
        layer       Layer to which this stroke belongs
        color       Color used for the stroke
        tool        Tool used for the stroke
        index       Stroke's index in the layer
        left        Left coordinate of this stroke
        right       Right coordinate of this stroke
        top         Top coordinate of this stroke
        bot         Bottom coordinate of this stroke
        width       Width of this stroke
        height      Height of this stroke
        points      Points of the stroke as (x, y) pairs
    '''
    def __init__(self, layer, stroke_xml):
        self.xml   = stroke_xml
        self.layer = layer

    def __repr__(self) -> str:
        return f'<Stroke {self.layer.page.index}-{self.layer.index}-{self.index}>'

    def __eq__(self, o):
        if isinstance(o, Stroke) and o.xml == self.xml and o.layer == self.layer:
            return True
        else:
            return False

    @property
    def index(self): return self.layer.strokes.index(self)
    @property
    def color(self): return self.xml.get('color')
    @property
    def tool(self): return self.xml.get('tool')
    @property
    def left(self): return min(self.points)[0]
    @property
    def right(self): return max(self.points)[0]
    @property
    def top(self): return min(self.points, key=lambda pt: pt[1])[1]
    @property
    def bot(self): return max(self.points, key=lambda pt: pt[1])[1]
    @property
    def width(self): return self.right - self.left
    @property
    def height(self): return self.bot - self.top
    @property
    def points(self):
        points = self.xml.text.split()
        points = [float(pt) for pt in points]
        points = [(points[2*i], points[2*i+1]) for i in range(len(points)//2)]
        return points

    def shape(self) -> str:
        ''' Return the shape of this stroke
        Possible outputs are 'line', 'square', or '' if no shape was found
        '''
        if len(self.points) == 2:
            return 'line'
        elif len(self.points) == 5 and\
            all(x1 == x2 if i % 2 == 0 else y1 == y2
                for i, ((x1, y1), (x2, y2)) in enumerate(pairwise(self.points))) :
            return 'square'
        else:
            return ''


class Layer:
    ''' Object representing a layer of a page of a xournal document
    Attributes and properties:
        xml         Xml tree of this page
        page        Document to which this layer belongs
        index       Layer's index in the page
        strokes     List of strokes of this layer
        images      List of images of this layer
        texts       List of texts of this layer
        elements    List of elements (strokes, images, and texts) of this layer
        left        Left coordinate of the leftmost element of this layer
        right       Right coordinate of the rightmost element of this layer
        top         Top coordinate of the highest element of this layer
        bot         Bottom coordinate of the lowest element of this layer
    '''
    def __init__(self, page, layer_xml):
        self.xml    = layer_xml
        self.page   = page

    def __repr__(self) -> str:
        return f'<Layer {self.page.index}-{self.index}>'

    def __eq__(self, o):
        if isinstance(o, Layer) and o.xml == self.xml and o.page == self.page:
            return True
        else:
            return False

    @property
    def index(self): return self.page.layers.index(self)
    @property
    def strokes(self): return [Stroke(self, s) for s in self.xml.findall('stroke')]
    @property
    def images(self): return [Image(self, im) for im in self.xml.findall('image')]
    @property
    def texts(self): return [Text(self, txt) for txt in self.xml.findall('text')]
    @property
    def elements(self): return self.strokes + self.images + self.texts
    @property
    def left(self): return min((s.left for s in self.elements))
    @property
    def right(self): return max((s.right for s in self.elements))
    @property
    def top(self): return min((s.top for s in self.elements))
    @property
    def bot(self): return max((s.bot for s in self.elements))

    def append(self, element):
        ''' Append an element to the layer
        '''
        self.xml.append(element.xml)

    def blank_copy(self):
        ''' Return a new layer with the same attributes, but without any elements
        '''
        cp = Element('layer', self.xml.attrib)
        return Layer(None, cp)

    def new_stroke(self, points, source=None, **attrib):
        ''' Add a new stroke to the layer
        See `add_new_element` function for more information
        '''
        points = ' '.join(' '.join(str(x) for x in pt) for pt in points)
        add_new_element(self.page.doc, self, 'stroke', source, points)

    def new_image(self):
        raise NotImplementedError

    def new_text(self):
        raise NotImplementedError


class Page:
    ''' Object representing a page of a xournal document
    Attributes and properties:
        xml         Xml tree of this page
        doc         Document to which this page belongs
        index       Page's index in the whole document
        width       Page's width
        height      Page's height
        background  Background layer of this page
        layers      List of layers of this page
        strokes     List of strokes of this page
        images      List of images of this page
        texts       List of texts of this page
        elements    List of elements (strokes, images, and texts) of this page
        left        Left coordinate of the leftmost element of this page
        right       Right coordinate of the rightmost element of this page
        top         Top coordinate of the highest element of this page
        bot         Bottom coordinate of the lowest element of this page
    '''
    def __init__(self, doc, page_xml):
        self.xml    = page_xml
        self.doc    = doc

    def __repr__(self) -> str:
        return f'<Page {self.index}>'

    def __eq__(self, o):
        if isinstance(o, Page) and o.xml == self.xml and o.doc == self.doc:
            return True
        else:
            return False

    @property
    def index(self): return self.doc.pages.index(self)
    @property
    def width(self): return float(self.xml.attrib['width'])
    @property
    def height(self): return float(self.xml.attrib['height'])
    @property
    def background(self): return self.xml.find("background")
    @property
    def layers(self): return [Layer(self, l) for l in self.xml.findall("layer")]
    @property
    def strokes(self): return sum([l.strokes for l in self.layers], [])
    @property
    def images(self): return sum([l.images for l in self.layers], [])
    @property
    def texts(self): return sum([l.texts for l in self.layers], [])
    @property
    def elements(self): return self.strokes + self.images + self.texts
    @property
    def left(self): return min((s.left for s in self.elements))
    @property
    def right(self): return max((s.right for s in self.elements))
    @property
    def top(self): return min((s.top for s in self.elements))
    @property
    def bot(self): return max((s.bot for s in self.elements))

    def append(self, layer):
        ''' Append a layer to this page
        '''
        self.xml.append(layer.xml)

    def insert(self, layer, index):
        ''' Insert a layer to this page
        '''
        self.xml.insert(index, layer.xml)

    def blank_copy(self):
        ''' Return a new page with the same attributes, but without any elements
        '''
        new = Element("page", self.xml.attrib)
        new.append(self.background)
        for l in self.layers:
            new.append(Element("layer", l.xml.attrib))
        new_page = Page(None, new)
        return new_page


class XournalDocument:
    ''' Object representing Xournal document
    Attributes and properties:
        filepath    Path of the xournal document
        xml         Xml tree representation of the xournal document
        xmlroot     Root of the xml tree
        pages       Pages of the xml document
        layers      Layers of the xml document
        strokes     List of strokes of this document
        images      List of images of this document
        texts       List of texts of this document
        elements    List of elements (strokes, images, and texts) of this document
    '''
    def __init__(self, filepath):
        self.filepath = filepath
        # open document, decompress it, and xml-parse it
        with gzip.open(filepath) as f:
            self.xml = ElementTree.parse(f)
        self.xmlroot = self.xml.getroot()

    @property
    def pages(self): return [Page(self, p) for p in self.xmlroot.findall("page")]
    @property
    def layers(self): return sum((p.layers for p in self.pages), [])
    @property
    def strokes(self): return sum((page.strokes for page in self.pages), [])
    @property
    def images(self): return sum((page.images for page in self.pages), [])
    @property
    def texts(self): return sum((page.texts for page in self.pages), [])
    @property
    def elements(self): return self.strokes + self.images + self.texts

    def append(self, page):
        ''' Append a page to this document
        '''
        self.xmlroot.append(page.xml)

    def insert(self, page, index):
        ''' Insert a page to this document
        '''
        self.xmlroot.insert(index, page.xml)

    def write(self, filepath):
        ''' Write this object as a xournal document in `filepath`
        '''
        with gzip.open(filepath, "wb") as f:
            self.xml.write(f)

def add_new_element(
        doc: XournalDocument,
        target: Union[XournalDocument, Page, Layer],
        tag: str,
        source:Union[Page, Layer, Stroke, Image, Text]=None,
        text:str='',
        **attrib):
    ''' Create a new element with tag `tag` and text `text`, and add it to `target`
    If `source` is not None: copy its attributes
    Elif `attrib` is not empty: use `attrib`'s (key, value) pairs as attributes
    Else: get the first element of the same type in `doc`, and copy its attributes; if there is no such element, raise Exception
    '''
    if source:
        attrib = source.attrib
    elif not attrib:
        try:
            attrib = doc.__getattribute__(tag + 's')[0].xml.attrib
        except IndexError:
            raise Exception('No element to copy.')
    element = Element(tag)
    element.text = text
    element.attrib = attrib
    if isinstance(target, XournalDocument):
        xml = target.xmlroot
    else:
        xml = target.xml
    xml.append(element)
