from base64 import b64decode
from itertools import pairwise
from pathlib import Path

from PIL import Image as PILImage, ImageDraw, ImageFont

from ..xournal_doc import Stroke, Text, Image, Layer, Page, XournalDocument


def render_stroke(stroke: Stroke):
    im = PILImage.new('RGBA', (int(stroke.width) + 1, int(stroke.height) + 1), (0, 0, 0, 0))
    draw = ImageDraw.Draw(im)
    points = [(x - stroke.left, y - stroke.top) for x, y in stroke.points]
    for pair in pairwise(points):
        draw.line(pair, fill=stroke.color)
    return im

def render_image(im: Image):
    Path('/tmp/xopp_im_render.png').write_bytes(b64decode(im.xml.text))
    return PILImage.open('/tmp/xopp_im_render.png')

def render_text(txt: Text):
    im = PILImage.new('RGBA', (int(txt.width) + 1, int(txt.height) + 1), (0, 0, 0, 0))
    draw = ImageDraw.Draw(im)
    draw.text((0, 0), txt.text, font=ImageFont.truetype(txt.font, txt.size), fill=txt.color)
    return im

def render_layer(layer: Layer):
    im = PILImage.new('RGBA', (int(layer.page.width) + 1, int(layer.page.height) + 1), (255, 255, 255, 255))
    print ('\nrendering strokes', end=' ')
    for i, s in enumerate(layer.strokes):
        print ('|', end='', flush=True)
        im2 = render_stroke(s)
        im.paste(im2, (int(s.left), int(s.top)), im2)
    print ('\nrendering texts', end=' ')
    for i, txt in enumerate(layer.texts):
        print ('|', end='', flush=True)
        im2 = render_text(txt)
        im.paste(im2, (int(txt.left), int(txt.top)), im2)
    print ('\nrendering images', end=' ')
    for i, img in enumerate(layer.images):
        print ('|', end='', flush=True)
        im2 = render_image(img)
        im.paste(im2, (int(img.left), int(img.top)), im2)
    print()
    return im


def main():
    doc = XournalDocument('xournal/gui/test.xopp')
    render_layer(doc.layers[1]).show()
