from pathlib import Path
from shutil import copy

from .utils import abspath


def new(name: str, path: str):
    lib_path = Path(path) / name
    lib_path.mkdir()
    copy(abspath('default_utils'), str(lib_path / 'utils.py'))
    copy(abspath('default_api'), str(lib_path / 'api.py'))
    copy(abspath('default_main'), str(lib_path / '__main__.py'))
    return lib_path
