import argparse

from . import api


def new(args):
    lib_path = api.new(args.name, args.path)
    print (f'Library directory written at {lib_path}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manage libraries")
    subparsers = parser.add_subparsers(dest="command")

    build_parser = subparsers.add_parser("new", help="build a library with a given name at a given path")
    build_parser.add_argument("name", help="name of the library")
    build_parser.add_argument("path", help="path to write of the library")

    args = parser.parse_args()
    match args.command:
        case 'new':
            new(args)
        case default:
            parser.print_usage()
