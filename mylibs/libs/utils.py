from pathlib import Path


def abspath(rel_path: str):
    dirpath = Path(__file__).parent
    return (dirpath / rel_path).as_posix()