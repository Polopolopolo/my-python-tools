import os
import subprocess


# DIRPATH = "$dirpath"


def abspath(rel_path):
    dirpath, _ = os.path.split(__file__)
    return os.path.join(dirpath, rel_path)


def mkdir(dirpath):
    if os.path.exists(dirpath):
        subprocess.run(["gio", "trash", dirpath])
    os.mkdir(dirpath)
