from .utils import abspath, mkdir

import os
import shutil
import subprocess
from zipfile import ZipFile


TMP_DIR = abspath("html_tmp")


def open(filename, browser):
    name = os.path.splitext(filename)[0]
    html_name = name + ".html"
    html_path = os.path.join(TMP_DIR, html_name)
    if not os.path.isfile(filename):
        return 1
    else:
        mkdir(TMP_DIR)
        with ZipFile(filename) as myzip:
            myzip.extractall(path=TMP_DIR)
        if not os.path.isfile(html_path):
            return 2
        else:
            process = subprocess.Popen([browser, html_path])


def zip(filename, aux):
    name = os.path.splitext(filename)[0]
    zhtml_name = name + ".zhtml"
    mkdir(TMP_DIR)
    shutil.move(filename, TMP_DIR)
    shutil.move(aux, TMP_DIR)
    shutil.make_archive(zhtml_name, "zip", TMP_DIR)
    shutil.move(zhtml_name + ".zip", zhtml_name)
