###########
# IMPORTS #
###########
import argparse
import os

from . import utils
from . import api


DEFAULT_BROWSER = "brave-browser"


#############
# CALLBACKS #
#############
def open_cb(args):
    retcode = api.open(args.filename, args.browser)
    if retcode:
        exit(f"Error (code {retcode})")
    else:
        exit("Done")


def zip_cb(args):
    if not args.aux:
        name = os.path.splitext(args.filename)[0]
        args.aux = name + "_files"
    api.zip(args.filename, args.aux)
    exit("Done")


########
# MAIN #
########
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Handles zhtml files")
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    open_parser = subparsers.add_parser("open", help="open a zhtml file")
    open_parser.add_argument(dest="filename", help="Name of the zhtml file to be opened")
    open_parser.add_argument("--browser", default=DEFAULT_BROWSER, help=f"Browser to be used to open the html file (default = {DEFAULT_BROWSER}")
    open_parser.set_defaults(callback=open_cb)

    zip_parser = subparsers.add_parser("zip", help="create a zhtml file")
    zip_parser.add_argument(dest="filename", help="Name of the html file")
    zip_parser.add_argument("--aux", help="path of the dir containing css/js/png... files to be used in the main html file (default is '<html filename w/o extension>_files'")
    zip_parser.set_defaults(callback=zip_cb)

    args = parser.parse_args()
    args.callback(args)
