import json
import os
import subprocess
import sqlite3


DIRPATH = "/home/paul/espaces/prog/python/mylibs/mylibs/papers2"


def abspath(rel_path):
    return os.path.join(DIRPATH, rel_path)


def print_color(text, color_code, **kwargs):
    print (f"\033[{color_code}m{text}\033[0m", **kwargs)


def db_fetchall(conn=None, table="Papers"):
    if not conn:
        conn = sqlite3.connect(abspath("papers.db"))
        conn.row_factory = sqlite3.Row
    entries = conn.execute(f"SELECT * FROM {table}").fetchall()
    return entries


def prompt_dict(filepath, keys, **kwargs):
    # write in empty dct
    with open(filepath, "w") as f:
        dct = {k: "" for k in keys}
        for (k, v) in kwargs.items():
            dct[k] = v
        json.dump(dct, f, indent=2)

    # open editor
    proc = subprocess.run(["nano", abspath("prompt.json")])

    if proc.returncode != 0:
        print ("Error during process execution, aborting...")
    else:
        with open(filepath) as f:
            dct = json.load(f)
        return dct


def search_paper_info(keyword):
    from scholarly import scholarly
    try:
        resp = scholarly.search_single_pub(keyword)
    except IndexError:
        return 1, None
    else:
        return 0, resp
