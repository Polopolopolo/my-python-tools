###########
# IMPORTS #
###########
import argparse
import json
import sqlite3
import subprocess

from . import utils
from .api import *


#############
# CALLBACKS #
#############
def ls_cb(conn, args):
    pdf_files, papers = ls_api(conn)
    for f in pdf_files:
        utils.print_color(pdf_files[f], 93, end=" ")
        if f in papers:
            print (papers[f])
        else:
            print ("???")


def list_cb(conn, args):
    papers = list_api(conn)
    for (i, pap) in enumerate(sorted(papers, key=lambda row: row["Title"])):
        print (i, pap["Title"])


def add_cb(conn, args):
    keys = [k[0].upper() + k[1:] for k in DB_SCHEME["papers"]]
    dct = args.dict

    # prompt
    if not dct:
        dct = utils.prompt_dict(utils.abspath("prompt.json"), keys, Filepath=args.path)

    # fill every missing fields of dct
    else:
        dct = {key: dct[key] if key in dct else "" for key in keys}

    retcode = add_api(conn, dct)
    if retcode == 1:
        print ("Error : Title and (Filepath or Url) are needed : aborting...")
    elif retcode == 2:
        print("Error : there is not only one paper matching these conditions, aborting...")
    else:
        print ("Done")


def rm_cb(conn, args):
    retcode = rm_api(conn, args.id)
    if retcode == 1:
        print (f"No paper for id {args.id}, aborting...")
    elif retcode != 0:
        print (f"Error {retcode}")
    else:
        print ("Done")


def mv_cb(conn, args):
    retcode = mv_api(conn, args.id, args.dirpath)
    if retcode == 1:
        print (f"Error : no paper for id {args.id}, aborting...")
    elif retcode == 2:
        print("Error : there is not only one paper matching these conditions, aborting...")
    elif retcode == 3:
        print ("Error when trying to move the file, aborting...")
    else:
        print ("Done")


def paper_cb(conn, args):
    if args.field.lower() not in DB_SCHEME["papers"]:
            print (f"Error : invalid field name : {args.field}, aborting...")
    else:
        retcode, pap = paper_api(conn, args.id)
        if retcode == 1:
            print (f"Error : no paper for id {args.id}")
        elif retcode == 2:
            print (f"Multiple papers match {args.id}:")
            for (i, p) in pap:
                print (i, p["Title"])
        else:
            print (pap[args.field])


def check_cb(conn, args):
    errors = check_api(conn)
    if not errors:
        print ("All good")
    else:
        print ("Wrong path for these papers:")
        print ("\n\n".join(f"{i}: {paper['Title']}\n({paper['Filepath']})" for (i, paper) in errors))


def fetch_cb(conn, args):
    retcode, dct = fetch_api(conn, args.keyword)
    if retcode == 0:
        keys = [k[0].upper() + k[1:] for k in DB_SCHEME["papers"]]
        dct = utils.prompt_dict(utils.abspath("prompt.json"), keys, **dct, Filepath=args.path)

        retcode = add_api(conn, dct)
        if retcode == 1:
            print ("Error : Title and (Filepath or Url) are needed : aborting...")
        elif retcode == 2:
            print("Error : there is no paper matching these conditions, aborting...")
        else:
            print ("Done")


def search_cb(conn, args):
    retcode, papers = search_api(conn, args.field, args.value)
    if retcode == 0:
        for pap in papers:
            print (pap[0], pap[1])
    elif retcode == 1:
        print ("Error : field not in the database")
    else:
        print ("Unexpected error")


def bibtex_cb(conn, args):
    retcode, bibtex = bibtex_api(conn, args.keyword)
    if retcode == 0:
        print (bibtex)
    elif retcode == 1:
        print("Error : there is no paper matching these conditions, aborting...")
        



########
# MAIN #
########
if __name__ == "__main__":
    conn = sqlite3.connect(utils.abspath("papers.db"))
    conn.row_factory = sqlite3.Row
    
    parser = argparse.ArgumentParser(description=None)
    parser.set_defaults(callback=lambda *args: parser.print_usage())
    subparsers = parser.add_subparsers(dest="command")

    ls_parser = subparsers.add_parser("ls", help="List the papers in the current directory")
    ls_parser.set_defaults(callback=ls_cb)


    list_parser = subparsers.add_parser("list", help="List the papers in the database")
    list_parser.set_defaults(callback=list_cb)


    add_parser = subparsers.add_parser("add", help="Add a paper in the database")
    add_parser.add_argument("path", default="", help="Filepath of the paper")
    add_parser.add_argument("--dict", help="key-value map with paper information")
    add_parser.set_defaults(callback=add_cb)


    rm_parser = subparsers.add_parser("rm", help="Remove a paper from the database")
    rm_parser.add_argument("id", help="Id of the paper (can be obtained from 'list' command")
    rm_parser.set_defaults(callback=rm_cb)


    mv_parser = subparsers.add_parser("mv", help="Move a to a new location and apply the changes to the database")
    mv_parser.add_argument("id", help="Id of the paper (can be obtained from 'list' command)")
    mv_parser.add_argument("dirpath", help="Path of the directory where to move the file")
    mv_parser.set_defaults(callback=mv_cb)


    paper_parser = subparsers.add_parser("paper", help="Get a paper's field from its index or title")
    paper_parser.add_argument("id", help="")
    paper_parser.add_argument("field", help="")
    paper_parser.set_defaults(callback=paper_cb)


    check_parser = subparsers.add_parser("check", help="Check wheither all the papers' path are correct or not")
    check_parser.set_defaults(callback=check_cb)

    fetch_parser = subparsers.add_parser("fetch", help="Add a paper from a keyword (e.g. its title or DOI")
    fetch_parser.add_argument("keyword")
    fetch_parser.add_argument("path", default="", help="Filepath of the paper")
    fetch_parser.set_defaults(callback=fetch_cb)

    search_parser = subparsers.add_parser("search", help="Search for papers in db matching filters")
    search_parser.add_argument("field")
    search_parser.add_argument("value")
    search_parser.set_defaults(callback=search_cb)

    bibtex_parser = subparsers.add_parser("bibtex", help="Display the bibtex of a paper")
    bibtex_parser.add_argument("keyword")
    bibtex_parser.set_defaults(callback=bibtex_cb)

    args = parser.parse_args()
    args.callback(conn, args)

    conn.close()
