import os
import sqlite3
import subprocess

from .utils import db_fetchall, search_paper_info


DB_SCHEME = {
    "papers": [
        "title",
        "filepath",
        "url",
        "authors",
        "tags",
        "notes",
    ]
}


def ls_api(conn):
    papers = db_fetchall(conn)
    papers = {pap["Filepath"] : pap["Title"] for pap in papers}
    pdf_files = {os.path.abspath(f): f for f in os.listdir(".") if f.endswith(".pdf")}
    return pdf_files, papers


def list_api(conn):
    papers = db_fetchall(conn)
    return papers


def add_api(conn, dct, **kwargs):
    if not dct["Title"] or not (dct["Filepath"] or dct["Url"]):
        return 1

    else:
        keys = [k[0].upper() + k[1:] for k in DB_SCHEME["papers"]]
        query_str = f"""INSERT INTO Papers ({','.join(keys)})
VALUES (?, ?, ?, ?, ?, ?);"""
        dct["Filepath"] = os.path.abspath(dct["Filepath"])
        values = [kwargs[k] if k in kwargs else dct[k] for k in keys]
        conn.execute(query_str, [dct[k] for k in keys])
        conn.commit()


def rm_api(conn, paper_id):
    retcode, pap = paper_api(conn, paper_id)
    if retcode != 0:
        return retcode
    else:
        if len(conn.execute("SELECT * FROM Papers WHERE Title = ? AND Filepath = ?",
                (pap["Title"], pap["Filepath"])).fetchall()) != 1:
            return 2
        else:
            conn.execute("DELETE FROM Papers WHERE Title = ? AND Filepath = ?",
                    (pap["Title"], pap["Filepath"]))
            conn.commit()
            return 0

def mv_api(conn, paper_id, dirpath):
    retcode, pap = paper_api(conn, paper_id)
    if retcode != 0:
        return retcode
    else:
        if len(conn.execute("SELECT * FROM Papers WHERE Title = ? AND Filepath = ?",
                (pap["Title"], pap["Filepath"])).fetchall()) != 1:
            return 2
        else:
            dirpath = os.path.abspath(dirpath)
            new_path = os.path.join(dirpath, os.path.basename(pap["Filepath"]))

            proc = subprocess.run(["mv", pap["Filepath"], dirpath])
            if proc.returncode != 0:
                return 3
            else:
                conn.execute("UPDATE Papers SET Filepath = ? WHERE Title = ? AND Filepath = ?",
                        (new_path, pap["Title"], pap["Filepath"]))
                conn.commit()


def paper_api(conn, paper_id):
    paper = None
    papers = db_fetchall(conn)
    papers = sorted(papers, key=lambda row: row["Title"])
    if paper_id.isnumeric():
        if int(paper_id) < len(papers):
            return 0, papers[int(paper_id)]
        else:
            return 1, []
    else:
        matching_papers = [ (i, pap) for (i, pap) in enumerate(papers)
                            if paper_id.lower() in pap["Title"].lower() ]
        if len(matching_papers) == 1:
            return 0, matching_papers[0][1]
        elif len(matching_papers) > 1:
            return 2, matching_papers
        else:
            return 1, []


def check_api(conn):
    papers = sorted(db_fetchall(conn), key=lambda row: row["Title"])
    errors = [ (i, paper) for (i, paper) in enumerate(papers) if not os.path.isfile(paper["Filepath"]) ]
    return errors


def fetch_api(conn, keyword):
    retcode, paper = search_paper_info(keyword)
    if retcode == 0:
        return 0, {"Title": resp.bib["title"], "Authors": ", ".join(resp.bib["author"]), "Url": resp.bib["url"]}
    elif retcode == 1:
        return 1, {}


def search_api(conn, field, value):
    if field not in DB_SCHEME["papers"]:
        return 1, []
    else:
        papers = db_fetchall(conn)
        papers = sorted(papers, key=lambda row: row["Title"])
        papers = [(i, pap["Title"]) for (i, pap) in enumerate(papers) if pap[field] and value.lower() in pap[field].lower()]
        return 0, papers


def bibtex_api(conn, keyword):
    retcode, paper = search_paper_info(keyword)
    if retcode == 0:
        return 0, paper.bibtex
    elif retcode == 1:
        return 1, {}
