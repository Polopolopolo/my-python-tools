import sqlite3

import PyQt5
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QListWidget, QMainWindow,\
                            QPushButton, QDesktopWidget

from .api import list_api
from . import utils



class PapersList(QListWidget):
    def __init__(self, conn):
        super().__init__()
        self.conn = conn
        self.papers = list_api(conn)
        self.papers.sort(key=lambda pap: pap["Title"])
        for pap in self.papers:
            self.addItem(pap["Title"])


class Ressouces(QListWidget):
    def __init__(self):
        pass



class MainWidget(QWidget):
    def __init__(self, conn):
        super().__init__()
        self.setLayout(QHBoxLayout())
        self.papers_list = PapersList(conn)
        self.layout().addWidget(self.papers_list)
        btn = QPushButton("push me")
        btn.clicked.connect(lambda: print (self.papers_list.currentItem().text()))
        self.layout().addWidget(btn)


if __name__ == "__main__":
    app = QApplication([])

    conn = sqlite3.connect(utils.abspath("papers.db"))
    conn.row_factory = sqlite3.Row

    main_window = QMainWindow()
    main_window.resize(QDesktopWidget().availableGeometry().size())
    main_widget = MainWidget(conn)
    main_window.setCentralWidget(main_widget)
    main_window.show()

    conn.close()

    app.exec_()

