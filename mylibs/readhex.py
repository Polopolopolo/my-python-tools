import argparse
from hashlib import sha256
import os


def callback(args):
    if not os.path.isfile(args.path):
        exit(f"Erreur: le chemin {args.path} n'est pas valide !")
    else:
        with open(args.path, "rb") as f:
            content = f.read()
            if args.hash:
                print (sha256(content).hexdigest())
            else:
                print (content.hex())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pretty print a byte-formated file")
    parser.add_argument("path", type=str, help="Path of the file")
    parser.add_argument("--hash", action="store_true", help="If written, only output the sha256 hash of the file")
    parser.set_defaults(func=callback)
    args = parser.parse_args()
    args.func(args)
