import regex

from myutils.iterable import split_indices


COLORS = ['BLACK', 'RED', 'GREEN', 'ORANGE', 'BLUE', 'PURPLE', 'CYAN', 'WHITE']
EFFECTS = ['', 'BOLD', 'DIM', '', 'UNDERLINE', 'BLINK', '', 'REVERSE', 'HIDE', 'CROSSED']
RESET = '\033[0m'

def _color_code(name: str, bright=True):
    return 30 + 60 * bright + COLORS.index(name.upper())

def _effect_code(name: str):
    return EFFECTS.index(name.upper())

def _code2term(ccode, ecode=0):
    return f'\033[{ecode};{ccode}m'

def _infer_color(s: str):
    words = s.upper().split()
    bright = not 'DARK' in words
    ccode = _color_code(next(w for w in words if w in COLORS), bright) if any(w in COLORS for w in words) else 39
    ecode = _effect_code(next(w for w in words if w in EFFECTS)) if any(w in EFFECTS for w in words) else 0
    return ccode, ecode

def cformat(text: str, color: str, cfilter=''):
    ccode, ecode = _infer_color(color)
    if not cfilter:
        return _code2term(ccode, ecode) + text + RESET
    else:
        indices = sum((m.span() for m in regex.finditer(cfilter, text)), tuple())
        ftext = ''
        for i, s in enumerate(split_indices(text, indices)):
            if i % 2 == 0:
                ftext += s
            else:
                ftext += cformat(s, color)
        return ftext

def cprint(text: str, color: str, cfilter='', end='\n'):
    print(cformat(text, color, cfilter), end=end)

def pcolorformat(*args, **kwargs):
    raise Exception('Deprecated', 'Use `cformat` instead')

def pcolor(*args, **kwargs):
    raise Exception('Deprecated', 'Use `cprint` instead')

def pcolor_match(*args, **kwargs):
    raise Exception('Deprecated', 'Use `cprint` instead')

def print_help():
    doc = ['# Available colors']
    for color in COLORS:
        doc.append(cformat(color, color) + '\t\t' + cformat('dark ' + color, color + ' dark'))
    doc.append('\n# Available effects')
    for effect in EFFECTS:
        if effect:
            doc.append(cformat(effect, effect))
    print ('\n'.join(doc))

