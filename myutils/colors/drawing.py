BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)


def color(colorname: str) -> tuple:
    return globals()[colorname.upper()]

def shade(clr: tuple, proportion: float) -> tuple:
    return tuple(round(c * proportion) for c in clr)

def _distance(clr1: tuple, clr2: tuple) -> int:
    return sum(abs(c1 - c2) for (c1, c2) in zip(clr1, clr2))

def color_from_shade(shd: tuple) -> tuple:
    min_distance = {'name': '', 'prop': 0, 'distance': _distance(BLACK, WHITE)}
    for clrname in 'black white red green blue'.split():
        for prop in range(1, 101):
            prop = prop / 100
            if (d := _distance(shd, shade(color(clrname), prop))) < min_distance['distance']:
                min_distance = {'name': clrname, 'prop': prop, 'distance': d}
    return min_distance


if __name__ == "__main__":
    from PIL import Image
    im = Image.open('painting.png')
    new_im = [[('', 0) for j in range(im.height)] for i in range(im.width)]
    mapping = {}
    for i in range(im.width):
        print (i)
        for j in range(im.height):
            px = im.getpixel((i, j))
            if px not in mapping:
                min_dist = color_from_shade(px)
                mapping[px] = min_dist['clrname'], min_dist['prop']
            new_im[i][j] = (mapping[px])
    ...
