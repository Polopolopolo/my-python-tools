from typing import List


def ascii_table(data: List[List[str]], headers: List[str]=None):
    ''' Pretty print a table of data  
    `data`:     a list of columns representing the data  
    `header`:   a list of columns' headers (optional)
    '''
    # sanity check
    if headers and len(data) != len(headers):
        raise Exception('Format error', 'headers do not fit the data')
    if not all(len(col) == len(data[0]) for col in data):
        raise Exception('Format error', 'not all columns have the same length')
    # handle headers
    if headers:
        for col, header in zip(data, headers):
            col.insert(0, header)
    # real work
    widths = [max(len(x) for x in col) for col in data]
    for i in range(data[0]):
        line = ' '.join(col[i].ljust(widths[j] + 1) for j, col in enumerate(data))
        print (line)
