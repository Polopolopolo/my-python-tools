''' Provides a Downloader object to download files from the internet.
'''
from pathlib import Path
import re
from typing import Tuple

from selenium import webdriver


class FilePresentException(Exception):
    pass


class Downloader:
    LABEL_MAX_LEN = 16
    def __init__(self, url: str, filename: str, label: str='', regex: bool=False) -> None:
        '''
        Arguments:
        url      -- url of the file to download
        filename -- name of the downloaded file (can be a regex if the `regex` argument is `True`)
        label    -- name of the task (default ''); if not empty string, print the progression; length must be <= 16
        regex    -- whether or not considering filename as a regex (default False)
        '''
        self.url = url
        self.filename = filename
        self.label = label
        self.regex = regex
        self.dl_name = ''
        self.started = False
        self._finished = False

    def filename_present(self) -> Tuple[bool, str]:
        ''' Check whether self.filename is present in the working directory, and return True or False accordingly.
        If filename is a regex, also return the match.
        '''
        curr_dir = [str(f) for f in Path('.').iterdir()]
        if self.regex and any(re.search(self.filename, f) for f in curr_dir):
            return True, next(re.search(self.filename, f) for f in curr_dir).group()
        elif not self.regex and self.filename in curr_dir:
            return True, ''
        else:
            return False, ''

    def start_dl(self, driver: webdriver.Chrome) -> None:
        ''' Start the download.
        Raise `FilePresentException` if `self.filename` is already present in the working directory.
        '''
        if self.filename_present()[0]:
            raise FilePresentException(f'File {self.filename} already present in the directory.')
        else:
            if self.label:
                print (f'[{self.label}]'.ljust(self.LABEL_MAX_LEN) + 'Starting download.', flush=True)
            driver.get(self.url)
            self.started = True

    def finished(self):
        ''' Checks whether the download is finished or not.
        '''
        if self._finished:
            return True
        elif self.started:
            ret, mat = self.filename_present()
            if ret:
                if self.regex:
                    self.dl_name = mat
                else:
                    self.dl_name = self.filename
                self._finished = True
                if self.label:
                    print (f'[{self.label}]'.ljust(self.LABEL_MAX_LEN) + 'Download finished.', flush=True)
                return True
            else:
                return False
        else:
            return False

    def move(self, path):
        ''' Move the downloaded file to another location.
        '''
        if self.finished():
            return Path(self.dl_name).rename(path)
