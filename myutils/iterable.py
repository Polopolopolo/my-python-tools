from typing import Iterable, List


def split_indices(lst: Iterable, indices: List[int]) -> List:
    ''' Split `lst` at every index in `indices`.  
    e.g. `split_indices('abracadabra', [0, 3, 5, 7, 10]) = ['', 'abr', 'ac', 'ad', 'abr', 'a']`
    '''
    splits = []
    last = 0
    for idx in indices:
        splits.append(lst[last:idx])
        last = idx
    return splits + [lst[last:]]

def split(lst: Iterable, expr: str) -> List:
    ''' Split `lst` at every occurence of `expr`
    '''
    splits = [[]]
    for i, x in enumerate(lst):
        if x != expr:
            splits[-1].append(x)
        elif i < len(lst) - 1:
            splits.append([])
    return splits

def even(lst: Iterable):
    return [lst[i] for i in range(len(lst)) if i % 2 == 0]

def odd(lst: Iterable):
    return [lst[i] for i in range(len(lst)) if i % 2 == 1]
