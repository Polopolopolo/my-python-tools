from pathlib import Path
import json
import tomllib
from typing import Any
import yaml


def read_text(filepath: str):
    return Path(filepath).read_text()

def read_lines(filepath: str):
    return read_text(filepath).splitlines()

def write_text(filepath: str, text: str):
    return Path(filepath).write_text(text)

def yaml_fload(filepath: str):
    return yaml.load(read_text(filepath), Loader=yaml.BaseLoader)

def yaml_fdump(data: Any, filepath: str):
    with open(filepath, 'w') as f:
        yaml.dump(data, f)

def json_fload(filepath: str):
    return json.loads(read_text(filepath))

def json_fdump(data: Any, filepath: str):
    with open(filepath, 'w') as f:
        json.dump(data, f)

def toml_fload(filepath: str):
    return tomllib.loads(read_text(filepath))
