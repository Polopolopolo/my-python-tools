from pathlib import Path

from phd_utils import grep, mkdir, new_file, write_file


def create_presentation(name):
    name = Path(name)
    phdtools = name / ".phdtools"
    gitignore = name / ".gitignore"
    config = phdtools / "config.yml"
    mkdir(name)
    mkdir(phdtools)
    new_file(gitignore)
    write_file(gitignore, ".phdtools")
    new_file(config)
    # phd_utils.write_file(config, )

def move_undefined_ref(logfile: str, bib_bibtex: str, short_bibtex: str):
    '''Handle undefined references

    - Find undefined references from a latex compilation  
    - then search them in a bibtex file  
    - finally write them in a simpler bibtex file
    '''
    import bibtexparser
    # Search for undefined references
    print ('Searching for undefined references...')
    undefined_refs = []
    lines = grep('undefined', Path(logfile).as_posix())
    for line in lines:
        s = 'LaTeX Warning: Citation '
        if line.startswith(s):
            line = line[len(s):]
            quote_1 = line.find("'")
            quote_2 = line[quote_1 + 1:].find("'")
            undefined_refs.append(line[quote_1 + 1:quote_2 + 1])
    print (f'-> {len(undefined_refs)} undefined references found.')
    if not len(undefined_refs):
        return
    # Parse big bibtex file
    print ('Parsing big bibtex...')
    raw_entries = []
    bib = bibtexparser.parse_file(bib_bibtex)
    for key in undefined_refs:
        entries = [entry for entry in bib.entries if entry.key == key]
        if not len(entries):
            print (f'  -> no entry found for {key}')
            continue
        elif len(entries) > 1:
            print (f'  -> {len(entries)} entries found for {key}, picking the first one')
        else:
            print (f'  -> Entry for {key} found')
        raw_entries.append(entries[0].raw)
    print ('-> Parsing big bibtex: done')
    # Writing to simple bibtex
    print ('Writing short bibtex...')
    with Path(short_bibtex).open('a') as f:
        for raw in raw_entries:
            f.write('\n')
            f.write(raw)
    print ('-> Writing short bibtex: done')
