from argparse import ArgumentParser


if __name__ == "__main__":
    parser = ArgumentParser(description="Tool for latex projects management.")
    subparser = parser.add_subparsers(title="command")

    new_latex_proj_sp = subparser.add_parser("new-latex-project", help="Create a new latex project.")
    new_latex_proj_sp.add_argument("type", help="Type of the latex project.")
    new_latex_proj_sp.add_argument("name", help="Name of the latex project.")
    
