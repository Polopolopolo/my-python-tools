from pathlib import Path

from myutils.file_io import read_lines, toml_fload


def abspath(rel_path):
    dirpath = Path(__file__).parent
    return (dirpath / rel_path).as_posix()

def get_config():
    return toml_fload(abspath('config.toml'))


class Dic:
    def __init__(self, filename: str, project: str, project_location: str, language: str) -> None:
        self.filename = filename
        self.project = project
        self.project_location = project_location
        self.language = language

    @classmethod
    def from_path(cls, path: str):
        path = Path(path).absolute()
        return cls(path.name, path.parent.parent.name, path.parent.parent.parent, path.name.split('.')[2])

    def __repr__(self) -> str:
        return f'{self.filename} ({self.project}) ({self.project_location})'

    def path(self):
        return Path(self.project_location) / self.project / '.vscode' / self.filename

    def type(self):
        dic_path = self.path()
        if dic_path.is_symlink() and str(dic_path) in (str(dic) for dic in global_dics()):
            return 'global'
        else:
            return 'local'

    def words(self):
        return read_lines(self.path())

    def register(self, path: str):
        if Path(path).exists():
            content = Path(path).read_text()
            content += '' if content.endswith('\n') else '\n'
        else:
            content = ''
        if str(self.path()) not in content.splitlines():
            Path(path).write_text(content + str(self.path()) + '\n')
            return True
        else:
            return False

    def diff(self, dic):
        return set(self.words()).difference(dic.words())


def global_dics(config=None):
    if not config:
        config = get_config()
    global_dics_path = Path(abspath('.')) / config['global dics']['path']
    return [Dic.from_path(path) for path in global_dics_path.iterdir()]
