from argparse import ArgumentParser
import os
from pathlib import Path

from . import utils
from . import api

from myutils.colors import cprint
from myutils.file_io import read_lines


def search(args):
    dics = api.search(args.path)
    if not dics:
        exit ('No dictionnaries here.')
    width = max(len(str(dic.path())) for dic in dics)
    for dic in dics:
        print (str(dic.path()).ljust(width + 4), end='')
        print (f'({dic.type()})', end=' ')
        if args.register and dic.type() == 'local':
            ret = dic.register(utils.abspath(utils.get_config()['local-dics']['path']))
            if ret:
                print ('registered !')
            else:
                print ('already registered')
        else:
            print()

def list_local_dics(args):
    path = utils.abspath(utils.get_config()['local-dics']['path'])
    lines = read_lines(path)
    if not lines:
        exit('No known local dictionnaries.')
    for i, line in enumerate(lines):
        print (f'{i+1} - {utils.Dic.from_path(line)}')

def list_global_dics(args):
    dirpath = Path(utils.abspath('.')) / utils.get_config()['global-dics']['dirname']
    dics = list(dirpath.iterdir())
    if not dics:
        exit('No global dictionnaries.')
    else:
        for i, dic in enumerate(dics):
            print (f'{i+1} - {dic.name}')

def diff(args):
    path = utils.abspath(utils.get_config()['local-dics']['path'])
    lines = read_lines(path)
    dic1 = utils.Dic.from_path(lines[args.dic1 - 1])
    dic2 = utils.Dic.from_path(lines[args.dic2 - 1])
    for word in dic1.diff(dic2):
        cprint(f'+ {word}', 'green')
    for word in dic2.diff(dic1):
        cprint(f'- {word}', 'red')

def link(args):
    # Link
    link_dir = Path(os.path.abspath('.'))
    if args.smart_location and link_dir.name != '.vscode':
        try:
            link_dir = next(x for x in link_dir.rglob('.vscode') if x.is_dir())
        except StopIteration:
            print ('Warning: no `.vscode` link_directory found')
    ipt = input(f'The link will be put in {link_dir} (yes/no)')
    if not ipt.lower().startswith('y'):
        exit('Aborting...')
    # Target
    dirpath = Path(utils.abspath('.')) / utils.get_config()['global-dics']['dirname']
    dics = list(dirpath.iterdir())
    try:
        dicname = dics[args.index - 1]
    except IndexError:
        exit(f'There is no dic corresponding to index {args.index}, try to run list-globals')
    # Create the link
    link = link_dir / 'ltex.dictionary.en-US.txt' if args.rename else link_dir / dicname
    target = dirpath / dicname
    print ('link', link)
    print ('target', target)
    # link.symlink_to(target)


if __name__ == '__main__':
    parser = ArgumentParser(description='manage LTex (Language Tools extension for vscode) dictionnaries')
    subparsers = parser.add_subparsers(dest='command')

    search_subparser = subparsers.add_parser('search', help='search for a ltex dictionnary')
    search_subparser.add_argument('path', help='path in which to search the dictionnary')
    search_subparser.add_argument('--register', type=bool, default=True, help='register or not found dictionnaries')

    list_global_dics_subparser = subparsers.add_parser('list-globals', help='list the global dics')

    list_local_dics_subparser = subparsers.add_parser('list-locals', help='list the known local dics')

    diff_subparser = subparsers.add_parser('diff', help='print the difference between two dics')
    diff_subparser.add_argument('dic1', type=int, help='index of first dic (found using list-locals)')
    diff_subparser.add_argument('dic2', type=int, help='index of second dic (found using list-locals)')

    link_subparser = subparsers.add_parser('link', help='create a symlink to the given global dic')
    link_subparser.add_argument('index', type=int, help='index of the dic (first one is 1)')
    link_subparser.add_argument('--smart-location', type=bool, default=True, help='if 1: find the location in which to put the link (default 1)')
    link_subparser.add_argument('--rename', type=bool, default=True, help='if 1: rename the dic so that it is understood by vscode-ltex (default 1)')

    args = parser.parse_args()
    match args.command:
        case 'search':
            search(args)
        case 'list-locals':
            list_local_dics(args)
        case 'list-globals':
            list_global_dics(args)
        case 'diff':
            diff(args)
        case 'link':
            link(args)
        case default:
            parser.print_usage()
