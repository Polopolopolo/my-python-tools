from pathlib import Path

from . import utils
from .utils import Dic


def search(root: str):
    dics = []
    for path in Path(root).rglob('*'):
        if path.name.startswith('ltex.dictionary.'):
            dics.append(path)
    return [Dic.from_path(path) for path in dics]
