import subprocess
from pathlib import Path
from typing import List


def repeat_input(text, accepted_answers, default_answer=None):
    while "answer is not accepted":
        answer = input(text)
        if answer.lower() in accepted_answers:
            return answer.lower()
        elif default_answer:
            return default_answer
        else:
            print (f"Wrong answer, must be in ({', '.join(accepted_answers)}).")

def mkdir(path, interactive=False):
    path = Path(path)
    try:
        path.mkdir(parents=True)
    except FileExistsError:
        if interactive:
            repeat_input(f"Path {path} already exists, replace it ? [Yes, No]",
                        ("yes", "no"))
        else:
            print (f"Path {path} already exists, aborting.")

def new_file(path, interactive=False):
    path = Path(path)
    if path.exists():
        if interactive:
            repeat_input(f"Path {path} already exists, replace it ? [Yes, No]",
                        ("yes", "no"))
        else:
            print (f"Path {path} already exists, aborting.")
    else:
        path.open("w")

def write_file(path, content, mode="w"):
    with open(path) as f:
        f.write(content)


def grep(pattern: str, file: str, decode=True) -> List[str]:
    proc = subprocess.run(['grep', pattern, file], stdout=subprocess.PIPE)
    lines = proc.stdout.splitlines()
    return [line.decode() for line in lines] if decode else lines
